﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EVT.Applications.GraphSolution.Graph;

namespace EVT.Applications.GraphSolution.GraphVisitor.Interfaces
{
    public interface IEdgeVisitor
    {
        void VisitEdge(Edge edge);
        //Добавить в интерфейс IEdgeVisitor:
        bool ShouldTerminateEdgeVisiting { get; }
    }


}
