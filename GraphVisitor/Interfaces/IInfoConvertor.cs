﻿using EVT.Applications.GraphSolution.GraphInfo.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Graph.GraphVisitor.Interfaces
{
    // для приведения списка информаций, присоединенных к ноде , к нужному типу
    public interface IInfoConvertor<TResult>
    {
        TResult ConvertInfo(IEnumerable<INodeInfo> infolist);
    }
}
