﻿using EVT.Applications.GraphSolution.Graph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.GraphVisitor.Interfaces
{
    public interface INodeVisitor
    {
        void VisitNode(Node node);
        //Добавить в интерфейс INodeVisitor:
        bool ShouldTerminateNodeVisiting { get; }
    }
    
}
