﻿using EVT.Applications.GraphSolution.GraphInfo.Interfaces;
using EVT.Applications.GraphSolution.Graph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EVT.Applications.GraphSolution.GraphVisitor.Interfaces;

namespace EVT.Applications.GraphSolution.NodeVisitor
{
    //2) Добавим посетителя NodeNumberVisitor
    //? почему не сделать визитор с параметром TInfo - ограничиваем конкретный визитор 
    //определенным типом информации, с которым он может работать
    public abstract class ResultNodeVisitorBase<TResult, TProcessResult> : INodeVisitor// where TInfo: INodeInfo
    {
        // массив результатов обработки графа
        private List<TProcessResult> _processResults;
        protected IReadOnlyList<TProcessResult> _ProcessResults => _processResults;

        protected TNodeInfo GetNodeInfo<TNodeInfo>(IReadOnlyList<INodeInfo<TNodeInfo>> list, Node node)
        {
            return list.SingleOrDefault(x => x.Node == node).Info;
        }

        public TResult Result { get { return DoGetResult(_ProcessResults); } }

        //2.4) Реализовать метод VisitNode(Node node) как
        public void VisitNode(Node node)
        {
            // var info = _Storage[node];
            //var infoList = _Storage.GetInfoList(node);
            //TInfo info = _Convertor.ConvertInfo(infoList);

            TProcessResult result = DoVisitNode(node);
            if (result != null)
            {
                _processResults.Add(result);
            }

        }

        abstract protected TProcessResult DoVisitNode(Node node);
        abstract protected TResult DoGetResult(IEnumerable<TProcessResult> array);

        //abstract protected TInfo DoConvertInfo(IEnumerable<INodeInfo> listInfo);
        public Boolean ShouldTerminateNodeVisiting { get; private set; }


        #region C-tor
        //2.3) Добавить в конструктор параметр nodeNumberInfos: List<NodeNumberInfo>, инициализировать _nodeNumberInfos.Инициализировать Numbers пустым списком
        protected ResultNodeVisitorBase()
        //protected ResultNodeVisitorBase(NodeInfoStorage storage, IInfoConvertor<TInfo> convertor)
        {
            // _Storage = storage;
            // _Convertor = convertor;
            _processResults = new List<TProcessResult>();
        }

        #endregion

    }
}
