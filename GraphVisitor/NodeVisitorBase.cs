﻿using EVT.Graph.GraphInfo;
using EVT.Graph.GraphVisitor.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Graph.NodeVisitor
{
    //2) Добавим посетителя NodeNumberVisitor
    public abstract class AccumulationNodeVisitorBase: INodeVisitor
    {
        //У ноды мб много связанных м.у собой info
        //некоторую операцию производить сразу с набором info

        //2.1) Добавить приватное поле List<NodeNumberInfo> _nodeNumberInfos
        Node _nodeInfos;
        //2.2) Добавить в класс поле List<int> Numbers
        System.Collections.Generic.List _Infos;

        //отсебятина
        Action<IEnumerable<Info> _usefulAction;
        public Object Result { get { return _usefulAction.Invoke(_Infos); } }

        internal IEnumerable<TNodeInfo> GetInfos()
        {
            return _Infos.AsReadOnly();
        }

        #region C-tor
        //2.3) Добавить в конструктор параметр nodeNumberInfos: List<NodeNumberInfo>, инициализировать _nodeNumberInfos.Инициализировать Numbers пустым списком
        public NodeVisitorBase(List<Info> nodeInfos, Func<IEnumerable<TNodeInfo>, Object> function)
        {
            _nodeInfos = nodeInfos;
            _Infos = new List<TNodeInfo>();
            //отсебятина
            _usefulAction = function;
        }

        #endregion


        //2.4) Реализовать метод VisitNode(Node node) как
        public virtual void VisitNode(Node node)
        {
            Info nodeInfo = _nodeInfos.FirstOrDefault(info => info.Node == node);
            //_Infos.Add(nodeInfo.Info);    

        }

        public Boolean ShouldTerminateNodeVisiting { get; private set; }

    }
}
