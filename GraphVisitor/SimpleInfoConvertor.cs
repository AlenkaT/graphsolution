﻿using EVT.Graph.GraphVisitor.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EVT.Applications.GraphSolution.GraphInfo.Interfaces;

namespace EVT.Graph.GraphVisitor
{
    class SimpleInfoConvertor<TResult> : IInfoConvertor<TResult>
    {

        public TResult ConvertInfo(IEnumerable<INodeInfo> infoList)
        {
            var result = (TResult) infoList.SingleOrDefault(x => x is TResult);
            
            return result;
        }


        internal SimpleInfoConvertor()
        { }
    }
}
