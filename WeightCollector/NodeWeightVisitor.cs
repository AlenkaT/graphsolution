﻿using EVT.Graph.CommonNodeInfos;
using EVT.Graph.GraphVisitor.Interfaces;
using EVT.Graph.NodeVisitor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EVT.Graph.GraphInfo;
using EVT.Graph.GraphInfoStorage;

namespace EVT.Graph.NodeWeightCollecting
{
    //2) Добавим посетителя NodeNumberVisitor
    class NodeWeightVisitor : ResultNodeVisitorBase<Double, Double>
    {
        //2.1) Добавить приватное поле List<NodeWeightInfo> _nodeWeightInfos
        //List<NodeWeightInfo> _nodeWeightInfos;
        //2.2) Добавить в класс поле List<float> TotalWeight
        //List<float> _numbers;


        protected override Double DoVisitNode(IEnumerable<INodeInfo> listInfo)
        {
            var info = listInfo.SingleOrDefault(x => x is WeightInfo);

            if (info!= null)
            {
                return ((WeightInfo)info).Info;
            }

            return 0;
        }
        #region C-tor
        //2.3) Добавить в конструктор параметр nodeWeightInfos: List<NodeWeightInfo>, инициализировать _nodeWeightInfos.Инициализировать TotalWeight нулем
        internal NodeWeightVisitor(NodeInfoStorage storage) : base(storage, list => { return list.Sum(); })
        {

        }

        #endregion


        //2.4) Реализовать метод VisitNode(Node node) как
        //public void VisitNode(Node node)
        //{
        //    //NodeWeightInfo nodeInfo = _nodeWeightInfos.FirstOrDefault(info => info.Node == node);
        //    //if (nodeInfo != null)
        //    //{
        //    //    TotalWeight += nodeInfo.Info;
        //    //}
        //}

    }
}
