﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EVT.Applications.GraphSolution.Graph;
using EVT.Applications.GraphSolution.NodeVisitor;

using EVT.Applications.GraphSolution.NodeInfoTypes.Shared;
using EVT.Applications.GraphSolution.NodeInfoStorageManager.Interfaces;

namespace EVT.Applications.GraphSolution.NodeWeightCollecting
{
    //2) Добавим посетителя NodeNumberVisitor
    class NodeWeightVisitor : ResultNodeVisitorBase<Double, Double>
    {
        //2.1) Добавить приватное поле List<NodeWeightInfo> _nodeWeightInfos
        //List<NodeWeightInfo> _nodeWeightInfos;
        //2.2) Добавить в класс поле List<float> TotalWeight
        //List<float> _numbers;
        protected readonly INodeInfoStorageManager<WeightNodeInfo> _weights;

        protected override double DoVisitNode(Node node)
        {
            //var info = listInfo.SingleOrDefault(x => x is WeightInfo);
            var result = _weights.GetInfo(node);
            return (result == null) ? 0 : (Double)result;
        }

        
        protected override double DoGetResult(IEnumerable<double> array)
        {
            return _ProcessResults.Sum();
        }

        #region C-tor
        //2.3) Добавить в конструктор параметр nodeWeightInfos: List<NodeWeightInfo>, инициализировать _nodeWeightInfos.Инициализировать TotalWeight нулем
        internal NodeWeightVisitor(INodeInfoStorageManager<WeightNodeInfo> weights) : base()
        {
            _weights = weights;
        }

        #endregion


    }
}
