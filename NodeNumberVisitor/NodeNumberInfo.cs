﻿
using EVT.Graph.GraphInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Graph.NodeNumberVisitor
{
    //1) Добавим класс NodeNumberInfo: NodeInfo<int>.Он будет просто сопоставлять вершине графа некоторое число
    public class NodeNumberInfo : NodeInfo<int>
    {
        public NodeNumberInfo(Node node, int info):base(node, info)
        { }
    }
}
