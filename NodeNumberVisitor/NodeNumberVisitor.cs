﻿using EVT.Graph.NodeVisitor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Graph.NodeNumberVisitor
{
    //2) Добавим посетителя NodeNumberVisitor
    class NodeNumberVisitor : NodeVisitorBase<NodeNumberInfo, int>
    {
        //2.1) Добавить приватное поле List<NodeNumberInfo> _nodeNumberInfos
        //List<NodeNumberInfo> _nodeNumberInfos;
        //2.2) Добавить в класс поле List<int> Numbers
        //List<int> _numbers;
        //internal IEnumerable<int> GetNumbers()
        //{
        //    return _numbers.AsReadOnly();
        //}

        #region C-tor
        //2.3) Добавить в конструктор параметр nodeNumberInfos: List<NodeNumberInfo>, инициализировать _nodeNumberInfos.Инициализировать Numbers пустым списком
        internal NodeNumberVisitor(List<NodeNumberInfo> nodeNumberInfos):base (nodeNumberInfos)
        {
            //_nodeNumberInfos = nodeNumberInfos;
            //_numbers = new List<int>();
        }

        #endregion


        //2.4) Реализовать метод VisitNode(Node node) как
        //public void VisitNode(Node node)
        //{
        //    NodeNumberInfo nodeInfo = _nodeNumberInfos.FirstOrDefault(info => info.Node == node);
        //    _numbers.Add(nodeInfo.Info);    
        //}

        //public Boolean ShouldTerminateNodeVisiting { get; private set; }

    }
}
