﻿using EVT.Applications.GraphSolution.Graph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace EVT.Serialization.Graph
{
    
    public class SerializeableNode: Node
    {
        public Guid ID { get; }
        public SerializeableNode()
        {
            ID = Guid.NewGuid();
        }
    }
}
