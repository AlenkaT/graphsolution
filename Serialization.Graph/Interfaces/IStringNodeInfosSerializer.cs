﻿using EVT.Applications.GraphSolution.GraphInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace EVT.Serialization.Graph.Interfaces
{
    interface IStringNodeInfosSerializer
    {
        string SerializeNodeInfosToString<TNodeInfo>(List<NodeInfo<TNodeInfo>> nodeInfos);
        List<NodeInfo<TNodeInfo>> DeserializeNodeInfosFromString<TNodeInfo>(String nodeInfosString);
        void SerializeNodeInfosToTextFile<TNodeInfo>(List<NodeInfo<TNodeInfo>> nodeInfos, String fileName);
        List<NodeInfo<TNodeInfo>> DeserializeNodeInfosFromTextFile<TNodeInfo>(String fileName);
    }
}
