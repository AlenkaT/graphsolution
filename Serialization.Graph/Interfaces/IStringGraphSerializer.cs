﻿using EVT.Applications.GraphSolution.Graph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace EVT.Serialization.Graph.Interfaces
{
    public interface IStringGraphSerializer
    {
        String SerializeGraphToString(DirectedGraph graph);
        DirectedGraph DeserializeGraphFromString(String graphString);
        void SerializeGraphToTextFile(DirectedGraph graph, String fileName);
        DirectedGraph DeserializeGraphFromTextFile(String fileName);
    }
}
