﻿using EVT.Applications.GraphSolution.Graph;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.StructuralConstructor.Interfaces
{
    public interface IStructuralConstructor
    {
        // определить количество добавляемых нод 
        Boolean TryDetectConstruction(out ConstructorParametr parametr, params Object[] parametrs);
        void Construct(IEnumerable<Node> arr, ConstructorParametr parametr);
    }
}
