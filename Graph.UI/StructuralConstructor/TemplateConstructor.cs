﻿using EVT.Applications.GraphSolution.Graph;
using EVT.Applications.GraphSolution.BL.Infrastructure.NodeInfoWizards;
using StructuralConstructor.Helpers;
using Helper = StructuralConstructor.Helpers.StaticMathVerificator;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EVT.Applications.GraphSolution.StructuralConstructor.SubConstructionTemplates;

namespace EVT.Applications.GraphSolution.StructuralConstructor
{
    //конструктор, создающей для ноды информацию о параметрах построения подконструкций
    public class TemplateConstructor : ConstructorBase 
    {

        private Int32 _infoCounter = 0;
        protected readonly AnyNodeInfoWizard _infoWizard;
        protected readonly CaptionNodeInfoWizard _captionWizard;


        public override Boolean TryDetectConstruction(out ConstructorParametr parametr, params Object[] parametrs)
        {
            parametr = null;
            //сформировать тип шаблона на основе params
            if (Helper.DoesParametrsCountEqual(parametrs, 1))
            {
                //parametr = new CircleKarkas();
                return true;
            }
            return false;
        }

        protected override bool CanConstruct(IEnumerable<Node> arr, ConstructorParametr parametr, ref Exception ex)
        {
            return base.CanConstruct(arr, parametr, ref ex) && !TryGetArgumentNullException(parametr, nameof(parametr), ref ex) && !TryGetInvalidEqualCastException<RebarArrayConstructorParametr>(parametr, nameof(parametr), ref ex);
        }
        protected override void DoConstruct(IEnumerable<Node> arr, ConstructorParametr parametr)
        {
            foreach (var item in arr)
            {
                _infoWizard.CreateInfos<ConstructorParametr>(item, parametr);
                _captionWizard.CreateInfos(item, parametr.Caption + _infoCounter);
            }
        }

        public TemplateConstructor(AnyNodeInfoWizard infoWizard, CaptionNodeInfoWizard captionWizard)
        {
            _infoWizard = infoWizard;
            _captionWizard = captionWizard;
        }

    }
}
