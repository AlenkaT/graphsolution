﻿using EVT.Applications.GraphSolution.Graph;
using EVT.Applications.GraphSolution.BL.Infrastructure.NodeInfoWizards;
using StructuralConstructor.Helpers;
using Helper = StructuralConstructor.Helpers.StaticMathVerificator;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.StructuralConstructor
{

    public class A400RebarArrayConstructor : ConstructorBase
    {

        private Int32 _infoCounter = 0;
        protected readonly RebarGost5781InfoWizard _rebarInfoWizard;
  

        public override Boolean TryDetectConstruction(out ConstructorParametr parametr, params Object[] parametrs)
        {
            parametr = null;
            //В parametrs - передать 

            // Размер поперечного сечения parametr[0] - проверить по базе
            // Длина стержня parametr[1] - положительное Int16
            // Длина стержня parametr[2] -значение  1 до 3
            // SpecialCaption parametr[3] - string специальное имя
            if (Helper.DoesParametrsCountEqual(parametrs, 3))
            {
                if (Helper.IsInt16or32Positive(parametrs[0]) &&
                        Helper.IsInt16or32Positive(parametrs[1]) &&
                        Helper.IsBytePositive(parametrs[2]) &&
                        ((Byte)parametrs[2]).IsWithinRange(1, 3))
                {
                    parametr = new RebarArrayConstructorParametr((String)parametrs[0], Convert.ToInt32(parametrs[1]), (Byte)parametrs[2]);
                    return true;
                }
                return false;
            }
            if (Helper.DoesParametrsCountEqual(parametrs, 4))
            {
                if (Helper.IsInt16or32Positive(parametrs[0]) &&
                        Helper.IsInt16or32Positive(parametrs[1]) &&
                        Helper.IsBytePositive(parametrs[2]) &&
                        ((Byte)parametrs[2]).IsWithinRange(1, 3) &&
                        parametrs[3] is String)
                {
                    parametr = new RebarArrayConstructorParametr((String)parametrs[0], Convert.ToInt32(parametrs[1]), (Byte)parametrs[2], (String)parametrs[3]);
                    return true;
                }
                return false;
            }

            return false;
        }

        protected override bool CanConstruct(IEnumerable<Node> arr, ConstructorParametr parametr, ref Exception ex)
        {
            return base.CanConstruct(arr, parametr, ref ex) && !TryGetArgumentNullException(parametr, nameof(parametr), ref ex) && !TryGetInvalidEqualCastException<RebarArrayConstructorParametr>(parametr, nameof(parametr), ref ex);
        }
        protected override void DoConstruct(IEnumerable<Node> arr, ConstructorParametr parametr)
        {
            var prm = (RebarArrayConstructorParametr)parametr;
            foreach (var item in arr)
            {
                _infoCounter++;
                _rebarInfoWizard.CreateA400Infos(item, prm.Caption, prm.SteelProductType, prm.Size, prm.RebarLength);
            }
        }

        public A400RebarArrayConstructor(RebarGost5781InfoWizard rebarWizard)
        {
            _rebarInfoWizard = rebarWizard;
          
        }

    }
}
