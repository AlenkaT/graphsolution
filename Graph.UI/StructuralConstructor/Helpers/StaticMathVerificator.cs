﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StructuralConstructor.Helpers
{
    static class StaticMathVerificator
    {

        #region Protected HelperMethods
        //убрать в какой нибуд хелпер класс
        internal static Boolean DoesParametrsCountEqual(Object[] prs, Int32 expactedCount)
        {
            return (prs != null) && (prs.Count() == expactedCount);
        }

        internal static Boolean IsInt16or32Positive(Object p)
        {
            return (p is Int32) && ((Int32)p > 0) || (p is Int16) && ((Int16)p > 0);
        }

        internal static Boolean IsBytePositive(Object p)
        {
            return (p is Byte) && ((Byte)p > 0);
        }

        internal static Boolean IsDoublePositive(Object p)
        {
            return (p is Double) && ((Double)p > 0);
        }

        internal static Boolean IsWithinRange(this Int32 value, Int32 minValue=Int32.MinValue, Int32 maxValue=Int32.MaxValue)
        {

            return (value >= minValue) && (value <= maxValue);
        }

        internal static Boolean IsWithinRange(this Double value, Double minValue = Double.MinValue, Double maxValue = Double.MaxValue)
        {
            return (value >= minValue) && (value <= maxValue);
        }

        internal static Boolean IsWithinRange(this Byte value, Byte minValue = Byte.MinValue, Byte maxValue = Byte.MaxValue)
        {
            return (value >= minValue) && (value <= maxValue);
        }
        #endregion
    }
}
