﻿using EVT.Applications.GraphSolution.AssociatedConstructionTemplateInfoTypes;
using EVT.Applications.GraphSolution.Graph;
using EVT.Applications.GraphSolution.NodeInfoWizards;
using StructuralConstructor.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.StructuralConstructor
{ 
    public class SimpleConstructor2 : ConstructorBase
    {
        private Int32 _infoCounter = 0;
        protected CaptionNodeInfoWizard _captionWizard;
        protected AssociatedConstructionNodeInfoWizard _costructionTemplateWizard;

        AssociatedConstructionTemplateBase _template; // появляется в результате работы TryDetectConstruction при парсинге параметров

        public override Boolean TryDetectConstruction(out Int32 countNode, params Object[] parametrs)
        {
            countNode = 1;
            
            return true;
        }

        protected override bool CanConstruct(IEnumerable<Node> arr, ref Exception ex)
        {
            if (!base.CanConstruct(arr, ref ex)) return false; 
            if (StaticMathVerificator.DoesParametrsCountEqual(arr.ToArray(), 1))
            {
                ex = new ArgumentException("Array items count must equal 1", nameof(arr));
            }
            _template = new SBNKarkasTemplate(10, 0.02f, (Single)(1.2 / 2 - 0.1 - 0.02 / 2));
            return true;

        }
        protected override void DoConstruct(IEnumerable<Node> arr)
        {
            foreach (var item in arr)
            {
                _infoCounter++;
                _costructionTemplateWizard.CreateInfos(item, _template);


            }
        }

        public SimpleConstructor2(CaptionNodeInfoWizard captionWizard, AssociatedConstructionNodeInfoWizard costructionTemplateWizard)
        {
            _captionWizard = captionWizard;
            _costructionTemplateWizard = costructionTemplateWizard;
        }

    }
}
