﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.StructuralConstructor
{
    public class RebarArrayConstructorParametr : ConstructorParametr
    {
        public String Size { get; }
        public Int32 RebarLength { get; }
        public Byte SteelProductType { get; }


        internal RebarArrayConstructorParametr(String size, Int32 rebarLength, Byte steelProductType, String caption="") : base(caption,0)
        {
            Size = size;
            RebarLength = rebarLength;
            SteelProductType = steelProductType;
          
        }
    }
}
