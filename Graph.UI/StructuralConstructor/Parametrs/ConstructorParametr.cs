﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.StructuralConstructor
{
    public class ConstructorParametr
    {
        public String Caption { get; }
        public Int32 CountNode { get; } //количество узлов, необходимых конструктору для работы
        internal ConstructorParametr(String caption, Int32 count)
        {
            Caption = caption;
            CountNode = count;
        }
    }

    
}
