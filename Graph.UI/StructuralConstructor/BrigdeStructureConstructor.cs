﻿using EVT.Applications.GraphSolution.Graph;
using EVT.Applications.GraphSolution.BL.Infrastructure.NodeInfoWizards;
using Helper = StructuralConstructor.Helpers.StaticMathVerificator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.StructuralConstructor
{
    public class BrigdeStructureConstructor: ConstructorBase
    {
        protected CaptionNodeInfoWizard _captionWizard;

        public override bool TryDetectConstruction(out ConstructorParametr parametr, params object[] parametrs)
        {
            parametr = null;
            //parametrs[0]- количество пролетов
            Boolean result = Helper.DoesParametrsCountEqual(parametrs, 1) && Helper.IsInt16or32Positive(parametrs[0]);
            if (result)
            {
                parametr = new ConstructorParametr("Bridge", 2 * Convert.ToInt32(parametrs[0])+2); //+1 опор на 1 больше , чем пролетов, +1 на родительский узел (мост\путепровод)
            }
            return result;
        }

        protected override bool CanConstruct(IEnumerable<Node> arr, ConstructorParametr parametr, ref Exception ex)
        {

            //проверяем количество так, чтобы мог отработать DoConstruct
            //если не выполняются последние 2 условия - не формируется исключение !!!! исправить
            if (!base.CanConstruct(arr, parametr, ref ex)) return false;
            if ( arr.Count() > 3)
            {
                ex = new ArgumentOutOfRangeException(nameof(arr), "Количество элементов массива должно быть >3");
                return false;
            }
            if (arr.Count() % 2 == 0)
            {
                ex = new ArgumentOutOfRangeException(nameof(arr), "Количество элементов массива должно быть кратно 2");
                return false;
            }
            return true;   
        }

        protected override void DoConstruct(IEnumerable<Node> arr, ConstructorParametr parametr)
        {
            var n = (arr.Count() - 2) / 2;
            _captionWizard.CreateInfos(arr.First(), "Сооружение");
            _captionWizard.CreateInfos(arr.ElementAt(1), "Опора ОК1");
            _captionWizard.CreateInfos(arr.ElementAt(n + 1), "Опора ОК" + (n + 1));
            for (var i = 1; i < n; i++)
            {
                _captionWizard.CreateInfos(arr.ElementAt(i), "Опора ОП" + (i + 1));
                _captionWizard.CreateInfos(arr.ElementAt(i+n), "Пролет" + i);
            }
            _captionWizard.CreateInfos(arr.Last(), "Пролет" + n);
        }

        public BrigdeStructureConstructor(CaptionNodeInfoWizard captionWizard)
        {
            _captionWizard = captionWizard;

        }
    }

}
