﻿using EVT.Applications.Common.Helpers.ExceptionsGenerator;
using EVT.Applications.GraphSolution.Graph;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.StructuralConstructor
{
    //каждый конкретный конструктор должен получить в свой конструктор  визардов инфы, к которым он будет обращаться
    public abstract class ConstructorBase : ExceptionsGeneratorBase, Interfaces.IStructuralConstructor
    {

        #region Interface Implementation
        //для определения количества нод, требуемых конструктору для создания структуры
        public abstract bool TryDetectConstruction(out ConstructorParametr parametr, params object[] parametrs);
        public void Construct(IEnumerable<Node> arr, ConstructorParametr parametr)
        {
            Exception ex = null;
            if (CanConstruct(arr, parametr, ref ex))
            {
                DoConstruct(arr, parametr);

                return;
            }
            throw ex;
        }
        #endregion

        #region Protected NVI
        protected virtual Boolean CanConstruct(IEnumerable<Node> arr, ConstructorParametr parametr, ref Exception ex)
        {
            return !(TryGetArgumentNullException(arr, nameof(arr), ref ex) );
        }

        protected abstract void DoConstruct(IEnumerable<Node> arr, ConstructorParametr parametr);
        #endregion

        
    }

}
