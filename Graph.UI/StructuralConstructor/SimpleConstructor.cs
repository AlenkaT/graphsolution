﻿using EVT.Applications.GraphSolution.Graph;
using EVT.Applications.GraphSolution.NodeInfoWizards;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.StructuralConstructor
{

    public class SimpleConstructor : ConstructorBase
    {
        private Int32 _infoCounter = 0;
        protected CaptionNodeInfoWizard _captionWizard;

        public override Boolean TryDetectConstruction(out Int32 countNode, params Object[] parametrs)
        {
            countNode = 1;
            return true;
        }

        protected override void DoConstruct(IEnumerable<Node> arr)
        {
            foreach (var item in arr)
            {
                _infoCounter++;
                _captionWizard.CreateInfos(item, "Конструкция"+_infoCounter);
                
            }
        }

        public SimpleConstructor(CaptionNodeInfoWizard captionWizard)
        {
            _captionWizard = captionWizard;

        }

    }
}
