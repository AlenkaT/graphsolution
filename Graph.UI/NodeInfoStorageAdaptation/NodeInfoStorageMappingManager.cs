﻿using EVT.Applications.Common.Helpers.ExceptionsGenerator;

using EVT.Applications.GraphSolution.GraphInfo.Interfaces;
using EVT.Applications.GraphSolution.VMServices.ObjectMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EVT.Applications.Common.Components.Interfaces.ObjectMapping;
using EVT.Applications.Common.Components.Interfaces.ObjectLocalStorage;

namespace EVT.Applications.GraphSolution.EntitiesAdaptationToView.NodeInfoAdaptation.NodeInfoStorage.GeometricInfo
{
    //для доступа к манагеру и хранилищу из вью модели (для сериализации)
    public class NodeInfoStorageMappingManager<T> : ObjectMappingManagerBase
    {

        public static NodeInfoStorageMappingManager<T> NodeInfoStorageMappingManagerCreate(ILocalStorage<INodeInfo<T>> storage, ILocalStorageManager<INodeInfo<T>> manager)
        {
            Exception ex = null;
            if (CanCreateManager(storage, manager, ref ex))
            {
                return new NodeInfoStorageMappingManager<T>();
            }
            throw ex;
        }

        private static Boolean CanCreateManager(ILocalStorage<INodeInfo<T>> storage, ILocalStorageManager<INodeInfo<T>> manager, ref Exception ex)
        {
            if (TryGetArgumentNullException(storage, nameof(storage), ref ex) || TryGetArgumentNullException(manager, nameof(manager), ref ex))
                return false;

            return true;
        }

        //Заполнить свойства в каждом подклассе
        //protected abstract InfoWrapperBase DoGetWrapper(INodeInfo[] nodeInfos);

        #region Implementation IObjectMappingManager
        public override IObjectMapping CreateMapping(object obj)
        {
            var result = new ObjectMappingBase();
            if (obj != null)
            {
                this.DoSetObjectInToMapping(result, obj);
            }
            return result;
        }

        #endregion

        #region  Override NVI implementation

        protected override Boolean CanSetObjectInToMapping(IObjectMapping mapping, Object obj, ref Exception ex)
        {
            return base.CanSetObjectInToMapping(mapping, obj, ref ex) ||
                TryGetInvalidCastException(obj, nameof(obj), typeof(ILocalStorageManager<INodeInfo<T>>), ref ex) ||
                TryGetInvalidEqualCastException<ObjectMappingBase>(mapping, nameof(mapping), ref ex);
        }

        protected override void DoSetObjectInToMapping(IObjectMapping mapping, object obj)
        {
            base.DoSetObjectInToMapping(mapping, obj);
        }

        #endregion

        protected NodeInfoStorageMappingManager()
        {

        }
    }
}
