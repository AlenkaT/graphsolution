﻿using EVT.Applications.Common.Components.Interfaces.Factories;
using EVT.Applications.Common.Helpers.ExceptionsGenerator;
using EVT.Applications.GraphSolution.Graph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace EVT.Applications.GraphSolution.Components.GraphFactory
{
    public class TreeGraphFactory : GraphFactory, ISimpleFactory<TreeGraph>
    {


        protected override DirectedGraph DoCreateProduct(Object parametr)
        {
            return new TreeGraph();
        }

        TreeGraph ISimpleFactory<TreeGraph>.CreateProduct(object parametr)
        {
            return (CreateProduct(parametr) as TreeGraph);
        }

        #region C-tor
        public TreeGraphFactory()
        {
            
        }
        #endregion

    }
}
