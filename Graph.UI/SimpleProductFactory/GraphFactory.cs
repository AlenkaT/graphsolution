﻿using EVT.Applications.Common.Components.Interfaces.Factories;
using EVT.Applications.Common.Helpers.ExceptionsGenerator;
using EVT.Applications.GraphSolution.Graph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace EVT.Applications.GraphSolution.Components.GraphFactory
{
    public class GraphFactory : ExceptionsGeneratorBase, ISimpleFactory<DirectedGraph>
    {
        #region Implimentation Interface

        //public event EventHandler ProductCreated;
        //public event EventHandler ProductCreationFailed;

        public DirectedGraph CreateProduct(Object parametr=null)
        {
            Exception ex = null;
            if (CanCreateProduct(parametr, ref ex))
            {
                // этот метод может выбрасывать эксепшн
                var result = DoCreateProduct(parametr);
                return result;
            }
            throw ex;
        }
        #endregion


        #region Protected Methods

        protected virtual Boolean CanCreateProduct(Object parametr, ref Exception ex)
        {
            return true;
        }

        protected virtual DirectedGraph DoCreateProduct(Object parametr)
        {
            return new DirectedGraph();
        }

        
        #endregion

        #region C-tor
        public GraphFactory()
        {
            
        }
        #endregion

    }
}
