﻿using EVT.Applications.Common.Components.Interfaces.Factories;
using EVT.Applications.GraphSolution.Graph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.Graph.GraphUnitTests
{
    internal static class SunFactory
    {
        internal static TreeGraph CreateCDAG(Byte sunLevel, Byte countFirstLevel, Byte countChildren)
        {
            //Солнышко
            int n1 = countFirstLevel;
            var nds = new List<TestNode>() {new TestNode()}; //центр
            var ndsL = new List<TestNode>(nds);
            var nds3 = new List<TestNode>();
            var edges = new List<Edge>();
            TestNode newNode;

            for (var level = 0; level < sunLevel; level++)
            {
                foreach (var node in ndsL)
                {
                    for (var i = 0; i < n1; i++)
                    {
                        newNode = new TestNode();
                        nds.Add(newNode);
                        nds3.Add(newNode);
                        edges.Add(new Edge(node, newNode));
                    }

                }
                ndsL = nds3.ToList();
                nds3.Clear();
                n1 = countChildren;
            }

            return new TreeGraph(nds, edges);

        }
    }

    public class TreeGraphTestFactory : ISimpleFactory<TreeGraph>
    {
        public Type GetProductType()
        {
            return typeof(TreeGraph);
        }
        public TreeGraph CreateProduct(Object parametr)
        {
            return SunFactory.CreateCDAG(2, 6, 2);
        }
    }
}
