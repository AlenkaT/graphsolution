﻿using EVT.Applications.Common.Components.Interfaces.EventAggregatorEventArgs;

using System;


namespace EVT.Applications.GraphSolution.EventAggregatorAdaptation.VMEventArgs //для сообщений от вьюмодели
{
    public class GraphViewUpdatedEventArgs : AggregatorEventArgs
    {
        public Object NewValue { get; }
        public Object Source { get; }
        public GraphViewUpdatedEventArgs(object newValue, object source) : base(null)
        {
            NewValue = newValue;
            Source = source;
        }
    }
}
