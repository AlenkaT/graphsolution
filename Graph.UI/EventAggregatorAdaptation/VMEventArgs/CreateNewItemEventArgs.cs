﻿using EVT.Applications.Common.Components.Interfaces.EventAggregatorEventArgs;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.EventAggregatorAdaptation.VMEventArgs //для сообщений от вьюмодели
{
    public class RequestNewItemEventArgs : RequestOperationEventArgs
    {
        public Object Owner { get; }
        public RequestNewItemEventArgs(Object parametr, Object owner):base(parametr)
        {
            Owner = owner;
        }
    }

    public class CreationNewItemSuccessfullyCompletedEventArgs : AnswerOperationEventArgs
    {
        public Object NewItem { get; }
        public CreationNewItemSuccessfullyCompletedEventArgs(int answerKey, Object newItem) : base(answerKey)
        {
            NewItem = newItem;
        }
    }

    public class CreationNewItemFailedEventArgs : AnswerOperationEventArgs
    {
        public String ExceptionMessage { get; }
        public CreationNewItemFailedEventArgs(int answerKey, String exceptionMessage) : base(answerKey)
        {
            ExceptionMessage = exceptionMessage;
        }
    }
}
