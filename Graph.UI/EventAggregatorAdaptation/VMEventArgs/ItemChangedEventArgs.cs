﻿using EVT.Applications.Common.Components.Interfaces.EventAggregatorEventArgs;

using System;


namespace EVT.Applications.GraphSolution.EventAggregatorAdaptation.VMEventArgs //для сообщений от вьюмодели
{
    public class ItemChangedEventArgs : AggregatorEventArgs
    {
        public Object OldValue { get; }
        public Object NewValue { get; }
        public ItemChangedEventArgs(object oldValue, object newValue) : base(null)
        {
            OldValue = oldValue;
            NewValue = newValue;
        }
    }
}
