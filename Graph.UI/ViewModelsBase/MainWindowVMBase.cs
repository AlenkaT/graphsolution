﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;

using EVT.WPF.MVVMSupport;

using EVT.Applications.Common.Components.Interfaces.ObjectMapping;
using EVT.Applications.GraphSolution.Common.Components.Interfaces.AdaptationService;
using EVT.Applications.GraphSolution.VMServices.ObjectMapping;
using EVT.Applications.GraphSolution.EntitiesAdaptationToView.NodeInfoAdaptation;

namespace EVT.Applications.GraphSolution.ViewModelsBase.MainWindowVM
{
    //TData - тип объекта, с которым работает приложение (настраивается до инициализации контрейнера)
    public class MainWindowVMBase<T> : BindableBase
    {
        // БАЗОВАЯ ВЬЮМОДЕЛЬ ДОЛЖНА СОДЕРЖАТЬ ВСЕ СВОЙСТВА, К КОТОРЫМ ПРИВЯЗЫВАЕТСЯ ВЬЮ //
        #region Properties, injected by IoC

        #endregion

        #region Fields
        MappingCollection rpg;
        
        protected readonly DefinitionObjectMappingManager _ObjMapManager ; 
       

        protected readonly IAdaptationService<T> _AdaptationService;
        protected ObservableCollection<IObjectMapping> _objectCollection;

        protected IObjectMapping _selectedTabItem;
        #endregion

        #region ViewBinding Properties

        public Type DataType { get; }
        public ReadOnlyObservableCollection<IObjectMapping> ObjectCollection { get; }
        public IObjectMapping SelectedTabItem
        {
            get { return _selectedTabItem; }
            set {
                SetProperty(ref _selectedTabItem, value, nameof(SelectedTabItem)); }
        }
        public MappingCollection PropertyGridSelectedObject
        {
            get
            {
                return rpg;
            }
        }
        
        // Свойства объекта

        public string ObjectName { get; set; }
        public string ObjectDefinition { get; set; }


        //#region CommandProperties
        public IDelegateCommand CreateNewItemCommand { get; }
        //// public IDelegateCommand LostFocusCommand { get; }


        //#endregion

        #endregion

        #region CommandExecute
        void CreateNewItemCommandExecute(Object parametr)
        {
            //Запрос на создание пустой обертки
            //?Порядок загрузки элементов может не соответствовать запросам?
            var mapping = _ObjMapManager.CreateMapping(ObjectName, ObjectDefinition); //! это функция отдельного сервиса слоя сервисов вью модели
            _objectCollection.Add(mapping);
            //Оповещаем вью , что надо затянуть новый элемент
            RaisePropertyChanged(nameof(ObjectCollection));

            BuildObjectIntoMapping(mapping, parametr);
        }

        protected void BuildObjectIntoMapping(IObjectMapping mapping, Object parametr)
        {
            _ObjMapManager.StartObjectLoading(mapping); 
            //var key = _AdaptationService._GetRequestKey(mapping);
            //Непонятно - корректен ли такой вызов в рамках mvvm
            //с одной стороны - это НЕ бизнеслогика
            // с другой стороны - мы просто запрашиваем свойство модели , а выполняем определенные операции с данными
            var result=_AdaptationService.CreateObject(parametr); //тут будет асинхронщина
            _ObjMapManager.SetObjectInToMapping(mapping, result);
            
        }
        #endregion

        #region Private Methods
        
        #endregion


        #region Constructor

        //public MainWindowViewModel(IEnumerable<MenuInfo> projecTypes)
        public MainWindowVMBase(Type dataType, IAdaptationService<T>  adaptationService,
            DefinitionObjectMappingManager mappingManager,
            MappingCollection nodeinfoMappingCollection)//ITreeViewComponent<T> treeViewComponentVM)
        {
            //dataType -  тип отображаемых данных

            #region  Commands Initializing

            CreateNewItemCommand = new EVT.WPF.MVVMSupport.DelegateCommand(CreateNewItemCommandExecute);
            //LostFocusCommand = new DelegateCommand(LostFocusInvokeEventExecute);
            #endregion

            //        AppCommands.ViewModel = this; // поскольку класс, в котором локализована обработка команд является не обязательным, а лишь удобным расширением, устанавливаем связь неявно

            _objectCollection = new ObservableCollection<IObjectMapping>();
            ObjectCollection = new ReadOnlyObservableCollection<IObjectMapping>(_objectCollection);
            //localStorage.NewItemAdded += NewObjectAddedEventHandler;

            ObjectName = "My Name is ...";
            ObjectDefinition = dataType.GetType().Name;

            DataType = dataType;
            _AdaptationService = adaptationService;
            _ObjMapManager = mappingManager;

            rpg = nodeinfoMappingCollection;


        }
    
        #endregion





    }
}


