﻿
using EVT.Applications.GraphSolution.ViewModelsBase.DataInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;


namespace EVT.Applications.GraphSolution.ViewModelsBase.ComponentsVMInterfaces
{
    //TData - тип источника данных
    //T - тип, содержащий объекты TData
    public interface ITreeViewComponent<out TData> 
    {
        IEnumerable<IHierarchicalData<TData>> RootSource { get; } //для загрузки в элемент ItemsSoure необходима коллекция
        IHierarchicalData<TData> SelectedTreeViewItem { get; }
        //IEnumerable<TData> CurrentChildren { get; } // ? нужны ли?
    }
}
