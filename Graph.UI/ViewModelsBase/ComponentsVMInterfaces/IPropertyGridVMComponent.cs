﻿
using System;
using System.Collections.Generic;
using System.Linq;


namespace EVT.Applications.GraphSolution.ViewModelsBase.ComponentsVMInterfaces
{
    public interface IPropertyGridVMComponent<in T>
    {
        T SelectedItem { set; } 
    }
}
