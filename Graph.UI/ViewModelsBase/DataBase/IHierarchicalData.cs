﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.ViewModelsBase.DataInterfaces
{
    public interface IHierarchicalData<out T> 
    {
       T Data { get; }
        IEnumerable<IHierarchicalData<T>> Children { get; }
        T Parent { get; }
    }

}
