﻿using EVT.Applications.Common.Components.Interfaces.Fasade;
using EVT.Applications.GraphSolution.Common.Components.Interfaces.AdaptationService;

using EVT.Applications.GraphSolution.Common.Components.Interfaces.Updater;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace EVT.Applications.GraphSolution.ViewModelsBase.AdaptationService
{
    public class AdaptationServiceBase<TConvertor, Tin, TOut> : IAdaptationService<Tin> where TConvertor : IConvertor<TOut, Tin>, new()
    {
        //private Dictionary<Int32, TOut> _convertedData;
        protected readonly Dictionary<Tin, Object> _convertedDataDictionary;

        protected readonly IFasade<Tin> _appFasade;

        protected readonly TConvertor _convertor;

        protected readonly IUpdaterCollection<Tin> _updaterCollection;

        Object IAdaptationService<Tin>.CreateObject(object parametr)
        {
            Tin result = DoCreateItem(parametr); // тут надо перейти на асинхрон.
            var conv = _convertor.Convert(result); //для itemssource тривью
            _convertedDataDictionary[result] = DoPostCreate(conv); // сохраняем объект и сконвертированный вид в словаре
            return _convertedDataDictionary[result];
        }



        protected virtual Tin DoCreateItem(Object parametr)
        {
            return _appFasade.CreateItem(parametr);
        }

        protected virtual Object DoPostCreate(TOut createdObject)
        {
            return createdObject;
        }

        void IAdaptationService<Tin>.UpdateObject(Object targetObject, String updaterName, SourceParametrs sparams)
        {
            if (_convertedDataDictionary.ContainsValue(targetObject)) //targetObject - сконвертированное представление =itemssource для графа
            {
                Tin obj = _convertedDataDictionary.Single(pair => targetObject.Equals(pair.Value)).Key;
                var updater = _updaterCollection.GetUpdater(updaterName);
                if (updater==null)
                {
                    //возможно уведомление о том, что метод не выполнен, так как не найден обработчик в словаре обработчиков
                    return;
                }
                DoUpdateObject(obj, updater, sparams);


                //_convertedDataDictionary[obj] = conv;
                DoPostUpdateObject(obj, sparams);
                return;
            }
            throw new Exception("Method UpDate in invalid!");
        }

        protected virtual void DoUpdateObject(Tin item, IUpdater<Tin> updater, SourceParametrs sparams)
        {
            _appFasade.UpdateItem(item, updater, sparams);
            return;
        }

        protected virtual void DoPostUpdateObject(Tin item, SourceParametrs sparams)
        { }

        public void DeleteObject(Tin obj)
        {
            _appFasade.DeleteItem(obj);
            _convertedDataDictionary.Remove(obj);

        }


        Object IAdaptationService<Tin>.ReadObject(Object parametr)
        {
            return ReadObject(parametr);
        }
        public TOut ReadObject(Object parametr)//, Int32 requestKey)
        {
            var item = _appFasade.ReadItem(parametr);
            //_convertedData[requestKey] = _convertor.Convert(item);.
            var conv = _convertor.Convert(item);
            _convertedDataDictionary[item] = conv;
            return conv;
        }

        protected virtual Tin DoReadObject(Object parametr)
        {
            return _appFasade.ReadItem(parametr);
        }




        #region C-tor
        public AdaptationServiceBase(IFasade<Tin> appFasade, IUpdaterCollection<Tin> updaterCollection)
        {
            _appFasade = appFasade;
            _convertor = new TConvertor();
            _convertedDataDictionary = new Dictionary<Tin, Object>();
            _updaterCollection = updaterCollection;
        }

        #endregion
    }
}
