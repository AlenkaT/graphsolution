﻿using EVT.Applications.Common.Components.Interfaces.EventAggregator;

using EVT.Applications.GraphSolution.ViewModelsBase.ComponentsVMInterfaces;
using EVT.Applications.GraphSolution.ViewModelsBase.DataInterfaces;

using EVT.WPF.MVVMSupport;

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.ViewModelsBase.Components
{
    public class TreeViewComponentVMBase<TData> : BindableBase, ITreeViewComponent<TData>
    {
        #region Fields
        protected IEnumerable<IHierarchicalData<TData>> _rootSource;
        protected IHierarchicalData<TData> _selectedItem;
       
        
        #endregion

        #region BindableProperties
        public IHierarchicalData<TData> MainRoot
        { get { return _rootSource.FirstOrDefault(); } }

        public IEnumerable<IHierarchicalData<TData>> RootSource
        {
            get { return _rootSource; }
            protected set { SetProperty(ref _rootSource, value, nameof(RootSource)); }
        }
        public IHierarchicalData<TData> SelectedTreeViewItem
        {
            get { return _selectedItem; }
            //set { _SetSelectedItem(value); }
            set
            {
                //Устанавливается только из View

                SetProperty(ref _selectedItem, value, nameof(SelectedTreeViewItem));
               
            }
        }

        
        #endregion

        #region Private Methos

        void _SetSelectedItem(TData value)
        //Изменяем CurrentChildren при изменении SelectedItem и сообщаем слушателям
        {
            //if (Object.Equals(_selectedItem, value))
            //{
            //    return;
            //}
            //_selectedItem = value;
            //_currentChildren.Clear();
            //var children = _selectedItem.Children; //эта операция даст IEnumerable<КонкретногоИерархического типа>
            //foreach (var child in children)
            //{
            //    _currentChildren.Add((T)child);
            //}

            //RaisePropertyChanged(nameof(CurrentChildren));

        }
        #endregion


        #region C-tor
        public TreeViewComponentVMBase(IEnumerable<IHierarchicalData<TData>> root) // этот компонент заижектим через свойство ObjectUpdaterCommands<TObject> commands)
        { 
            RootSource = root;

        }
        #endregion 
    }
}
