﻿

//using EVT.Applications.GraphSolution.GraphUIViewModels.MainWindowVM.Components;
using EVT.WPF.MVVMSupport;
using System;

using System.Windows;
using System.Windows.Controls;



namespace EVT.GraphUI.Views.Components
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class TreeViewComponentView : Page, IView
    {
        public TreeViewComponentView()
        {
            //DataContextChanged += DataContextChangedCallBack;
            InitializeComponent();
            //при запуске через контейнер происходит следующее:
            /* создание главного окна с фреймом
             * DataContext фрейма устанавливается правильно
             * Page создаётся и загружается только после
             * команды app.Run(mainwindow)
             * поскольку форма содержит навигационный контейр - фрейм,
             * то происходит загрузка страницы (подробно Мак Дональд Метью, стр.750)
             * при этом происходит вызов конструктора станицы и 
             * DataContext страницы  сбрасывается в null
             */
           // DataContext = DefaultTreeViewComponentVM.VM; //работает
        }

        BindableBase IView.DataContext
        {
            get
            {
                return this.DataContext as BindableBase;
            }
            set
            {
                this.DataContext = value;
            }
        }

        public void Activate()
        {
            throw new NotImplementedException();
        }



        private void DataContextChangedCallBack(Object sender, DependencyPropertyChangedEventArgs e)
        {
            MessageBox.Show("Контекст treeView изменен");
        }

        //private void UnLoadedCallBack(Object sender, RoutedEventArgs e)
        //{
        //    var message = "Иди спать!";
        //}
        //private void CallBackSelectedObjectChanged(object sender, RoutedPropertyChangedEventArgs<Object> e)
        //{
        //    //var eds = MyPropertyGrid.EditorDefinitions;
        //}

        //private void LostFocusPGCallBack(Object sender, RoutedEventArgs e )
        //{
        //    var str = "Я тебя поймала";
        //}
        //private void SelectedObjectChangedPGCallBack(Object sender, RoutedEventArgs e)
        //{
        //   // var str = MyPropertyGrid.SelectedObject as StringWrapper;

        //   // if (str != null)
        //   // {
        //  //      var var1 = str.Text;
        //  //  }
        //}

    }
}

/*
 * При работе с настройкой отображения элементов для дерева получены выводы:
 * Установка TreeView ItemsSource - задаёт коллекцию только для первого уровня дерева
 * Чтобы настроить процесс отображения детей для всех уровней понадобилось устанавлитвать ItemsSource для ItemContainerStyle,
 * чтобы элемент знал , что каждый элемент дерева может иметь коллекцию детей
 * Установка ItemsSource в HierarchicalDataTemplate не помогает, так как задает только коллекцию для детей
 * второго уровня
 * Параметр TemplatedParent привязки источника данных для шаблона , размещенного в разделе ресурсов, означает
 * что источник будет определяться родителем в том месте, где применен шаблон
 * при этом если нужно сделать привязку к свойству данных из контекста , то источник Path нужно задавать через 
 * DataContext.PropertyName
 */