﻿

using System.Windows;


namespace EVT.Graph.UI.MainWindowView
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindowView: Window
    {
        public MainWindowView()
        {
            
            InitializeComponent();
            //DataContext = DefaultTreeViewComponentVM.VM;
            DataContextChanged += DataContextChangedEventHandler;

           
        }

        #region EventHandlers
        private void DataContextChangedEventHandler(object sender, DependencyPropertyChangedEventArgs e)
        {
            
           // MessageBox.Show("DataContext of MainWindow was changed");
            
        }
        #endregion
    }
}
