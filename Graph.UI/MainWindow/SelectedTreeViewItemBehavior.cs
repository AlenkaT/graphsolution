﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;

using System.Windows.Data;
using System.Collections;

namespace EVT.Applications.GraphSolution.AttachedDependencyProperties
{  // при инициализации раздела бихавиорс контрола выполняются методы OnAttached()
   // в которых на событие SelectedItemChanged(или то, обработка которого требуется)
   // senderа подписывается приватный метод класса.
   // В этом методе меняется значение свойства зависимости, что приводит к выполнению
   // обработчика события изменения свойства зависимости (который мы передали в метаданные при
   // регистрации свойства зависимости
   // 
   // Summary:
   //     Encapsulates state information and zero or more ICommands into an attachable
   //     object.
   //
   // Type parameters:
   //   T:
   //     The type the System.Windows.Interactivity.Behavior`1 can be attached to.
   //
   // Remarks:
   //     Behavior is the base class for providing attachable state and commands to an
   //     object. The types the Behavior can be attached to can be controlled by the generic
   //     parameter. Override OnAttached() and OnDetaching() methods to hook and unhook
   //     any necessary handlers from the AssociatedObject.
   // public abstract class Behavior<T> : Behavior where T : DependencyObject

    public class SelectedTreeViewItemBehavior : Behavior<TreeView>
    {
        public object SelectedItem
        {
            get { return (object)GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }

        public static readonly DependencyProperty SelectedItemProperty = DependencyProperty.Register("SelectedItem",
            typeof(object), typeof(SelectedTreeViewItemBehavior),
            new UIPropertyMetadata(null));

        //Метод выполняемый при изменении свойства засисимости, если он добавлен при регистрации свойства зависимости
        private static void OnSelectedItemChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            //выполняется при изменении свойства зависимости
            //передаём новый выбранный элемент значению свойства данного класса
            // в данном случае sender-ом является класс, в котором зарегистрировано свойство зависимости
            var item = e.NewValue as TreeViewItem;

            if (item != null)
            {
                //тривьюайтем устанавливает значение своего свойства зависимости IsSelected в тру
                // это происходит когда свойсвтво зависимости меняется самим объектом 

                // если для TreeView также определено свойство зависимости IsSelectedProperty, то нужно установить его значение
                //  item.SetValue(TreeViewItem.IsSelectedProperty, true);  
            }
        }

        private static object OnCoerceValueCallback(DependencyObject sender, object data)

        {
            //Код возможной корректировки значения свойства зависимости
            //if (data == null) return _propertyValue;
            return data;
        }

        protected override void OnAttached() //выполняется при инициализации??или при загрузке контрола, так как определён раздел бихавиорс
        {
            // Выполняется при инициализации события присоединения свойства к объекту
            base.OnAttached();
            //подписываемся на обработку события изменения элемента тривью(в данном случае он AssociatedObject)
            this.AssociatedObject.SelectedItemChanged += OnTreeViewSelectedItemChanged;


        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            if (this.AssociatedObject != null)
            {
                //отписываемся при отсоединении свойства
                this.AssociatedObject.SelectedItemChanged -= OnTreeViewSelectedItemChanged;
            }
        }


        private void OnTreeViewSelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            //измени свойство зависимости, когда произошло событие SelectedItemChanged
            // этот метод вызвает событие измениния свойства зависимости 
            // и OnSelectedItemChanged
            this.SelectedItem = e.NewValue; // имеет тип контекста элемента
                                            //  SetCurrentChildren((UIElement)e.Source, ((TreeViewItem)e.NewValue).Items);

            //((TreeViewItem)e.NewValue).SourceUpdated+=SourceUpdatedEventHandler;
            //((TreeViewItem)e.OldValue).SourceUpdated -= SourceUpdatedEventHandler;

        }


    }
       
}


