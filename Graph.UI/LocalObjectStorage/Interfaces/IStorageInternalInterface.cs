﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.Common.Components.Interfaces.ObjectLocalStorage
{
    interface IStorageInternalInterface<T>
    {
        Int64 AddObject(T obj);
        Int64? GetObjectID(T obj);
        Boolean TryGetObjectByID(Int64 id, out T obj);
        void RemoveObject(T obj);

    }
}
