﻿using EVT.Applications.Common.Components.Interfaces.ObjectLocalStorage;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using EVT.Applications.Common.ComponentsInterfaces.EventAggregator;
//using EVT.Applications.GraphSolution.EventAggregatorAdaptation.LocalStorageEventArgs;

namespace EVT.Applications.Common.Components.ObjectLocalStorage
{
    public class ObjectLocalStorage<T> : IStorageInternalInterface<T>, ILocalStorage<T>
    {
        static Int64 _LastID;
        static Int64 GetID()
        {
            return _LastID++;
        }
        protected Dictionary<Int64, T> _storage;
     

        #region Implement Internal Interface
        Int64 IStorageInternalInterface<T>.AddObject(T obj)
        {
            var id = ((IStorageInternalInterface<T>)this).GetObjectID(obj);
            if (id != null)
            {
                return id.Value;
            }
            id = GetID();
            _storage[id.Value] = obj;
            
            //Наверно подключить ивентарегатора
           // _eventAggregator.PublishEvent(this, new LocalStorageEventArgs() )
            //RaiseNewItemAdded(obj);
            return id.Value;
        }
        Int64? IStorageInternalInterface<T>.GetObjectID(T obj)
        {
            var pair = _storage.SingleOrDefault(p => Object.ReferenceEquals(obj, p.Value));
            if (pair.Equals(default(KeyValuePair<Int64, T>)))
            {
                return null;
            }
            return pair.Key;
        }
        Boolean IStorageInternalInterface<T>.TryGetObjectByID(Int64 id, out T obj)
        {
            var result = _storage.TryGetValue(id, out obj);
            return result;
        }
        void IStorageInternalInterface<T>.RemoveObject(T obj)
        {
            if (obj == null) return;
            var id = ((IStorageInternalInterface<T>)this).GetObjectID(obj);
            if (id != null)
            {
                _storage.Remove(id.Value);

                //RaiseItemDeleted(obj);
            }
        }
        #endregion

        #region Impliment interface
       // public event EventHandler<LocalStorageEventArgs> NewItemAdded;
       // public event EventHandler<LocalStorageEventArgs> ItemDeleted;

        public IReadOnlyList<T> GetObjects()
        {
            return _storage.Values.ToList().AsReadOnly();
        }

        public String Name { get; }

        #endregion

        #region Protected Methods
        //protected virtual void RaiseNewItemAdded(T obj)
        //{
        //    NewItemAdded?.Invoke(this, new LocalStorageEventArgs(obj));
        //}

        //protected virtual void RaiseItemDeleted(T obj)
        //{
        //    ItemDeleted?.Invoke(this, new LocalStorageEventArgs(obj));
        //}
        #endregion

        #region C-tor
        public ObjectLocalStorage()
        {
            Name = typeof(T).Name;
            _storage = new Dictionary<Int64, T>();
           
        }
        #endregion

    }
}
