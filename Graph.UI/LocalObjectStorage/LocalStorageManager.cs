﻿using EVT.Applications.Common.Components.Interfaces.ObjectLocalStorage;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using EVT.Applications.Common.ComponentsInterfaces.EventAggregator;
//using EVT.Applications.GraphSolution.EventAggregatorAdaptation.LocalStorageEventArgs;

namespace EVT.Applications.Common.Components.ObjectLocalStorage
{
    public class LocalStorageManager<T> : ILocalStorageManager<T>
    {
        IStorageInternalInterface<T> _storage;//= new ObjectLocalStorage<T>(); // реализация по умолчанию
        protected ILocalStorage<T> Storage => (ILocalStorage<T>)_storage;

        //public event EventHandler<StorageChangedEventArgs<T>> StorageChanged;

        public void AttachStorage(ILocalStorage<T> storage)
        {
            var intStorage = storage as IStorageInternalInterface<T>;
            if (intStorage != null)
            {
                var oldStorage = _storage;
                _storage = intStorage;
                //RaiseStorageChanged(storage, oldStorage as IStoragePublicInterface<T>);
                //_eventAggregator.PublishEvent(this, new StorageChangedEventArgs<T>(storage, oldStorage as IStoragePublicInterface<T>));
                return;
            }
            throw new InvalidCastException("");
        }
        
        public void AddObjectToStorage(T obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }
            _storage.AddObject(obj);

        }

        public void RemoveObjectFromStorage(T obj)
        {
            _storage.RemoveObject(obj);

        }

        public T ReadObjectByID(Int64 id)
        {
            T obj;
            if (_storage.TryGetObjectByID(id, out obj))
            {
                return obj;
            }
            return default(T);
        }

        #region Protected Methods
        //protected virtual void RaiseStorageChanged(IStoragePublicInterface<T> newStorage, IStoragePublicInterface<T> oldStorage)
        //{
        //    StorageChanged?.Invoke(this, new StorageChangedEventArgs<T>(newStorage, oldStorage));
        //}
        #endregion

        #region C-tor
        public LocalStorageManager()
        {
         
        }
        #endregion
    }
}
