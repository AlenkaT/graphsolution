﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.Common.Components.Interfaces.Factories
{
    public interface ISimpleFactory<out TBase>
    {
        //Параметр на всякий случай!
        TBase CreateProduct(Object parametr=null);
        //Type GetProductType();

    }
}
