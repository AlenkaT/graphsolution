﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.Common.Components.Interfaces.AdaptationService
{
    public interface IConvertor<Tout, Tin> 
    {
        Tout Convert(Tin obj, Object parametr=null);
    }
}
