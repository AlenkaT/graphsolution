﻿using EVT.Applications.GraphSolution.Common.Components.Interfaces.Updater;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.Common.Components.Interfaces.AdaptationService
{ 
    public interface IAdaptationService<Tin>
    {
        Object CreateObject(Object parametr);
        void UpdateObject(Object targetObject, String updaterName, SourceParametrs sparams);
        Object ReadObject(Object parametr);
    }
}
