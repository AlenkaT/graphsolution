﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.Common.Components.Interfaces.ObjectLocalStorage
{
    public interface ILocalStorageManager<T> // T- тип данных хранилища
    {
        void AttachStorage(ILocalStorage<T> storage);
        //event EventHandler<StorageChangedEventArgs<T>> StorageChanged;

        void AddObjectToStorage(T obj);
        T ReadObjectByID(Int64 id);
        void RemoveObjectFromStorage(T obj);
    }
}
