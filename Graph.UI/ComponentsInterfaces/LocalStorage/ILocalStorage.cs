﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.Common.Components.Interfaces.ObjectLocalStorage
{
    public interface ILocalStorage<out T>
    {
        String Name { get; }
        //Boolean TryGetObjectID(T obj, out Int64? id);
        //Int64? GetObjectID(T obj);
        // Boolean TryGetObjectByID(Int64 id, out T obj);
        IReadOnlyList<T> GetObjects();

    }
}
