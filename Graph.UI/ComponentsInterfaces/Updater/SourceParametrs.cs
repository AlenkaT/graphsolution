﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.Common.Components.Interfaces.Updater
{
    public class SourceParametrs
    {
        public Object Source { get; }
        public Object[] Parametrs { get; }

        public SourceParametrs(Object source , params Object[] parametrs)
        {
            Source = source;
            Parametrs = parametrs;
        }
    }

   
}
