﻿using EVT.Applications.GraphSolution.Common.Components.Interfaces.Updater;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.Common.Components.Interfaces.Updater
{
    
    // для применения действия к графу
    public interface IUpdaterCollection<T>
    {
        IUpdater<T> GetUpdater(String updaterName);
        
    }
}
