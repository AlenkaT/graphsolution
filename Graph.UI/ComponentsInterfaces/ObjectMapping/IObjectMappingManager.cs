﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.Common.Components.Interfaces.ObjectMapping
{
    public interface IObjectMappingManager
    {
        IObjectMapping CreateMapping(Object obj );


        void StartObjectLoading(IObjectMapping mapping);


        void SetObjectInToMapping(IObjectMapping mapping, Object obj);


    }
}
