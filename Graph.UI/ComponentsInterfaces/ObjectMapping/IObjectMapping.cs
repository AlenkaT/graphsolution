﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.Common.Components.Interfaces.ObjectMapping
{
    public enum LoadingState
    {
        NotAssigned,
        InProgress,
        Completed,
        Failed
    }
    public interface IObjectMapping
    {
        Object MappedObject { get; }

        LoadingState State { get; }
    }
}
