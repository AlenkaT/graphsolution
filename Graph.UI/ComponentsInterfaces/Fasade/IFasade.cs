﻿using EVT.Applications.GraphSolution.Common.Components.Interfaces.Updater;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.Common.Components.Interfaces.Fasade
{
    public interface IFasade<T>
    {
        T CreateItem(Object parametr);
        T ReadItem(Object parametr);
        void UpdateItem(T targetObject, IUpdater<T> updater, SourceParametrs sparams);
        void DeleteItem(T item);
        
    }
}
