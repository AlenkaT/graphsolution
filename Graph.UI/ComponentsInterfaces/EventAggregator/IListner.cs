﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.Common.Components.Interfaces.EventAggregator
{
    //Реальные слушатели реализуют много интерфейсов IListner, закрытых соответствующими аргументами
    public interface IListner<T> where T: EventAggregatorEventArgs.AggregatorEventArgs
    {
        void EventHandler(Object sender, T e);
    }
}
