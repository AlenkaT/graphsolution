﻿using EVT.Applications.Common.Components.Interfaces.EventAggregatorEventArgs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.Common.Components.Interfaces.EventAggregator
{
    public interface IEventAggregator
    {
        //подход, когда метод агрегатора вызывается напрямую, а не отправляется сообщение
        void PublishEvent(Object sender, AggregatorEventArgs subject);
        void SubscribeListener<T>(Object listner, Action<Object, AggregatorEventArgs> CallBackMethod) where T : AggregatorEventArgs;
        /* удаление  слушателя из реестра агрегатора напрямую => 
         * необходимо отслеживать, когда слушатель становится неактуальным , 
         * чтобы удалить 
        */
        void UnSubscribeListener(Object listner, Action<Object,AggregatorEventArgs> CallBackMethod);

    }
}
