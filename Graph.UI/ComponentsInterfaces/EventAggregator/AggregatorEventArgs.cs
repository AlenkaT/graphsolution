﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.Common.Components.Interfaces.EventAggregatorEventArgs
{
    public abstract class AggregatorEventArgs : EventArgs
    {
        public Object Parametr { get; }
        public AggregatorEventArgs(Object parametr)
        {
            Parametr = parametr;
        }
    }

    public abstract class RequestOperationEventArgs : AggregatorEventArgs
    {
        public int Key { get; }
        public RequestOperationEventArgs(Object parametr):base(parametr)
        {
            Key = this.GetHashCode();
        }
    }

    public abstract class AnswerOperationEventArgs : AggregatorEventArgs
    {
        public int AnswerKey { get; }

        public AnswerOperationEventArgs(int answerKey):base(null)
        {
            AnswerKey = answerKey;
        }
    }
}
