﻿using EVT.Applications.GraphSolution.SharedInfoTypes;
using EVT.Applications.GraphSolution.Graph;
using EVT.Applications.GraphSolution.GraphInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.NodeInfoTypes.Shared
{
    public class RegestryHelper { }

    //Этими классами будут закрыты NodeInfoManager-ы , входящие в конструкторы 
    //визиторов , чтобы жестко ограничить тип получаемой информации при работе визитора
    public class WeightNodeInfo : NodeInfo<Double>
    {
        public WeightNodeInfo(Node node, Double info ):base(node, info) {}
    }

    public class SectionNodeInfo : NodeInfo<Section>
    {
        public SectionNodeInfo(Node node, Section info) : base(node, info) { }
    }
    public class MaterialNodeInfo : NodeInfo<Material>
    {
        public MaterialNodeInfo(Node node, Material info) : base(node, info) { }
    }
    public class ConstructionTypeNodeInfo : NodeInfo<EnConstructionMaterialType>
    {
        public ConstructionTypeNodeInfo(Node node, EnConstructionMaterialType info) : base(node, info) { }
    }

    
    public class CaptionNodeInfo : NodeInfo<String>
    {
        public CaptionNodeInfo(Node node, String info) : base(node, info) { }
    }



    //public class SharedNodeInfoTypes
    //{
    //    public static readonly String WeightInfoName = "Weight";
    //    public static readonly String SectionInfoName = "Section";
    //    public static readonly String MaterialInfoName = "Material";
    //    public static readonly String ConstructionTypeInfoName = "ConstructionType";
    //    public static readonly String CaptionInfoName = "Caption";

    //    public static readonly IReadOnlyDictionary<String, Type> InfoTypes = new Dictionary<string, Type>()
    //    {
    //        { WeightInfoName, typeof(Double) },
    //        { SectionInfoName, typeof(Section) },
    //        { MaterialInfoName, typeof(Material) },
    //        { ConstructionTypeInfoName, typeof(EnConstructionMaterialType) },
    //        { CaptionInfoName, typeof(String) }

    //    };

    public class SteelProductTypeNodeInfo : NodeInfo<EnSteelProductType>
    {
        public SteelProductTypeNodeInfo(Node node, EnSteelProductType info) : base(node, info) { }
    }
}