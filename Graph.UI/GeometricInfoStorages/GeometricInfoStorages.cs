﻿using EVT.Applications.Common.Components.ObjectLocalStorage;
using EVT.Applications.GraphSolution.GraphInfo.Interfaces;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.NodeInfoStorage.GeometricInfo
{
    public sealed class LengthInfoStorage : ObjectLocalStorage<INodeInfo<Double>>
    {
    }

    public sealed class WidthInfoStorage : ObjectLocalStorage<INodeInfo<Double>>
    {
    }

    public sealed class HeightInfoStorage : ObjectLocalStorage<INodeInfo<Double>>
    {
    }

    public sealed class AreaInfoStorage : ObjectLocalStorage<INodeInfo<Double>>
    {
    }

    public sealed class VolumeInfoStorage : ObjectLocalStorage<INodeInfo<Double>>
    {
    }

}
