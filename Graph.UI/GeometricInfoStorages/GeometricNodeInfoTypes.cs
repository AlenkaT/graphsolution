﻿using EVT.Applications.GraphSolution.SharedInfoTypes;
using EVT.Applications.GraphSolution.Graph;
using EVT.Applications.GraphSolution.GraphInfo;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.NodeInfoTypes.Geometric
{
    public class RegestryHelper { }

    //Этими классами будут закрыты NodeInfoManager-ы , входящие в конструкторы 
    //визиторов , чтобы жестко ограничить тип получаемой информации при работе визитора
    public class LengthNodeInfo : NodeInfo<Double>
    {
        public LengthNodeInfo(Node node, Double info) : base(node, info) { }
    }


    public class WidthNodeInfo : NodeInfo<Double>
    {
        public WidthNodeInfo(Node node, Double info) : base(node, info) { }
    }

    public class AreaNodeInfo : NodeInfo<Double>
    {
        public AreaNodeInfo(Node node, Double info) : base(node, info) { }
    }

    public class VolumeNodeInfo : NodeInfo<Double>
    {
        public VolumeNodeInfo(Node node, Double info) : base(node, info) { }
    }

    public class HeightNodeInfo : NodeInfo<Double>
    {
        public HeightNodeInfo(Node node, Double info) : base(node, info) { }
    }
    //public class SharedNodeInfoTypes
    //{
    //    public static readonly String WeightInfoName = "Weight";
    //    public static readonly String SectionInfoName = "Section";
    //    public static readonly String MaterialInfoName = "Material";
    //    public static readonly String ConstructionTypeInfoName = "ConstructionType";
    //    public static readonly String CaptionInfoName = "Caption";

    //    public static readonly IReadOnlyDictionary<String, Type> InfoTypes = new Dictionary<string, Type>()
    //    {
    //        { WeightInfoName, typeof(Double) },
    //        { SectionInfoName, typeof(Section) },
    //        { MaterialInfoName, typeof(Material) },
    //        { ConstructionTypeInfoName, typeof(EnConstructionMaterialType) },
    //        { CaptionInfoName, typeof(String) }

    //    };
}

