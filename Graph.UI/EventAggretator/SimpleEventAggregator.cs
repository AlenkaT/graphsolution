﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EVT.Applications.Common.Helpers.ExceptionsGenerator;
using EVT.Applications.Common.Components.Interfaces.EventAggregator;
using EVT.Applications.Common.Components.Interfaces.EventAggregatorEventArgs;

namespace EVT.Applications.Common.Components.EventAggregator
{
    public class SimpleEventAggregator : ExceptionsGeneratorBase, IEventAggregator
    {
        readonly Dictionary<Object, Dictionary<Type, Action<Object, AggregatorEventArgs>>> _listnersDictionary;

        #region Interface Implimentation
        public void PublishEvent(object sender, AggregatorEventArgs e)
        {
            Exception ex = null;
            if (CanPublishEvent(sender, e, ref ex))
            {
                DoPublishEvent(sender, e);
                return;
            }
            throw ex;

        }

        public void SubscribeListener<T>(Object listner, Action<Object, AggregatorEventArgs> callBackMethod) where T: AggregatorEventArgs
        {
            Exception ex = null;
            if (CanSubscribeListener(listner, callBackMethod, ref ex))
            {
                DoSubscribeListener<T>(listner, callBackMethod);
                return;
            }
            throw ex;
        }

        public void UnSubscribeListener(object listner, Action<Object, AggregatorEventArgs> CallBackMethod)
        {
            Exception ex = null;
            if (CanUnSubscribeListener(listner, CallBackMethod,  ref ex))
            {
                DoUnSubscribeListener(listner, CallBackMethod); ;
                return;
            }
            throw ex;
        }
        #endregion

        #region Protected Methods (NVI implimentation)
        protected virtual Boolean CanSubscribeListener(Object listner, Action<Object, AggregatorEventArgs> callBackMethod, ref Exception ex)
        {
            if (TryGetArgumentNullException(listner, nameof(listner), ref ex))
            { return false; }
            if (TryGetArgumentNullException(callBackMethod, nameof(callBackMethod), ref ex))
            { return false; }
            

            return true;
        }
        protected virtual void DoSubscribeListener<T>(Object listner, Action<Object, AggregatorEventArgs> callBackMethod) where T: AggregatorEventArgs
        {
            if (!_listnersDictionary.ContainsKey(listner))
            {
                _listnersDictionary[listner] = new Dictionary<Type, Action<object, AggregatorEventArgs>>();
                
            }
            _listnersDictionary[listner][typeof(T)] = callBackMethod; // если т
        }

        protected virtual Boolean CanUnSubscribeListener(Object listner, Action<Object, AggregatorEventArgs> callBackMethod, ref Exception ex)
        {
            if (TryGetArgumentNullException(listner, nameof(listner), ref ex))
            { return false; }
            if (TryGetArgumentNullException(callBackMethod, nameof(callBackMethod), ref ex))
            { return false; }
            return true;
        }
        protected virtual void DoUnSubscribeListener(Object listner, Action<Object, AggregatorEventArgs> callBackMethod)
        {
            if (_listnersDictionary[listner].ContainsValue(callBackMethod))
            {
                var eventTypes=_listnersDictionary[listner].Where(pair => pair.Value == callBackMethod).Select(p=>p.Key);
                foreach (var eventType in eventTypes)
                {
                    _listnersDictionary[listner].Remove(eventType);
                }
            }
        }

        protected virtual Boolean CanPublishEvent(Object sender, AggregatorEventArgs e, ref Exception ex)
        {
            if (TryGetArgumentNullException(e, nameof(e), ref ex, "MessageContext "))
            { return false; }
            return true;
        }

        protected virtual void DoPublishEvent(Object sender, AggregatorEventArgs e)
        {
            foreach (var listner in _listnersDictionary)
            {
                var typeKey = e.GetType();
                if (listner.Value.ContainsKey(typeKey))
                {
                    listner.Value[typeKey].Invoke(sender, e);
                }
            }
        }
        #endregion


        #region C-tor
        public SimpleEventAggregator()
        {
            _listnersDictionary = new Dictionary<object, Dictionary<Type, Action<object, AggregatorEventArgs>>>();
        }
        #endregion
    }
}
