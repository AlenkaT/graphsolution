﻿
using EVT.Applications.GraphSolution.Graph;
using EVT.Applications.GraphSolution.GraphInfo.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.NodeInfoStorageManager.Interfaces
{
    public interface INodeInfoAccessor
    {
        // получить все виды инф для одной ноды
        IReadOnlyList<INodeInfo> GetNodeInfos(Node node); 
    }

    public interface INodeInfoStorageManager<TNodeInfo> where TNodeInfo:class, INodeInfo//, ILocalStorageManager<INodeInfo<TInfo>> 
    {
        // получить инфу определенного типа для массива точек (для сериализации)
        IReadOnlyList<TNodeInfo> GetNodeInfos(IEnumerable<Node> nodes); 
        // получить инфу определенной ноды 
        TNodeInfo GetNodeInfo(Node node);

        Object GetInfo(Node node);
    }

   
}
