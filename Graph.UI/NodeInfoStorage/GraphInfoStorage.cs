﻿using EVT.Applications.GraphSolution.CommonNodeInfos;
using EVT.Applications.GraphSolution.Graph;
using EVT.Applications.GraphSolution.GraphInfo.Interfaces;
using EVT.Applications.GraphSolution.SteelSpecificationCollecting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.NodeInfoStorage
{
    public class GraphInfoStorage //общее хранилище всех нодинфо
    {
        #region C-tor
        public GraphInfoStorage()
        { }
        #endregion

        #region Fields
        //protected readonly INodeInfoStorage<Double> _LengthInfoStorage;
        //protected readonly INodeInfoStorage<Section> _SectionInfoStorage;
        //protected readonly INodeInfoStorage<Material> _MaterialInfoStorage;
        //protected readonly INodeInfoStorage<Double> _WidthInfoStorage;
        //protected readonly INodeInfoStorage<Double> _HeightInfoStorage;
        //protected readonly INodeInfoStorage<Double> _AreaInfoStorage;
        //protected readonly INodeInfoStorage<Double> _VolumeInfoStorage;
        //protected readonly INodeInfoStorage<Double> _WeightInfoStorage;
        //protected readonly INodeInfoStorage<String> _CaptionInfoStorage;
        #endregion


        

        //#region Length
        //IReadOnlyList<INodeInfo<LengthInfo>> INodeInfoStorage<LengthInfo>.Storage => _LengthInfoStorage.Storage;
        //public bool AddInfo(Node node, LengthInfo Info)
        //{
        //    return _LengthInfoStorage.AddInfo(node, Info);
        //}
        //LengthInfo INodeInfoStorage<LengthInfo>.GetInfo(Node node)
        //{
        //    return _LengthInfoStorage.GetInfo(node);
        //}
        //#endregion

        //#region Width
        //IReadOnlyList<INodeInfo<WidthInfo>> INodeInfoStorage<WidthInfo>.Storage => _WidthInfoStorage.Storage;
        //public bool AddInfo(Node node, WidthInfo Info)
        //{
        //    return _WidthInfoStorage.AddInfo(node, Info);
        //}
        //WidthInfo INodeInfoStorage<WidthInfo>.GetInfo(Node node)
        //{
        //    return _WidthInfoStorage.GetInfo(node);
        //}
        //#endregion

        //#region Height
        //IReadOnlyList<INodeInfo<HeightInfo>> INodeInfoStorage<HeightInfo>.Storage => _HeightInfoStorage.Storage;
        //public bool AddInfo(Node node, HeightInfo Info)
        //{
        //    return _HeightInfoStorage.AddInfo(node, Info);
        //}
        //HeightInfo INodeInfoStorage<HeightInfo>.GetInfo(Node node)
        //{
        //    return _HeightInfoStorage.GetInfo(node);
        //}
        //#endregion

        //#region Area
        //IReadOnlyList<INodeInfo<AreaInfo>> INodeInfoStorage<AreaInfo>.Storage => _AreaInfoStorage.Storage;
        //public bool AddInfo(Node node, AreaInfo Info)
        //{
        //    return _AreaInfoStorage.AddInfo(node, Info);
        //}
        //AreaInfo INodeInfoStorage<AreaInfo>.GetInfo(Node node)
        //{
        //    return _AreaInfoStorage.GetInfo(node);
        //}
        //#endregion

        //#region Volume
        //IReadOnlyList<INodeInfo<VolumeInfo>> INodeInfoStorage<VolumeInfo>.Storage => _VolumeInfoStorage.Storage;
        //public bool AddInfo(Node node, VolumeInfo Info)
        //{
        //    return _VolumeInfoStorage.AddInfo(node, Info);
        //}
        //VolumeInfo INodeInfoStorage<VolumeInfo>.GetInfo(Node node)
        //{
        //    return _VolumeInfoStorage.GetInfo(node);
        //}
        //#endregion

        //#region Weight
        //IReadOnlyList<INodeInfo<WeightInfo>> INodeInfoStorage<WeightInfo>.Storage => _WeightInfoStorage.Storage;
        //public bool AddInfo(Node node, WeightInfo Info)
        //{
        //    return _WeightInfoStorage.AddInfo(node, Info);
        //}
        //WeightInfo INodeInfoStorage<WeightInfo>.GetInfo(Node node)
        //{
        //    return _WeightInfoStorage.GetInfo(node);
        //}
        //#endregion

        //#region Section
        //IReadOnlyList<INodeInfo<SectionInfo>> INodeInfoStorage<SectionInfo>.Storage => _SectionInfoStorage.Storage;
        //public bool AddInfo(Node node, SectionInfo Info)
        //{
        //    return _SectionInfoStorage.AddInfo(node, Info);
        //}
        //SectionInfo INodeInfoStorage<SectionInfo>.GetInfo(Node node)
        //{
        //    return _SectionInfoStorage.GetInfo(node);
        //}
        //#endregion

        //#region Material
        //IReadOnlyList<INodeInfo<MaterialInfo>> INodeInfoStorage<MaterialInfo>.Storage => _MaterialInfoStorage.Storage;

        //public bool AddInfo(Node node, MaterialInfo Info)
        //{
        //    return _MaterialInfoStorage.AddInfo(node, Info);
        //}
        //MaterialInfo INodeInfoStorage<MaterialInfo>.GetInfo(Node node)
        //{
        //    return _MaterialInfoStorage.GetInfo(node);
        //}
        //#endregion

        //#region ProductType
        //protected readonly INodeInfoStorage<EnSteelProductType> _ProductTypeInfoStorage;
        //IReadOnlyList<INodeInfo<EnSteelProductType>> INodeInfoStorage<EnSteelProductType>.Storage => _ProductTypeInfoStorage.Storage;

        //public bool AddInfo(Node node, EnSteelProductType Info)
        //{
        //    return _ProductTypeInfoStorage.AddInfo(node, Info);
        //}
        //EnSteelProductType INodeInfoStorage<EnSteelProductType>.GetInfo(Node node)
        //{
        //    return _ProductTypeInfoStorage.GetInfo(node);
        //}
        //#endregion

    }
}
