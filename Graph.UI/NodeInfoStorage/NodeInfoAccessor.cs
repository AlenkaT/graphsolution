﻿
using EVT.Applications.Common.Components.Interfaces.ObjectLocalStorage;
using EVT.Applications.Common.Components.ObjectLocalStorage;
using EVT.Applications.GraphSolution.SharedInfoTypes;
using EVT.Applications.GraphSolution.GraphInfo.Interfaces;
using EVT.Applications.GraphSolution.NodeInfoStorageManager;
using EVT.Applications.GraphSolution.NodeInfoStorageManager.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EVT.Applications.GraphSolution.Graph;

namespace EVT.Applications.GraphSolution.NodeInfoAccessors
{
    
    //для работы необходимо создать сразу все нужные хранилища (не отложенно), чтобы нормально отображались данные в проперти грид
    public class NodeInfoAccessor : INodeInfoAccessor // этот класс используется как вью модель только для установки соответствия менеджер-хранилище, так как  не удается настроить стратегию создания менеджера для юнити
    {
        protected readonly ILocalStorage<INodeInfo> _Storage;

        public IReadOnlyList<INodeInfo> GetNodeInfos(Node node)
        {
            // получить все виды инф для одной ноды
            var result = _Storage.GetObjects().Where(x => x.Node == node);
            return result.ToList().AsReadOnly();
        }

        public NodeInfoAccessor(ILocalStorage<INodeInfo> storage )
        {
            _Storage = storage;
        }

       
    }

}
