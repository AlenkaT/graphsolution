﻿using EVT.Applications.Common.Components.ObjectLocalStorage;
using EVT.Applications.GraphSolution.Graph;
using EVT.Applications.GraphSolution.GraphInfo.Interfaces;
using EVT.Applications.Common.Components.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EVT.Applications.Common.Components.Interfaces.ObjectLocalStorage;


namespace EVT.Applications.GraphSolution.NodeInfoStorageManager
{
    //ILocalStorageManager<INodeInfo<String>> - не приводится к этому интерфейссу

    public class NodeInfoStorageManager<TNodeInfo> : LocalStorageManager<INodeInfo>, Interfaces.INodeInfoStorageManager<TNodeInfo> where TNodeInfo: class, INodeInfo
    {

        #region INodeInfoStorageManager<TNodeInfo> interface implementation

        // получить инфу определенного типа для массива точек (для сериализации)
        public IReadOnlyList<TNodeInfo> GetNodeInfos(IEnumerable<Node> nodes)
        {
            var result = Storage.GetObjects().Where(x => nodes.Contains(x.Node) && x is TNodeInfo).Cast<TNodeInfo>(); //&& x.NodeInfoType == nodeInfoType && x.Info is TInfo).Select(y => (TInfo)y.Info);
            return result.ToList();
        }

        // получить инфу определенной ноды
        public TNodeInfo GetNodeInfo(Node node)
        {
            var nodeinfo = Storage.GetObjects().SingleOrDefault(x => x.Node == node) as TNodeInfo;
            return nodeinfo;
        }

        

        // если информация для типов структур не найдена , то вернется 0, а должно возвращаться null
        // поэтому этот метод, возвращающий конетертную информацию ноды, не использовать
        public Object GetInfo(Node node)
        {
            var nodeinfo = Storage.GetObjects().SingleOrDefault(x => x.Node == node);
            if (nodeinfo != null)
            {
                return nodeinfo.Info;
            }
            return null;
        }
        #endregion


        #region C-tor
        public NodeInfoStorageManager() : base()
        { }
        #endregion
    }


}

