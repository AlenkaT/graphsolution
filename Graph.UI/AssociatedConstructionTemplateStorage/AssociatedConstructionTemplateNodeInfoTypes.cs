﻿
using EVT.Applications.GraphSolution.AssociatedConstructionTemplateInfoTypes;
using EVT.Applications.GraphSolution.Graph;
using EVT.Applications.GraphSolution.GraphInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.NodeInfoTypes.AssociatedConstructionTemplate
{

    //Этими классами будут закрыты NodeInfoManager-ы , входящие в конструкторы 
    //визиторов , чтобы жестко ограничить тип получаемой информации при работе визитора
    public class AssociatedConstructionTemplateNodeInfo : NodeInfo<AssociatedConstructionTemplateBase>
    {
        public AssociatedConstructionTemplateNodeInfo(Node node, AssociatedConstructionTemplateBase info ):base(node, info) {}
    }


}
