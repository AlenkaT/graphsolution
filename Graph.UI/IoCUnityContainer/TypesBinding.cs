﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Ioc.ContainerBasedOnUnity
{
    class TypesBinding
    {
        //для установки соответсвия классов View и ViewModel
        private readonly Dictionary<Type, Type> _binding;

        public TypesBinding()
        {
            _binding = new Dictionary<Type, Type>();
           
        }

        public void Bind<TView, TViewModel>()
        {
            _binding[typeof(TView)] = typeof(TViewModel);
        }
        public void Bind(Type tView, Type tViewModel)
        {
            _binding[tView] = tViewModel;
        }

        public Type GetViewType<TView>() => _binding[typeof(TView)];
    }
}
