﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity.Extension;
using Unity.Lifetime;
using System.Reflection;
using Unity.Builder.Strategy;
using Unity.Builder;
using Unity;
using EVT.Applications.GraphSolution.GraphInfo.Interfaces;
using EVT.Applications.GraphSolution.NodeInfoStorageManager;
using EVT.Applications.GraphSolution.NodeInfoStorageManager.Interfaces;

using EVT.Applications.Common.Components.Interfaces.ObjectLocalStorage;
using Unity.Injection;
using EVT.Applications.GraphSolution.BL.Infrastructure.NodeInfoWizards;
using EVT.Applications.GraphSolution.Common.Components.Interfaces.Updater;
using EVT.Applications.GraphSolution.BL.Infrastructure.Updaters;

namespace EVT.Ioc.ContainerBasedOnUnity
{
    //Расширения контейнера используются для добавления различной функциональности
    public static class RegistrationHelper
    {
        // Для работы механизма авторегистации необходимо, чтобы класс ViewModel-и имел префикс VM
        public static void NodeInfoWizards(this IUnityContainer container, String nodeInfoStorageManagerName, Type helperType)
        {
            //Инфовизарды
            
            var asmTypes = helperType.Assembly.GetExportedTypes().Where(t => t.Name.Contains("NodeInfoWizard") && !t.IsAbstract); 
            foreach (var type in asmTypes)
            {
                container.RegisterType(type, new ContainerControlledLifetimeManager(),
                    new InjectionConstructor(new ResolvedParameter<ILocalStorageManager<INodeInfo>>(nodeInfoStorageManagerName)));
            }
        }

        public static void Updaters<T>(this IUnityContainer container,  Type helperType)
        {
            //Инфовизарды
            var updaterInterface = typeof(IUpdater<T>);
            var asmTypes = helperType.Assembly.GetExportedTypes().Where(t => updaterInterface.IsAssignableFrom(t) && !t.IsAbstract);
            var rp = new List<ResolvedParameter<KeyValuePair<String, IUpdater<T>>>>();
            foreach (var type in asmTypes)
            {
                var name = type.Name;
                container.RegisterType(updaterInterface, type, name , new ContainerControlledLifetimeManager());
                container.RegisterInstance<String>(name, name);
                container.RegisterType<KeyValuePair<String, IUpdater<T>>>(name, 
                    new InjectionConstructor(new ResolvedParameter<String>(name), new ResolvedParameter<IUpdater<T>>(name)));
                rp.Add(new ResolvedParameter<KeyValuePair<string, IUpdater<T>>>(name));
            }

            container.RegisterType<IUpdaterCollection<T>, UpdaterCollection<T>>(new InjectionConstructor
                (new ResolvedArrayParameter<KeyValuePair<String, IUpdater<T>>>( rp.ToArray())));
        }

    }
}
