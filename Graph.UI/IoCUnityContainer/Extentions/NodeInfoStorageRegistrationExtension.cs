﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity.Extension;
using Unity.Lifetime;
using System.Reflection;
using Unity.Builder.Strategy;
using Unity.Builder;
using Unity;
using EVT.Applications.GraphSolution.GraphInfo.Interfaces;
using EVT.Applications.GraphSolution.NodeInfoStorageManager;
using EVT.Applications.GraphSolution.NodeInfoStorageManager.Interfaces;

using EVT.Applications.Common.Components.Interfaces.ObjectLocalStorage;
using Unity.Injection;



namespace EVT.Ioc.ContainerBasedOnUnity
{
    //Расширения контейнера используются для добавления различной функциональности
    public class NodeInfoStorageRegistrationExtension<T> : UnityContainerExtension
    {
        // Для работы механизма авторегистации необходимо, чтобы класс ViewModel-и имел префикс VM
        protected override void Initialize()
        {
            //T - теперь Т класс в котором статические поля
            var asm = Assembly.GetAssembly(typeof(T));
            var asmTypes = asm.GetExportedTypes().Where(t => typeof(INodeInfo).IsAssignableFrom(t)); // выбираем классы , унаследованные от INodeInfo

            //var type = typeof(T);
            //var field = type.GetField("InfoTypes");
            //var value = field.GetValue(null);
            //var dictionary = value as IReadOnlyDictionary<String, Type>;
            //if (dictionary != null)
            //{
            //foreach (var t in dictionary)
            //{
            foreach (var type in asmTypes)
            {
                //var @interface = t.GetInterfaces().Where(i => i.Name.Contains("ILocalStorage")).SingleOrDefault();
                //if (@interface == null) continue;

                //var gp = @interface.GetGenericArguments().SingleOrDefault();
                //if (gp == null) continue;

                //var simpleType = gp.GetGenericArguments().SingleOrDefault();
                //if (simpleType == null) continue;
                ////регистрация хранилища
                //Container.RegisterType(@interface, t, t.Name, new ContainerControlledLifetimeManager());

                //var simpleType = t.Value;
                var simpleType = type;
                var typeArr = new Type[] { simpleType };
                var newManagerInterface = typeof(INodeInfoStorageManager<>).MakeGenericType(typeArr);
                var newManagerType = typeof(NodeInfoStorageManager<>).MakeGenericType(typeArr);
                var managerName = type.Name +"StorageManager";
                //регистрация манагера
                //Container.RegisterType(newManagerInterface, newManagerType, t.Key + "NodeInfoStorageManager", new ContainerControlledLifetimeManager());
                Container.RegisterType(newManagerInterface, newManagerType, managerName, new ContainerControlledLifetimeManager());
                

                //Для работы активатора менеджеров и хранилища регистрируем менеджеры с теме жи именами на другие интерфейсы
                //регистрация манагера
                Container.RegisterType(typeof(ILocalStorageManager<INodeInfo>), newManagerType, managerName, new ContainerControlledLifetimeManager());

                //simpleType = typeof(INodeInfo<>).MakeGenericType(typeArr);
                //typeArr = new Type[] { simpleType };
                //newManagerInterface = typeof(ILocalStorageManager<>).MakeGenericType(typeArr);
                //newManagerType = typeof(LocalStorageManager<>).MakeGenericType(typeArr);
                //регистрация манагера
                //Container.RegisterType(newManagerInterface, newManagerType, t.Key + "LocalStorageManager", new ContainerControlledLifetimeManager());


                //Context.Strategies.Add(new NodeInfoManagerCreationBuilderStrategy(t, newManagerType), UnityBuildStage.Creation);
            }
            //}
            //}
        }


        //неправиьно работает настройка стратегии. При создании CaptionNodeInfoManager не видно соответветствующую стратегию
        private class NodeInfoManagerCreationBuilderStrategy : BuilderStrategy
        {
            Type _storageType;
            Type _managerType; // фактическая реализация

            public override void PreBuildUp(IBuilderContext context)
            {

                if (_managerType.IsAssignableFrom(context.BuildKey.Type))
                {

                    var storageInstance = context.NewBuildUp(_storageType, "");

                    //var manager = context.Existing as INodeInfoStorageManager;
                    dynamic manager = context.Existing;
                    manager.AttachStorage(storageInstance);
                }
            }

            public NodeInfoManagerCreationBuilderStrategy(Type storageType, Type managerType)
            {
                _storageType = storageType;
                _managerType = managerType;
            }
        }
    }
}
