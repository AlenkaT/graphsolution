﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity.Extension;
using Unity.Lifetime;
using System.Reflection;
using Unity.Builder.Strategy;
using Unity.Builder;
using Unity;

namespace EVT.Ioc.ContainerBasedOnUnity
{
    //Расширения контейнера используются для добавления различной функциональности
    public class ViewAndViewModelsRegistrationExtension<TView, TViewModel> : UnityContainerExtension 
    {
        // Для работы механизма авторегистации необходимо, чтобы класс ViewModel-и имел префикс VM
        protected override void Initialize()
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies(); // получаем сборки. загруженные в текущий домен, классы которых реализуют IView и IViewModel
                                                                      //типы, реализующие IView
                                                                      //сборки, имена которых содержат "View"
            var vAssemblies = assemblies.Where(x => x.FullName.Contains("View")).ToArray();
            
            var v_vm_binding = new TypesBinding();

            foreach (var asm in vAssemblies)
            {
                
                var vmTypes = asm.GetExportedTypes().Where(x => typeof(TViewModel).IsAssignableFrom(x)).Where(x => x.Name.Contains("VM"));
                foreach (var t in vmTypes)
                {
                    var name = t.Name;
                    var position=name.IndexOf("VM");
                    var viewName = name.Substring(0, position) + "View";

                    //есть ли тип View с подходящим именем
                    var vType = vAssemblies.Select(a => a.GetExportedTypes().SingleOrDefault(type => type.Name.Equals(viewName))).SingleOrDefault(x => x != null);
                    if(vType!=null)
                    {
                        this.Container.RegisterType(typeof(TView), vType, vType.FullName, new TransientLifetimeManager());
                        // this.Container.RegisterType(typeof(TViewModel), t, t.FullName, new TransientLifetimeManager());
                        v_vm_binding.Bind(vType, t);
                        
                    }
                }

                Context.Strategies.Add(new ViewCreationBuilderStrategy(typeof(TView), v_vm_binding), UnityBuildStage.Initialization);
            }
           

        }

       private class  ViewCreationBuilderStrategy : BuilderStrategy
       {
            Type _viewType;
            TypesBinding _typeBinding;
            public override void PreBuildUp(IBuilderContext context)
            {
                if (_viewType.IsAssignableFrom(context.BuildKey.Type))
                {
                    //при запросе сздания вьюмодели, создаём вью
                    // var viewType = _mapping.GetViewType(context.BuildKey.Type);
                    var vmInstance = context.NewBuildUp(context.BuildKey.Type, "");
                    // приводим объект создания к IViewModel
                    //IView viewModel = context.Existing as IView;

                    //viewModel.AttachView(viewInstance as IView); //связывание контекста вью с инстансом вьюмодели
                }
            }

            public ViewCreationBuilderStrategy(Type viewBaseType, TypesBinding typeBinding)
            {
                _viewType = viewBaseType;
                _typeBinding = typeBinding;
            }
        }

    }
}
