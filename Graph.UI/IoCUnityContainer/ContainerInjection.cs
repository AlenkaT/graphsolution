﻿using EVT.Applications.Common.Components.Interfaces.Factories;
using EVT.Applications.Common.Components.Interfaces.ObjectLocalStorage;

using EVT.Applications.Common.Components.ObjectLocalStorage;



using EVT.Applications.GraphSolution.GraphUIViewModels.MainWindowVM;

using EVT.WPF.MVVMSupport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Unity;
using Unity.Injection;
using Unity.Lifetime;

using EVT.Applications.GraphSolution.Common.Components.Interfaces.AdaptationService;
using EVT.Applications.Common.Components.Interfaces.EventAggregator;
using EVT.Applications.Common.Components.EventAggregator;
using EVT.Applications.GraphSolution.Common.Components.Interfaces.Updater;
using EVT.Applications.Common.Components.Interfaces.Fasade;

using EVT.Applications.GraphSolution.Graph;

using EVT.Applications.GraphSolution.Fasade;


using EVT.Applications.GraphSolution.EntitiesAdaptationToView.GraphAdaptation;
using EVT.Applications.GraphSolution.GraphUIViewModels.MainWindowVM.Components;



using EVT.Applications.GraphSolution.GraphUIViewModels.MainWindowVM.Components.Updater;

using EVT.Graph.GraphBuilder.Interfaces;
using EVT.Graph.GraphBuilder;
using EVT.Applications.GraphSolution.Graph.GraphUnitTests;
using EVT.Applications.GraphSolution.StructuralConstructor.Interfaces;
using EVT.Applications.GraphSolution.StructuralConstructor;
using EVT.Applications.GraphSolution.GraphInfo.Interfaces;

using EVT.Applications.GraphSolution.VMServices.ObjectMapping;

using EVT.Applications.GraphSolution.EntitiesAdaptationToView.NodeInfoAdaptation;
using EVT.Applications.GraphSolution.NodeInfoStorageManager.Interfaces;
using EVT.Applications.GraphSolution.Components.GraphFactory;

using EVT.Applications.GraphSolution.NodeInfoAccessors;
using EVT.Applications.GraphSolution.BL.Infrastructure.Updaters;
using EVT.Applications.GraphSolution.BL.Infrastructure.Updaters.WizardCollection;

namespace EVT.Ioc.ContainerBasedOnUnity
{
    public static class ContainerInjection
    {
        readonly private static Dictionary<IUnityContainer, Application> _ContainerDictionary = new Dictionary<IUnityContainer, Application>();
        private static void SetGenericConfiguration<TData>(IUnityContainer container)
        {
            //Конфигурация контейнера - регистрация классов, экземпляры которых появляются после запроса разрешения соответствующих интерфейсов
            //регистрируем вью модель главного окна

            container.RegisterType<ILocalStorage<TData>, ObjectLocalStorage<TData>>(new ContainerControlledLifetimeManager());
            container.RegisterType<ILocalStorageManager<TData>, LocalStorageManager<TData>>(new ContainerControlledLifetimeManager());

            container.RegisterType<IFasade<TData>, AppFasade<TData>>(new ContainerControlledLifetimeManager());
            container.RegisterType<ObjectUpdaterCommands<TData>>(new ContainerControlledLifetimeManager());

            RegistrationHelper.Updaters<TData>(container, typeof(EVT.Applications.GraphSolution.BL.Infrastructure.RegestryHelpers.UpdatersRegistryHelper));

            

        }

        private static void SetConcreteConfiguration(IUnityContainer container)
        {

            container.RegisterType<ISimpleFactory<TreeGraph>, TreeGraphFactory>(new ContainerControlledLifetimeManager());
            container.RegisterType<IGraphBuilder, TreeGraphBuilder<TestNode>>(new ContainerControlledLifetimeManager());

            // Constructors
            
            //Wizard without Constructors
           // container.RegisterType<IStructuralConstructor, SimpleConstructor2>("SimpleCtor", new ContainerControlledLifetimeManager());
            container.RegisterType<IStructuralConstructor, BrigdeStructureConstructor>("BrigdeStructureCtor", new ContainerControlledLifetimeManager());

            
            ////Wizard
            // AddBeginingNodeToGraphWizard
            //container.RegisterType<IUpdater<TreeGraph>, AddBeginingNodeToGraphWizard>("AddSBNKarkasTempate", new ContainerControlledLifetimeManager(),
            //    new InjectionConstructor(new ResolvedParameter<IGraphBuilder>(), 
            //                               new ResolvedArrayParameter<IStructuralConstructor>(
            //                                    new ResolvedParameter<IStructuralConstructor>("SimpleCtor"))
            //));
            //container.RegisterType<IUpdater<TreeGraph>, AddNodeToGraphWizard<TestNode>>("AddNodeUpdater", new ContainerControlledLifetimeManager(),
            //new InjectionConstructor(new ResolvedParameter<IGraphBuilder>(), new ResolvedArrayParameter<IStructuralConstructor>(
            //new ResolvedParameter<IStructuralConstructor>("BrigdeStructureCtor"))
            //));





            //Сборка конфигураций для типа данных, для которого запускается WPF приложение

            SetGenericConfiguration<TreeGraph>(container);




            //Вью модели
            container.RegisterType<DefinitionObjectMappingManager>(new ContainerControlledLifetimeManager());
            container.RegisterType<NodeInfoMappingManager>(new ContainerControlledLifetimeManager());

            container.RegisterType<BindableBase, GraphTreeViewComponentVM<Node, TreeGraph>>("NodeTreeViewComponentVM");

            //регистрация компонентов слоя вьюмоделей
            container.RegisterType<IEventAggregator, SimpleEventAggregator>(new ContainerControlledLifetimeManager());
            container.RegisterInstance(typeof(TreeGraph)); // это входит параметом в констуктор MainWindowVM

            container.RegisterType<IAdaptationService<TreeGraph>, GraphAdaptationService>(new ContainerControlledLifetimeManager());
            container.RegisterType<MainWindowVM<TreeGraph>>(new ContainerControlledLifetimeManager());

            #region DataAccessLayer Registraation
            container.RegisterType<INodeInfoAccessor, NodeInfoAccessor>(new InjectionConstructor(new ResolvedParameter<ILocalStorage<INodeInfo>>("NodeInfoStorage")));
            
            //Регистрация хранилищ инфы и их манажеров (авторегистрация через расширение)
            container.RegisterType<ILocalStorage<INodeInfo>, ObjectLocalStorage<INodeInfo>>("NodeInfoStorage", new ContainerControlledLifetimeManager());
            //манажер, который пишет любую информацию
            container.RegisterType<ILocalStorageManager<INodeInfo>, LocalStorageManager<INodeInfo>>("NodeInfoStorageManager", new ContainerControlledLifetimeManager());
            #endregion

            //Инфовизарды регистрирует экстеншн
            RegistrationHelper.NodeInfoWizards(container, "NodeInfoStorageManager", typeof(EVT.Applications.GraphSolution.BL.Infrastructure.RegestryHelpers.NodeInfoWizardRegistryHelper));
            
        }

        private static void AddExtensions(IUnityContainer container)
        {
            container.AddNewExtension<NodeInfoStorageRegistrationExtension<EVT.Applications.GraphSolution.NodeInfoTypes.Shared.RegestryHelper>>();
            container.AddNewExtension<NodeInfoStorageRegistrationExtension<EVT.Applications.GraphSolution.NodeInfoTypes.Geometric.RegestryHelper>>();
        }


        public static void ApplicationRun<TMainWindow, TMainApp>() where TMainWindow : Window where TMainApp : Application //where TData:class
        {
            using (var Ioc = new UnityContainer())
            {
                AddExtensions(Ioc);
                //1. предварительно конфигурируем контейнер
                SetConcreteConfiguration(Ioc);
                //2. Регистрируем класс главного окна приложения, в свойство датаконтекст которого при создании экземпляра будет инжектиться экземпляр вьюмодели
                // главному окну присваивается стиль существования синглтон
                //начала отработает создание окна, потом создание вьюмодели, потом заполнение датаконтекста
                Ioc.RegisterType<TMainWindow>(new ContainerControlledLifetimeManager(), new InjectionProperty("DataContext", new ResolvedParameter<MainWindowVM<TreeGraph>>()));
                // два варианта - либо вручную создавать гл окно и гл приложение и регать инстансы
                // вариант 2:

                //запуск wpf приложения
                //2. Объект приложения

                //ресолв слоя данных
                var managers = Ioc.ResolveAll<ILocalStorageManager<INodeInfo>>();
                var storage = Ioc.Resolve<ILocalStorage<INodeInfo>>("NodeInfoStorage");
                foreach (var manager in managers)
                {
                    manager.AttachStorage(storage);
                }


                var app = Ioc.Resolve<TMainApp>();
                // app.Navigating += App_NavigatingEventHandler;
                _ContainerDictionary[Ioc] = app;
                app.Navigated += App_NavigatedEventHandler;
                app.LoadCompleted += App_LoadCompletedEventHandler;
                //3. С помощью контейнера получаем экземпляр главного окна
                Ioc.Resolve<INodeInfoAccessor>();
                var mainWindow = Ioc.Resolve<TMainWindow>();

                /* 
                * Что происходит при выполнении п.3 стр 54
                * Вызов конструктора главного окна
                * Метод InitializeComponent вызовет событие запуска процесса навигации
                * Так как контейнер инжектит свойство, то при подписке на событие изменения DataContext
                * сработает обработчик этого события
                * Далее вызывается конструктор Page
                * Перед выполнением следующей строки сработает событие завершения навигации
                * при загрузке станицы дата контекст не передаётся от фрейма и =null
                */
                app.Run(mainWindow);
            }
        }


        private static void App_LoadCompletedEventHandler(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            //происходит после события Navigeted
            // MessageBox.Show("Load Completed");
        }

        private static void App_NavigatedEventHandler(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            // MessageBox.Show("Навигация завершена "+sender.ToString()+" "+e.Content);
            var app = sender as Application;
            var view = e.Content as IView;
            if (view != null)
            {
                //при создании элемента Page его ДатаКонтекст не передаётся от родительского фрейма!!! (навигатора)
                // view.DataContext = DefaultTreeViewComponentVM.VM;
                var obj = ((FrameworkElement)e.Navigator).Tag;// вытаскиваем переданный через Tag параметр

                if (_ContainerDictionary.ContainsValue(app))
                {
                    var container = _ContainerDictionary.Single(pair => pair.Value.Equals(app)).Key;

                    var vm = container.Resolve<BindableBase>("NodeTreeViewComponentVM", new Unity.Resolution.ParameterOverride("root", obj));


                    view.DataContext = vm;
                }
            }
        }


        private static void App_NavigatingEventHandler(object sender, System.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            //sender- объект Application
            //e.Navigator - навигационный контейнер - фрейм или навигайшнвиндов
            //e.Content=null
            //MessageBox.Show("Навигация запущена ");
        }
    }
}

