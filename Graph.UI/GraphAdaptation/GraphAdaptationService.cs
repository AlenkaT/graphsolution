﻿using EVT.Applications.Common.Components.Interfaces.EventAggregator;
using EVT.Applications.Common.Components.Interfaces.Fasade;
using EVT.Applications.GraphSolution.Common.Components.Interfaces.Updater;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EVT.Applications.GraphSolution.ViewModelsBase.AdaptationService;
using EVT.Applications.GraphSolution.EventAggregatorAdaptation.VMEventArgs;
using EVT.Applications.GraphSolution.Graph;

namespace EVT.Applications.GraphSolution.EntitiesAdaptationToView.GraphAdaptation
{
    public class GraphAdaptationService : AdaptationServiceBase<GraphHierarchicalConvertor, TreeGraph, GraphHierarchicalView>
    {
        protected readonly IEventAggregator _eventAggregator;

        protected override object DoPostCreate(GraphHierarchicalView createdObject)
        {
            return new GraphHierarchicalView[] { createdObject }; // для ItemsSurce тривью
        }

        protected override void DoUpdateObject(TreeGraph targetObject, IUpdater<TreeGraph> updater, SourceParametrs sparams)
        {
            var newSource = sparams.Source as GraphHierarchicalView;
            if (newSource == null) return;
            var node = newSource.Data;

            _appFasade.UpdateItem(targetObject, updater, new SourceParametrs(node, sparams.Parametrs));
            

            if (node == null || targetObject.GetNodes().Contains(node))
            {
                //node == null -был пустой граф
                var conv = _convertor.Convert(targetObject, node);
                newSource.HierarchicalViewUpdate(conv.Data, conv.Children);
                //родитель при конвертации не цепляется
            }
            else
            {
                var parent = newSource.Parent;// as GraphHierarchicalView;
                //удалить представление
                if (parent== null) return;
                //var children = parentSource.Children.Where(x => x != newSource);
                //parentSource.HierarchicalViewUpdate(parentSource.Data, children);
                //если тут летят эксепшны, но править код!!!
                var graphviewParent= (_convertedDataDictionary[targetObject] as IEnumerable<GraphHierarchicalView>).Single();
                //ищем элемент представления родителя
                var queue = new Queue<GraphHierarchicalView>();
                queue.Enqueue(graphviewParent);
                while (queue.Count!=0)
                {
                    var item=queue.Dequeue();
                    if (item.Data==parent)
                    {
                        //найдено родительское представление , выходим
                        graphviewParent = item;
                        queue.Clear();
                        break;
                    }
                    foreach (var child in item.Children)
                    {
                        queue.Enqueue(child as GraphHierarchicalView);
                    }
                }
                
                var newChildren = graphviewParent.Children.Where(x => x.Data != newSource.Data).Cast<GraphHierarchicalView>();
                graphviewParent.HierarchicalViewUpdate(graphviewParent.Data, newChildren);
            }
            
            //_eventAggregator.PublishEvent(this, new GraphViewUpdatedEventArgs(conv, sparams.Source));
        }

       
        public GraphAdaptationService(IFasade<TreeGraph> appFasade, IUpdaterCollection<TreeGraph> updaterCollection, IEventAggregator eventAggregator) : base(appFasade, updaterCollection)
        {
            _eventAggregator = eventAggregator;
        }

    
    }
}
