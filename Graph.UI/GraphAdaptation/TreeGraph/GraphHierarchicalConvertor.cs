﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Collections.ObjectModel;
using EVT.Applications.GraphSolution.Common.Components.Interfaces.AdaptationService;
using EVT.Applications.GraphSolution.Graph;

namespace EVT.Applications.GraphSolution.EntitiesAdaptationToView.GraphAdaptation
{
    public class GraphHierarchicalConvertor : IConvertor<GraphHierarchicalView, TreeGraph>//IHierarchicalData<Node>
    {
        public GraphHierarchicalView Convert(TreeGraph obj, Object parametr = null)
        {
            var source = parametr as Node;
            var result = DoConvert(obj, source);
            //if (parentNode != null)
            //{
            //    result.SetParent(parentNode);
            //}
            //foreach (var child in result.Children)
            //{
            //    (child as GraphHierarchicalView).SetParent(result);
            //}
            return result;
        }

        protected GraphHierarchicalView DoConvert(TreeGraph graph, Node rootNode = null, Node parent = null)
        {
            var childrenView = new List<GraphHierarchicalView>();
            //Не рантайм заполение тривью
            if (rootNode == null)
            {
                rootNode = graph.GetChildren().FirstOrDefault(); // получаем корневой узел
                if (rootNode == null)
                {
                    //пустой граф вью
                    return new GraphHierarchicalView(null, childrenView, null);
                }
                parent = null;
            }
            else
            {
                if (parent == null)
                {
                    var edge = graph.GetEdges().Where(x => x.To == rootNode).SingleOrDefault();
                    parent = (edge == null) ? null : edge.From;
                }
            }

            var children = graph.GetChildren(rootNode);

            foreach (var child in children)
            {
                var childView = DoConvert(graph, child, rootNode);
                childrenView.Add(childView);
            }
            return new GraphHierarchicalView(rootNode, childrenView, parent);
        }
    }
}
