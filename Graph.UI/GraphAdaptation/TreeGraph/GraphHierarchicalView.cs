﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Collections.ObjectModel;
using EVT.Applications.GraphSolution.ViewModelsBase.DataInterfaces;
using EVT.Applications.GraphSolution.Graph;
using EVT.WPF.MVVMSupport;

namespace EVT.Applications.GraphSolution.EntitiesAdaptationToView.GraphAdaptation
{
    public class GraphHierarchicalView : BindableBase, IHierarchicalData<Node>
    {
        private Node _data;
        private Node _parent;

        private IEnumerable<IHierarchicalData<Node>> _children;
        public Node Data => _data;
        public IEnumerable<IHierarchicalData<Node>> Children => _children;
        public Node Parent => _parent;

        internal void HierarchicalViewUpdate(Node newData, IEnumerable<IHierarchicalData<Node>> newChildren)
        {
            
            SetProperty(ref _data, newData, nameof(Data));

            SetProperty(ref _children, newChildren, nameof(Children));
        }

        public GraphHierarchicalView(Node node, IEnumerable<IHierarchicalData<Node>> children, Node parent)
        {
           _data = node;
           _children = children;
           _parent = parent;
        }

    }
}
