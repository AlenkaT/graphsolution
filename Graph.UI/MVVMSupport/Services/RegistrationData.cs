﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EVT.WPF.MVVMSupport.Services
{
    public struct RegistrationData
    {
        public readonly String Name;

        public RegistrationData(String name)
        {
            Name = name;
        }
    }
}
