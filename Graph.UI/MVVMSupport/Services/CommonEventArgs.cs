﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EVT.WPF.MVVMSupport.Services
{
    public class CommonEventArgs:EventArgs
    {
        public Object Parameter { get; }

        public CommonEventArgs(Object parameter)
        {
            Parameter = parameter;
        }
    }
}
