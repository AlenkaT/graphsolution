﻿using System;
using System.Windows;
using System.Windows.Input;

namespace EVT.WPF.MVVMSupport.AttachedDependencyProperty.CommandsSupport
{
    public static class CommandsBindingsDP
    {
        
        // Данный класс определяет свойство зависимости, присоединяя которое к элементу управления
        // верхнего уровня мы предоставляем его для всего логического дерева элементов
        //Элемент замль присоединяет данное свойство и биндится к свойству своего контекста типа CommandBindingCollection 

        public static DependencyProperty AttachedCommandBindingsProperty =
            DependencyProperty.RegisterAttached("AttachedCommandBindings", typeof(CommandBindingCollection), typeof(CommandsBindingsDP),
                new PropertyMetadata(null, OnAttachedCommandBindingChanged));

        // Для установки значения свойста замль-анализатору требуюется вызов данных методов

        public static void SetAttachedCommandBindings(UIElement element, CommandBindingCollection value)
        {
            if (element != null)
                element.SetValue(AttachedCommandBindingsProperty, value);
        }
        public static CommandBindingCollection GetAttachedCommandBindings(UIElement element)
        {
            return (element != null ? (CommandBindingCollection)element.GetValue(AttachedCommandBindingsProperty) : null);
        }
        // метод, вызываемый при изменении значения свойста зависимости
        private static void OnAttachedCommandBindingChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            UIElement element = sender as UIElement;
            if (element != null)
            {
                CommandBindingCollection bindings = e.NewValue as CommandBindingCollection;
                if (bindings != null)
                {
                    element.CommandBindings.AddRange(bindings);
                }
            }
        }

    }

}


