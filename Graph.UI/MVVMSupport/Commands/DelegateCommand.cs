﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace EVT.WPF.MVVMSupport
{
    //Отличие от стандартного класса в том что нет типизации параметром аргумента события
    public class DelegateCommand: CommandBase
    {
        Action<Object> execute;
        Func<Object, bool> canExecute;
        public DelegateCommand(Action<Object> execute, Func<Object, bool> canExecute)
        {
            if (execute == null)
            {
                throw new ArgumentNullException("execute");
            }
            
            this.execute = execute;
            //if (canExecute == null) this.canExecute = AlwaysCanExecute;// так не делать , так как если вызов будут с двумя параметрами и второй=null, произойдёт его замещение и конструкктор выполнится
            // else this.canExecute = canExecute;
            if (canExecute == null)
            {
                throw new ArgumentNullException("canExecute");
            }
            this.canExecute = canExecute;
        }

        public DelegateCommand(Action<Object> execute): this(execute, DelegateCommand.AlwaysCanExecute) { }

        public override void Execute(Object param)
        {
            execute(param);
        }
        public override bool CanExecute(Object param)
            // проверка на null вроде не требуется, так как она была в конструкторе
        {
            return canExecute(param);
        }
        static bool AlwaysCanExecute(Object param)
        {
            return true;
        }
        public void Destroy()
        {
            this.canExecute = _ => false;
            this.execute = _ => { return; };
        }
        
    }
}
