﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace EVT.WPF.MVVMSupport
{
    public interface IDelegateCommand: ICommand
    {
        void RaiseCanExecuteChanged();
    }
}
