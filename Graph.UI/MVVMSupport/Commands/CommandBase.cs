﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace EVT.WPF.MVVMSupport
{
    public abstract class CommandBase: IDelegateCommand
    {
        abstract public void Execute(Object param);

        abstract public bool CanExecute(Object param);
        
        public event EventHandler CanExecuteChanged;
        public void RaiseCanExecuteChanged() //метод вызывается из вью-модел и инициирует событие CanExecuteChanged, уведомляющее источники команд, что значение, возвращаемое CanExecute() изменилось
        {
            //метод инициирующий событие CanExecuteChanged
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }

       // http://qaru.site/questions/174019/what-is-the-actual-task-of-canexecutechanged-and-commandmanagerrequerysuggested
        //CanExecuteChanged уведомляет все источники команд(например, Button или MenuItem), привязанные к этому ICommand, что значение, возвращаемое CanExecute, изменилось. Источники команд заботятся об этом, потому что обычно им необходимо обновить свой статус(например, Button отключит себя, если CanExecute() возвращает false).
        //Событие CommandManager.RequerySuggested возникает, когда CommandManager считает, что что-то изменилось, что повлияет на способность команд выполнять. Например, это может быть изменение фокуса.Оказывается, это событие очень сильно срабатывает.
        //Таким образом, в сущности, этот бит кода гарантирует, что всякий раз, когда менеджер команд думает, что возможность выполнения команды изменилась, команда поднимет CanExecuteChanged, даже если она фактически не изменилась.

        //Мне действительно не нравится этот подход к внедрению ICommand.CanExecuteChanged - он чувствует себя ленивым и не совсем надежным. Я предпочитаю гораздо более тонкий подход, когда команда предоставляет метод (например, RaiseCanExecuteChanged()), который вы можете вызвать для повышения CanExecuteChanged, затем вы вызываете это в соответствующие моменты из своей модели просмотра.

        //Например, если у вас есть команда, которая удаляет выбранного клиента, у него будет обработчик CanExecute(), который возвращает true, только если выбран клиент.Поэтому вы вызываете RaiseCanExecuteChanged всякий раз, когда изменяется выбранный клиент.
    }
}
