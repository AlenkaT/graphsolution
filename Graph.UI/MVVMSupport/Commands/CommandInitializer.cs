﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace EVT.WPF.MVVMSupport
{
    public static class CommandInitializer
    {
        public static CommandBindingCollection AttachApplicationCommands(InitializingCommand[] cmds, Type type)
        {
            //ICommand command=ApplicationCommands.Save
            //Create a command binding for the Save command
            //public CommandBinding(ICommand command, ExecutedRoutedEventHandler executed, CanExecuteRoutedEventHandler canExecute);
            //Создаём привязку и связываем с командой методы SaveExecuted и SaveCanExecute
            var commandBindings = new CommandBindingCollection();
            foreach (InitializingCommand cmd in cmds)
            {
                CommandBinding binding = new CommandBinding(cmd.command, cmd.executed, cmd.canExecute);
                //Регистрация привязки в этом классе. Зачем ?
                CommandManager.RegisterClassCommandBinding(type, binding);
                //Добавление созданной и зарегистрированной привязки в коллекцию
                commandBindings.Add(binding);
            }
            return commandBindings;
        }
    }

    public class InitializingCommand
    {
        // класс сделан, чтобы создать массив команд
        internal readonly ICommand command;
        internal readonly ExecutedRoutedEventHandler executed;
        internal readonly CanExecuteRoutedEventHandler canExecute;

        public InitializingCommand(ICommand command, ExecutedRoutedEventHandler executed, CanExecuteRoutedEventHandler canExecute)
        {
            this.command = command;
            this.executed = executed;
            this.canExecute = canExecute;
        }
    }
}
