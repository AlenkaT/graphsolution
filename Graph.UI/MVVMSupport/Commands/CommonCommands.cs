﻿using System;

using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using System.Windows.Controls;



namespace EVT.WPF.MVVMSupport
{
    public static class CommonCommands
    {
        
        public static void EmptyExecute(object sender, ExecutedRoutedEventArgs e) { }

        public static void CanBeExecute(object sender, CanExecuteRoutedEventArgs e)
        { e.CanExecute = true; }

    }
}
