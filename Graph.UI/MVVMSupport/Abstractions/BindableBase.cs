﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.ComponentModel;

namespace EVT.WPF.MVVMSupport
{
    public abstract class BindableBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChanged?.Invoke(this, e);//не является потокобезопасным вызовом
        }
        protected void RaisePropertyChanged(ExtendPropertyChangedEventArgs e)
        {
            PropertyChanged?.Invoke(this, e);//не является потокобезопасным вызовом
        }

        protected void RaisePropertyChanged(String propertyName)
        {
            RaisePropertyChanged(new PropertyChangedEventArgs(propertyName));//не является потокобезопасным вызовом
        }
        protected virtual Boolean SetProperty<T>(ref T storage, T newValue, String propertyName, Boolean resetValue=false)
        {

            if (!resetValue && object.Equals(storage, newValue)) return false;
         
                var oldvalue = storage;
                storage = newValue;
                RaisePropertyChanged(new ExtendPropertyChangedEventArgs(propertyName, oldvalue, newValue));


            return true;

        }
    }
}
