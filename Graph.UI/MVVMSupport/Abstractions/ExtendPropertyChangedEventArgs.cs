﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.ComponentModel;

namespace EVT.WPF.MVVMSupport
{
    public class ExtendPropertyChangedEventArgs : PropertyChangedEventArgs
    {
        public Object OldValue { get; }
        public Object NewValue { get; }


        public ExtendPropertyChangedEventArgs(String propertyName,  object oldValue, object newValue) : base(propertyName)
        {
            OldValue = oldValue;
            NewValue = newValue;
        }
    }
}
