﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace EVT.WPF.MVVMSupport
{
    public delegate TProduct StaticFactoryWithConverter<TProduct>(Object param=null);

    public class ItemsSourceCollectionViewModel<TProduct>:BindableBase
    {

        // IFactory<TProduct, TProductParametrs> factory; // Предполагает внедрение через конструктор
        //Базовый класс если необходимо работать с ItemsSource для UIElements

        #region Fields

        private TProduct selItem;
        private StaticFactoryWithConverter<TProduct> staticFactory;      
        private ObservableCollection<TProduct> collect; // если тут делать интерфейс, то при работе с реальными классами будет невозможно обратиться к полям класса

        #endregion

        #region Properties

        public ObservableCollection<TProduct> Collection { get { return collect; } }
        public IDelegateCommand AddCommand { get; }
        public IDelegateCommand RemoveCommand { get; }
        public TProduct SelectedItem
        {
            get { return selItem; }
            set { if (SetProperty<TProduct>(ref selItem, value, nameof(SelectedItem))) return; }
        }

        #endregion
        //Метод, назначающий полям item данные из obj , переопределяется в зависимости от того, какие данные передаются через CommandParametr

        #region PublicInterface // Используются для привязки комманд Command
        public void Add(Object obj) { AddItem(obj); }

        public Boolean CanAdd(Object obj)
        {
            return CanAddItem(obj);}

        public Boolean CanDelete(Object obj) { return CanDeleteItem(obj); }
        public void Delete(Object obj) { DeleteItem(obj); }

        public void AddProduct(TProduct product)
        {AddItem(product); }
        #endregion

        #region ProtectedVirtualMethods
        protected virtual void AddItem(TProduct product)
        { collect.Add(product);}

        protected virtual Boolean CanAddItem(Object obj)
        {
            return staticFactory != null;
        }
        protected virtual void AddItem(Object obj) // Определение DelegateCommand такое, что функция должна принимать параметр Object
        {
            //obj - параметр, передаваемый через CommandParametr из View
            //здесь должны быть операции с моделью
            var adding=staticFactory(obj);
            collect.Add(adding);
        }
        protected virtual void DeleteItem(Object obj) // Определение DelegateCommand такое, что функция должна принимать параметр Object
        {
            //obj - параметр, переваемый в CommandParametr, если CommandParametr отсутствует должен передаться null
            //здесь должны быть операции с моделью
            if (obj == null) { collect.Remove(selItem); }
            else
            {
                try
                {
                   //может вызвать эксепт
                    collect.Remove((TProduct)obj);
                }
                catch
                {
                }
            }
        }

        protected virtual Boolean CanDeleteItem(Object obj)
        { return collect.Count != 0; }
        #endregion
        #region Constructor
        public ItemsSourceCollectionViewModel(StaticFactoryWithConverter<TProduct> staticFactory=null, params TProduct[] array)
        {

            this.staticFactory = staticFactory;
            
            if (array == null) collect = new ObservableCollection<TProduct>();
            else
                collect = new ObservableCollection<TProduct>(array);
           
            RemoveCommand = new DelegateCommand(Delete, CanDelete);
            AddCommand = new DelegateCommand(Add, CanAdd);
            
        }
#endregion
    }
}
