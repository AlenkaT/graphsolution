﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EVT.WPF.MVVMSupport
{
    public interface IView
    {
        BindableBase DataContext { get; set; } // т.о. у окон и страниц два свойста DataContext
        event RoutedEventHandler Loaded;
        event RoutedEventHandler Unloaded;
        void Activate();
    }
}
