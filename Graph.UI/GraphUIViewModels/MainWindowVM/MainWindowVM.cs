﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;

using EVT.Applications.GraphSolution.ViewModelsBase.MainWindowVM;
using EVT.Applications.GraphSolution.EventAggregatorAdaptation.VMEventArgs;
using EVT.Applications.Common.Components.Interfaces.EventAggregator;
using EVT.Applications.Common.Components.Interfaces.EventAggregatorEventArgs;
using EVT.Applications.GraphSolution.Common.Components.Interfaces.AdaptationService;

using EVT.Applications.GraphSolution.VMServices.ObjectMapping;
using EVT.Applications.GraphSolution.EntitiesAdaptationToView.NodeInfoAdaptation;

namespace EVT.Applications.GraphSolution.GraphUIViewModels.MainWindowVM
{
    //TData - тип объекта, с которым работает приложение (настраивается до инициализации контрейнера)
    public class MainWindowVM<T> : MainWindowVMBase<T> //Добавляем eventAggregator к базовой вью модели
    {
        // БАЗОВАЯ ВЬЮМОДЕЛЬ ДОЛЖНА СОДЕРЖАТЬ ВСЕ СВОЙСТВА, К КОТОРЫМ ПРИВЯЗЫВАЕТСЯ ВЬЮ //
        #region Fields
        protected readonly IEventAggregator _eventAggregator;
        protected readonly HashSet<RequestOperationEventArgs> _pullRequest = new HashSet<RequestOperationEventArgs>();

        #endregion


        #region Private Methods

        protected void _PublishMessage<Targs>(Targs args) where Targs : RequestOperationEventArgs
        {
            //Передаем модели запрос на создание
            _pullRequest.Add(args);
            _eventAggregator.PublishEvent(this, args);
        }

        #endregion

        #region Listner Interface Implimentation

       

        //void CreationNewItemFailedEventHandler(object sender, AggregatorEventArgs e)
        //{
        //    //if (e.NewItem is TData && _answerKey.Contains(e.AnswerKey))// тут низя использовать as
        //    //{
        //    //    var obj = (TData)e.NewItem;
        //    //    _objectCollection.Add(new ObjectMapping<TData>(ObjectName, ObjectDefinition, obj));

        //    //    //Оповещаем вью , что надо затянуть новый элемент
        //    //    RaisePropertyChanged(nameof(ObjectCollection));
        //    //}
        //}

        //void GraphViewUpdatedEventHandler(object sender, AggregatorEventArgs e)
        //{
        //    var args = e as GraphViewUpdatedEventArgs;
        //    if (args == null) return;
        //    var mappingCollection = ObjectCollection.Where(x => x.MappedObject == args.OldValue).ToArray();
        //    foreach (var map in mappingCollection)
        //    {
        //        _ObjMapManager.SetObjectInToMapping(map, args.NewValue);
        //    }
        //    RaisePropertyChanged(nameof(SelectedTabItem));
        //}

        void SelectedTreeViewItemdEventHandler(object sender, AggregatorEventArgs e)
        {
            var args = e as ItemChangedEventArgs;
            if (args == null) return;

            //Вызвать установку нового значения в обертку PropertyGrid

            PropertyGridSelectedObject.SetObjectIntoMapping(args.NewValue);
        }
        #endregion



        #region Constructor

        //public MainWindowViewModel(IEnumerable<MenuInfo> projecTypes)
        public MainWindowVM(IEventAggregator eventAggregator, Type dataType, IAdaptationService<T> adaptationService,
            DefinitionObjectMappingManager mappingManager,
            MappingCollection nodeinfoMappingCollection)
            : base(dataType, adaptationService, mappingManager, nodeinfoMappingCollection)//ITreeViewComponent<T> treeViewComponentVM)
        {
            _eventAggregator = eventAggregator;
            //подписка на изменение выделенного элемента
            eventAggregator.SubscribeListener<ItemChangedEventArgs>(this, SelectedTreeViewItemdEventHandler);
        }

        #endregion





    }
}


