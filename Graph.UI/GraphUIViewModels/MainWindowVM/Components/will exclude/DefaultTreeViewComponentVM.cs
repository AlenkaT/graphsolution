﻿using EVT.Applications.GraphSolution.ViewModelsBase.Components;
using EVT.Applications.GraphSolution.ViewModelsBase.DataInterfaces;

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace EVT.Applications.GraphSolution.GraphUIViewModels.MainWindowVM.Components
{
    
    public class DefaultTreeViewComponentVM : TreeViewComponentVMBase<String>
    {
        public static readonly DefaultTreeViewComponentVM VM;
        static DefaultTreeViewComponentVM()
        {
            int deep = 0;
            VM = new DefaultTreeViewComponentVM(new StringHierarchical("TestRoot", new List<string>() { "Child1", "Child2", "Child3" }, ref deep, 5));
        }

          #region C-tor
        private DefaultTreeViewComponentVM(StringHierarchical rootSource):base(new List<IHierarchicalData<String>>() { rootSource})
        {
            IEnumerable<IHierarchicalData<String>> value = new Collection<StringHierarchical>() { rootSource}; // конвертация из List<> не возможна, так как интрефейс IList<T> инвариантен!

            SelectedTreeViewItem = rootSource;
        }
        #endregion 
    }
}