﻿using EVT.Applications.GraphSolution.ViewModelsBase.DataInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.GraphUIViewModels.MainWindowVM.Components
{
    public class StringHierarchical : IHierarchicalData<String>
    {
        List<StringHierarchical> _list;

        public String Data { get; }
        public IEnumerable<IHierarchicalData<String>> Children { get { return _list; } }

        public String Parent
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        internal StringHierarchical(String rootName, List<String> children, ref int deep, int maxDeep)
        {
            deep++;
            Data = rootName;
            _list = new List<StringHierarchical>();
            if (children != null && deep <= maxDeep)
            {
                foreach (var txt in children)
                {
                    _list.Add(new StringHierarchical(txt, new List<string>() { txt + "." + deep }, ref deep, maxDeep));
                }
            }
            else
            { deep -= maxDeep; }
        }


    }
}
