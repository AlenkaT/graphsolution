﻿using EVT.Applications.Common.Components.Interfaces.EventAggregator;
using EVT.Applications.Common.Components.Interfaces.EventAggregatorEventArgs;
using EVT.Applications.GraphSolution.EventAggregatorAdaptation.VMEventArgs;
using EVT.Applications.GraphSolution.GraphUIViewModels.MainWindowVM.Components.Updater;
using EVT.Applications.GraphSolution.ViewModelsBase.Components;
using EVT.Applications.GraphSolution.ViewModelsBase.DataInterfaces;
using EVT.WPF.MVVMSupport;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace EVT.Applications.GraphSolution.GraphUIViewModels.MainWindowVM.Components
{
    //к базовому классу подключается ивент агрегатор и команды
    //в терминологии графа TData=Node, TObject=TreeGraph
    public class GraphTreeViewComponentVM<TData, TObject> : TreeViewComponentVMBase<TData>
    {
        protected readonly IEventAggregator _eventAggregator;
        protected CommandBindingCollection _commandBindings;

        public CommandBindingCollection CommandBindings => _commandBindings;
        public ObjectUpdaterCommands<TObject> ObjectUpdaterCommands { get; }


        #region Listner Implementation
        void GraphViewUpdatedEventHandler(object sender, AggregatorEventArgs e)
        {
            var args = e as GraphViewUpdatedEventArgs;
            if (args == null) return;
            var newvalue = args.NewValue as IEnumerable<IHierarchicalData<TData>>;
            if (newvalue != null)
            {
                RootSource = newvalue; // вызывает обновления всего графа
                return;
            }
        }
        #endregion

        #region Raise events
        private void SelectedTreeViewItemChangedEventHandler(Object sender, PropertyChangedEventArgs e)
        {
            var args = e as ExtendPropertyChangedEventArgs;
            if (args != null && args.PropertyName == nameof(SelectedTreeViewItem))
            //пересылаем событие через ивентагрегатор другой вьюмодели
            {
                var nodeView = args.NewValue as IHierarchicalData<TData>;
                if (nodeView != null)
                {
                    _eventAggregator.PublishEvent(sender, new ItemChangedEventArgs(null, nodeView.Data));
                }
            }
        }

        #endregion

        #region C-tor
        public GraphTreeViewComponentVM(IEnumerable<IHierarchicalData<TData>> root, IEventAggregator eventAggregator, ObjectUpdaterCommands<TObject> commands) : base(root)
        {
            _eventAggregator = eventAggregator;
            ObjectUpdaterCommands = commands;
            _commandBindings = new CommandBindingCollection(commands.CommandBindings); // для собирания общей коллекции команд вью в одном месте (если в дальнейшем будет поступать несколько объектов, содержащих коллекции привязок
            //подписка на обновление графа
            eventAggregator.SubscribeListener<GraphViewUpdatedEventArgs>(this, GraphViewUpdatedEventHandler);
            this.PropertyChanged += SelectedTreeViewItemChangedEventHandler;

        }
        #endregion
    }
}
