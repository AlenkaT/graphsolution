﻿using System;

using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using System.Windows.Controls;
using EVT.WPF.MVVMSupport;
using EVT.Applications.GraphSolution.Common.Components.Interfaces.AdaptationService;
using EVT.Applications.GraphSolution.Common.Components.Interfaces.Updater;
using EVT.Applications.GraphSolution.ViewModelsBase.ComponentsVMInterfaces;

namespace EVT.Applications.GraphSolution.GraphUIViewModels.MainWindowVM.Components.Updater
{
    // T - исходный тип объекта, который потом конвертится для отображения в тривью
    // Тут сосредоточены команды (операции), которые предполагается выполнять приложению - это часть вьюмодели!
    public class ObjectUpdaterCommands<T>
    {
        #region Fields

        private readonly CommandBindingCollection commandBindings;

        readonly IAdaptationService<T> _adaptationService;
        
        #endregion

        #region Properties
        public CommandBindingCollection CommandBindings => commandBindings; // для привязки ко View

        // Делегаты команд
        public RoutedUICommand AddNode { get; }
        public RoutedUICommand AddRootNode { get; }
        public RoutedUICommand RemoveNode { get; }

        #endregion

        #region Events

        //public static event EventHandler<ElementCommandsEventArgs> ElementCreating;
        //public static event EventHandler<ElementCommandsEventArgs> ElementTypeSetting;

        #endregion

        //#region Methods
        ////private static void OnRootTypeSetting(Object sender, ElementCommandsEventArgs e)
        ////{
        ////    RootTypeSetting?.Invoke(sender, e);
        ////}

        //private static void OnElementCreating(Object sender, ElementCommandsEventArgs e)
        //{
        //    ElementCreating?.Invoke(sender, e);
        //}

        //private static void OnElementTypeSetting(Object sender, ElementCommandsEventArgs e)
        //{
        //    ElementTypeSetting?.Invoke(sender, e);
        //}
        //#endregion



        #region CommandHandlers
        #region AddRootNode
        void AddRootNodeExecute(object sender, ExecutedRoutedEventArgs e)
        {
            var treeView = e.Source as TreeView;
            if (treeView == null) return;
            var root = treeView.ItemsSource;

            // сюда попадает датаконтекст item-а

            _adaptationService.UpdateObject(root, "AddNodeToGraphWizard", new SourceParametrs(treeView.Items.CurrentItem));

        }

        void CanAddRootNodeExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            //В метод CanExecute CommandTarget не передаётся!!!!!!!!!!!!!!!! все проверки для привидений типов выполнять в Execute
            //e.Source - элемент, который в данный момент находится в фокусе! 
            // e.OriginalSource  - первичный источник (элемент, который непосредственно активен) - может быть любой внутренний элемент ячейки таблицы
            //e.Source - конечный источник, всплытия команды пузырьком
            //в данном случае использовать CommandTarget не нужно, так как может перебить проверный Source

            ////DataGrid datagrid = (e.Source as DataGrid);
            ////if (datagrid == null) return;
            ////e.CanExecute = !AttachedMembers.GetCanCreateRoot(datagrid);
            //if (e.Parameter == null) return;

            var treeView = e.Source as TreeView;

            e.CanExecute = true;
        }
        #endregion

        #region AddNode
        void AddNodeExecute(object sender, ExecutedRoutedEventArgs e)
        {
            var treeView = e.Source as TreeView;
            if (treeView == null) return;
            var root = treeView.ItemsSource;

            // сюда попадает датаконтекст item-а
            if (treeView.SelectedItem != null)
            {
                var countNode = 2; // для подключенного визарда - количество пролетов
                                   //В treeView.SelectedValue лежит нода
                                   //не надо обновлять весь граф, так как это вызывает перезагрузку всего тривью   
                _adaptationService.UpdateObject(root, "AddNodeToGraphWizard", new SourceParametrs(treeView.SelectedItem, countNode));

            }
        }

        void CanAddNodeExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            //В метод CanExecute CommandTarget не передаётся!!!!!!!!!!!!!!!! все проверки для привидений типов выполнять в Execute
            //e.Source - элемент, который в данный момент находится в фокусе! 
            // e.OriginalSource  - первичный источник (элемент, который непосредственно активен) - может быть любой внутренний элемент ячейки таблицы
            //e.Source - конечный источник, всплытия команды пузырьком
            //в данном случае использовать CommandTarget не нужно, так как может перебить проверный Source

            ////DataGrid datagrid = (e.Source as DataGrid);
            ////if (datagrid == null) return;
            ////e.CanExecute = !AttachedMembers.GetCanCreateRoot(datagrid);
            //if (e.Parameter == null) return;

            var treeView = e.Source as TreeView;

            e.CanExecute = treeView != null && treeView.SelectedItem != null;
        }
        #endregion

        #region RomoveNode
        void RemoveNodeExecute(object sender, ExecutedRoutedEventArgs e)
        {
            var treeView = e.Source as TreeView;
            if (treeView == null) return;
            var root = treeView.ItemsSource;
            ; // сюда попадает датаконтекст item-а
            if (treeView.SelectedItem != null)
            {
                _adaptationService.UpdateObject(root, "RemoveNodeFromGraphUpdater", new SourceParametrs(treeView.SelectedItem));
            }
        }

        void CanRemoveNodeExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var treeView = e.Source as TreeView;

            e.CanExecute = treeView != null && treeView.SelectedItem != null;

            //TODO В тут должны быть проверки применимоти команды 
        }
        #endregion

        #endregion

        #region Constructor



        public ObjectUpdaterCommands(IAdaptationService<T> adaptationService)
        {
            var thisType = this.GetType();

            AddRootNode = new RoutedUICommand("Добавить узел", "AddRootNode", thisType);
            AddNode = new RoutedUICommand("Добавить узел", "AddNode", thisType);
            RemoveNode = new RoutedUICommand("Удалить узел", "DeleteNode", thisType);

            var initCmds = new List<InitializingCommand>
            {
                new InitializingCommand(AddRootNode, AddRootNodeExecute, CanAddRootNodeExecute),
                new InitializingCommand(AddNode, AddNodeExecute, CanAddNodeExecute),
                new InitializingCommand(RemoveNode, RemoveNodeExecute, CanRemoveNodeExecute),
            };

            commandBindings = CommandInitializer.AttachApplicationCommands(initCmds.ToArray(), thisType);

            //TODO убрать коллекцию апдейтеров в адаптайшнсервис!
            _adaptationService = adaptationService;
           
        }

        #endregion
    }
}
