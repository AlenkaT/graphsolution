﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.AssociatedConstructionTemplateInfoTypes
{

    public class AssociatedConstructionTemplateBase
    {
        public String TemplateName { get; }
        
        public AssociatedConstructionTemplateBase(String templateName)
        {
            TemplateName = templateName;
        }
    }


    public class SBNKarkasTemplate:AssociatedConstructionTemplateBase
    {
        public Int16 ItemsCount { get; }
        public Single ItemDiametr { get; }
        public Single KarkasRadius { get; }
        public SBNKarkasTemplate(Int16 count, Single itemDiametr, Single karkasRadius): base("Каркас КР")
        {
            ItemsCount = ItemsCount;
            ItemDiametr = itemDiametr;
            KarkasRadius = karkasRadius;
        }
    }
}
