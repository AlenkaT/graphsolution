﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using VU = EVT.Applications.Verification.Utilities;
using System.Reflection;




namespace EVT.Applications.ExeptionsGenerators
{
    public abstract class ExceptionsGeneratorBase
    {
        static ExceptionsGeneratorBase()
        {
            //TODO  сделать чтение из xml
            NotEmptyStringMessage = "Значение параметра НЕ должно быть пустой строкой";
            PositiveParametrMessage = "Значение параметра должно быть положительным числом";
            NonNegativeParametrMessage = "Значение параметра должно быть целым и НЕ ОТРИЦАТЕЛЬНЫМ числом";
            NotNullParametrMessage = "Указатель на параметр НЕ должн иметь значение NULL";
            IncorrectParametrTypeMessage = "Тип данных параметра не соответствует ожидаемому ";
            NotNumericParametrMessage = "Параметр не является числом";
            ContainerDoesNotContainItemMessage = "Элемент не содержится в коллекции";
        }
        protected enum ArgRangeCondition : byte
        {
            EmptyString,
            Positive,
            NonNegative
        }
        protected static String NotEmptyStringMessage { get; set; }
        protected static String PositiveParametrMessage { get; set; }
        protected static String NonNegativeParametrMessage { get; set; }
        protected static String NotNullParametrMessage { get; set; }
        protected static String IncorrectParametrTypeMessage { get; set; }
        protected static String NotNumericParametrMessage { get; set; }
        protected static String ContainerDoesNotContainItemMessage { get; set; }


        protected static bool TryGetArgumentNullException(object parametr, String parametrName, ref Exception ex, String message = "")
        {
            if (parametr == null)
            {
                ex = new ArgumentNullException(parametrName, NotNullParametrMessage + " " + message);
                return true;
            }
            return false;
        }

        //TODO проверить вызовы этой функции, проверить condition - где передается Double, где Integer!
        protected static bool TryGetArgumentOutOfRangeException(object parametr, String parametrName, ArgRangeCondition condition, ref Exception ex, String message = "")
        {
            var result = false;
            switch (condition)
            {
                case ArgRangeCondition.EmptyString:
                    if ((String)parametr == String.Empty)
                    {
                        message = NotEmptyStringMessage + message;
                        result = true;
                    }
                    break;

                case ArgRangeCondition.NonNegative:
                    if (VU.IsNumericNegative(parametr))
                    {
                        message = NonNegativeParametrMessage + message;
                        result = true;
                    }
                    break;
                case ArgRangeCondition.Positive:
                    if (!VU.IsNumericPositive(parametr))
                    {
                        message = PositiveParametrMessage + message;
                        result = true;
                    }
                    break;

            }

            if (result)
            {
                ex = new ArgumentOutOfRangeException(parametrName, parametr, message);
            }
            return result;
        }

        protected static InvalidOperationException GetInvalidOperationException(String message = "", Exception innerException = null)
        {
            var ex = (innerException == null) ? new InvalidOperationException(message) : new InvalidOperationException(message, innerException);
            return ex;
        }

        protected static NotImplementedException GetNotImplementedException(String message = "", Exception innerException = null)
        {
            var ex = (innerException == null) ? new NotImplementedException(message) : new NotImplementedException(message, innerException);
            return ex;
        }

        protected static Boolean TryGetArgumentException(Object parametr, String prmrName, Type expactedType, ref Exception ex, String message = "")
        {
            if (parametr==null)
            {
                ex = new ArgumentException(NotNullParametrMessage + expactedType + message, prmrName);
                return true;
            }
            if( parametr.GetType() != expactedType)
            {
                ex = new ArgumentException(IncorrectParametrTypeMessage + expactedType + message, prmrName);
                return true;
            }
            return false;

        }

        protected static Boolean TryGetArgumentException(IEnumerable parametrs, IEnumerable<String> prmrNames, IEnumerable<Type> expactedTypes, ref Exception ex, String message = "")
        {
            Int32 i = 0;
            foreach (var prmr in parametrs)
            {

                var currentType = prmr.GetType();
                if (prmr==null ||
                    !currentType.IsSubclassOf(expactedTypes.ElementAt(i)) &&
                    currentType !=expactedTypes.ElementAt(i) 
                    )
                {
                    ex = new ArgumentException(IncorrectParametrTypeMessage + expactedTypes.ElementAt(i) + message, prmrNames.ElementAt(i));
                    return true;
                }
                i++;
            }
            return false;
        }


        protected static Boolean TryGetArgumentException(IEnumerable parametrs, IEnumerable<String> prmrNames, Type expactedType, ref Exception ex, String message = "")
        {
            Int32 i = 0;
            foreach (var prmr in parametrs)
            {

                var currentType = prmr.GetType();
                if (prmr == null ||
                    !currentType.IsSubclassOf(expactedType) &&
                    currentType != expactedType
                    )
                {
                    ex = new ArgumentException(IncorrectParametrTypeMessage + expactedType + message, prmrNames.ElementAt(i));
                    return true;
                }
                i++;
            }
            return false;
        }

        protected static Boolean TryGetArgumentException(Object parametr, String prmrName, ref Exception ex, String message = "")
        {

            if (!Microsoft.VisualBasic.Information.IsNumeric(parametr))
            {
                ex = new ArgumentException(NotNumericParametrMessage + message, prmrName);
                return true;
            }

            return false;
        }

        protected static Boolean TryGetArgumentException(IEnumerable parametrs, IEnumerable<String> prmrNames, ref Exception ex, String message = "")
        {
            Int32 i = 0;
            foreach (var prmr in parametrs)
            {
                if (!Microsoft.VisualBasic.Information.IsNumeric(prmr))
                {
                    ex = new ArgumentException(NotNumericParametrMessage + message, prmrNames.ElementAt(i));
                    return true;
                }
                i++;
            }
            return false;
        }
    }
}
