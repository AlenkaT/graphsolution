﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.Common.Helpers.ExceptionsGenerator
{
    internal class ExceptionMessages
    {
        static ExceptionMessages _inst;
        internal static ExceptionMessages Messages
        {
            get
            {
                if (_inst == null)
                {
                    _inst = new ExceptionMessages();
                }
                return _inst;
            }
        }
        protected ExceptionMessages()
        {
            //TODO  сделать чтение из xml
            NotEmptyStringMessage = "Значение параметра НЕ должно быть пустой строкой";
            PositiveParametrMessage = "Значение параметра должно быть положительным числом";
            NonNegativeParametrMessage = "Значение параметра должно быть целым и НЕ ОТРИЦАТЕЛЬНЫМ числом";
            NotNullParametrMessage = "Указатель на параметр НЕ должн иметь значение NULL";
            InvalidParametrTypeMessage = "Тип данных параметра не соответствует ожидаемому ";
            NotNumericParametrMessage = "Параметр не является числом";
            ContainerDoesNotContainItemMessage = "Элемент не содержится в коллекции";
        }

        internal String NotEmptyStringMessage { get; private set; }
        internal String PositiveParametrMessage { get; private set; }
        internal String NonNegativeParametrMessage { get; private set; }
        internal String NotNullParametrMessage { get; private set; }
        internal String InvalidParametrTypeMessage { get; private set; }
        internal String NotNumericParametrMessage { get; private set; }
        internal String ContainerDoesNotContainItemMessage { get; private set; }

    }
}
