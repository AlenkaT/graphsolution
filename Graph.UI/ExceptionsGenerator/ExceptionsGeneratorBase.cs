﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace EVT.Applications.Common.Helpers.ExceptionsGenerator
{
    public abstract class ExceptionsGeneratorBase
    {
        protected static bool TryGetArgumentNullException(object parametr, String parametrName, ref Exception ex, String message = "")
        {
            if (parametr == null)
            {
                ex = new ArgumentNullException(parametrName, ExceptionMessages.Messages.NotNullParametrMessage + message);
                return true;
            }
            return false;
        }

        protected static Boolean TryGetArgumentException(Object parametr, String prmrName, ref Exception ex, String message = "")
        {

            if (!Microsoft.VisualBasic.Information.IsNumeric(parametr))
            {
                ex = new ArgumentException(ExceptionMessages.Messages.NotNumericParametrMessage + message, prmrName);
                return true;
            }

            return false;
        }

        protected static Boolean TryGetInvalidCastException(Object parametr, String prmrName, Type requestedType, ref Exception ex, String message = "")
        {
            if (!requestedType.IsAssignableFrom(parametr.GetType()))
            //if (parametr.GetType()!=requestedType)
            {
                ex = new InvalidCastException(ExceptionMessages.Messages.InvalidParametrTypeMessage+ message);
                return true;
            }

            return false;
        }

        protected static Boolean TryGetInvalidEqualCastException<TRequestedType>(Object parametr, String prmrName, ref Exception ex, String message = "")
        {

            if (!Object.ReferenceEquals(parametr.GetType(), typeof(TRequestedType)))
           // if (!typeof(TRequestedType).IsAssignableFrom(parametr.GetType()))
            {
                ex = new InvalidCastException(ExceptionMessages.Messages.InvalidParametrTypeMessage + message);
                return true;
            }

            return false;
        }
    }
}
