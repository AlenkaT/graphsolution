﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.Verification
{
    public static class Utilities
    {
        public static Boolean IsPositive(Double val)
        {
            return val > 0;
        }
        public static Boolean IsPositive(Single val)
        {
            return val > 0;
        }

        public static Boolean IsPositive(Int64 val)
        {
            return val > 0;
        }
        public static Boolean IsPositive(Int32 val)
        {
            return val > 0;
        }
        public static Boolean IsPositive(Int16 val)
        {
            return val > 0;
        }
        public static Boolean IsPositive(Decimal val)
        {
            return val > 0;
        }

        public static Boolean IsNegative(Double val)
        {
            return val < 0;
        }
        public static Boolean IsNegative(Single val)
        {
            return val < 0;
        }

        public static Boolean IsNegative(Int64 val)
        {
            return val < 0;
        }

        public static Boolean IsNegative(Int32 val)
        {
            return val < 0;
        }

        public static Boolean IsNegative(Int16 val)
        {
            return val < 0;
        }
        public static Boolean IsNegative(Decimal val)
        {
            return val < 0;
        }

        public static Boolean IsNumericPositive(Object val, out Object result)
        {
            //выполняется боксинг в result
            /// <summary>Проверяет, является ли val примитивным численным типом, значение которого положительно и возвращает в result упакованный результат операции приведённия к типу Double
            /// <para>Here's how you could make a second paragraph in a description. <see cref="System.Console.WriteLine(System.String)"/> for information about output statements.</para>
            /// <exception cref="System.InvalidCastException">Исключение, если во время опереации приведения возникает переполнение</exception>
            /// </summary>
            checked
            {
                if ((val is Byte) && ((Byte)val > 0))
                { result = (Double)(Byte)val; return true; }
                if ((val is Int16) && ((Int16)val > 0))
                { result = (Double)(Int16)val; return true; }
                if ((val is Int32) && ((Int32)val > 0))
                { result = (Double)(Int32)val; return true; }
                if ((val is Int64) && ((Int64)val > 0))
                { result = (Double)(Int64)val; return true; }
                if ((val is SByte) && ((SByte)val > 0))
                    { result = (Double)(SByte)val; return true; }
                if ((val is UInt16) && ((UInt16)val > 0))
                    { result = (Double)(UInt16)val; return true; }
                if ((val is UInt32) && ((UInt32)val > 0))
                    { result = (Double)(UInt32)val; return true; }
                if ((val is UInt64) && ((UInt64)val > 0))
                    { result = (Double)(UInt64)val; return true; }
                if ((val is Decimal) && ((Decimal)val > 0))
                    { result = (Double)(Decimal)val; return true; }
                if ((val is Double) && ((Double)val > 0))
                    { result = (Double)val; return true; }
                if ((val is Single) && ((Single)val > 0))
                    { result = (Double)(Single)val; return true; }
            }
            result = null; 
            return false;
        }

        public static Boolean IsNumericNonNegative(Object val, out Object result)
        {
            //выполняется боксинг в result
            /// <summary>Проверяет, является ли val примитивным численным типом, значение которого положительно и возвращает в result упакованный результат операции приведённия к типу Double
            /// <para>Here's how you could make a second paragraph in a description. <see cref="System.Console.WriteLine(System.String)"/> for information about output statements.</para>
            /// <exception cref="System.InvalidCastException">Исключение, если во время опереации приведения возникает переполнение</exception>
            /// </summary>
            checked
            {
                if ((val is Byte) && ((Byte)val >= 0))
                { result = (Double)(Byte)val; return true; }
                if ((val is Int16) && ((Int16)val >= 0))
                { result = (Double)(Int16)val; return true; }
                if ((val is Int32) && ((Int32)val >=0))
                { result = (Double)(Int32)val; return true; }
                if ((val is Int64) && ((Int64)val >= 0))
                { result = (Double)(Int64)val; return true; }
                if ((val is SByte) && ((SByte)val >= 0))
                { result = (Double)(SByte)val; return true; }
                if ((val is UInt16) && ((UInt16)val >= 0))
                { result = (Double)(UInt16)val; return true; }
                if ((val is UInt32) && ((UInt32)val >= 0))
                { result = (Double)(UInt32)val; return true; }
                if ((val is UInt64) && ((UInt64)val >= 0))
                { result = (Double)(UInt64)val; return true; }
                if ((val is Decimal) && ((Decimal)val >=0))
                { result = (Double)(Decimal)val; return true; }
                if ((val is Double) && ((Double)val >= 0))
                { result = (Double)val; return true; }
                if ((val is Single) && ((Single)val >=0))
                { result = (Double)(Single)val; return true; }
            }
            result = null;
            return false;
        }


        public static Boolean IsNumericPositive(Object val)
        {
            //выполняется боксинг в result
            /// <summary>Проверяет, является ли val примитивным численным типом, значение которого положительно и возвращает в result упакованный результат операции приведённия к типу Double
            /// <para>Here's how you could make a second paragraph in a description. <see cref="System.Console.WriteLine(System.String)"/> for information about output statements.</para>
            /// <exception cref="System.InvalidCastException">Исключение, если во время опереации приведения возникает переполнение</exception>
            /// </summary>
            checked
            {
                if ((val is Byte) && ((Byte)val > 0))
                {  return true; }
                if ((val is Int16) && ((Int16)val > 0))
                { return true; }
                if ((val is Int32) && ((Int32)val > 0))
                { return true; }
                if ((val is Int64) && ((Int64)val > 0))
                { return true; }
                if ((val is SByte) && ((SByte)val > 0))
                { return true; }
                if ((val is UInt16) && ((UInt16)val > 0))
                {return true; }
                if ((val is UInt32) && ((UInt32)val > 0))
                { return true; }
                if ((val is UInt64) && ((UInt64)val > 0))
                { return true; }
                if ((val is Decimal) && ((Decimal)val > 0))
                { return true; }
                if ((val is Double) && ((Double)val > 0))
                { return true; }
                if ((val is Single) && ((Single)val > 0))
                { return true; }
            }
           
            return false;
        }

        

        public static Boolean IsNumericNegative(Object val)
        {
            //выполняется боксинг в result
            /// <summary>Проверяет, является ли val примитивным численным типом, значение которого положительно и возвращает в result упакованный результат операции приведённия к типу Double
            /// <para>Here's how you could make a second paragraph in a description. <see cref="System.Console.WriteLine(System.String)"/> for information about output statements.</para>
            /// <exception cref="System.InvalidCastException">Исключение, если во время опереации приведения возникает переполнение</exception>
            /// </summary>
            checked
            {
                if ((val is Byte) && ((Byte)val < 0))
                { return true; }
                if ((val is Int16) && ((Int16)val < 0))
                { return true; }
                if ((val is Int32) && ((Int32)val < 0))
                { return true; }
                if ((val is Int64) && ((Int64)val < 0))
                { return true; }
                if ((val is SByte) && ((SByte)val < 0))
                { return true; }
                if ((val is UInt16) && ((UInt16)val < 0))
                { return true; }
                if ((val is UInt32) && ((UInt32)val < 0))
                { return true; }
                if ((val is UInt64) && ((UInt64)val < 0))
                { return true; }
                if ((val is Decimal) && ((Decimal)val < 0))
                { return true; }
                if ((val is Double) && ((Double)val < 0))
                { return true; }
                if ((val is Single) && ((Single)val < 0))
                { return true; }
            }

            return false;
        }


        public static Boolean IsEmptyString(String str)
        {
            return str== String.Empty;
        }



        public static Boolean IsNull(Object obj)
        {
            var result = obj == null;
            return result;
        }

        public static Boolean IsValueType(Object obj)
        {
            var result = obj is ValueType;
            return result;
        }

        public static bool IsNumeric(Object value)
        {
            return (value is Byte ||
                    value is Int16 ||
                    value is Int32 ||
                    value is Int64 ||
                    value is SByte ||
                    value is UInt16 ||
                    value is UInt32 ||
                    value is UInt64 ||
                    value is Decimal ||
                    value is Double ||
                    value is Single);
        }


    }
}
