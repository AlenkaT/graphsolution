﻿using EVT.Applications.Common.Components.Interfaces.ObjectMapping;
using EVT.Applications.Common.Helpers.ExceptionsGenerator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.VMServices.ObjectMapping
{
    public abstract class ObjectMappingManagerBase : ExceptionsGeneratorBase, IObjectMappingManager
    {
        #region Implementation IObjectMappingManager
        public abstract IObjectMapping CreateMapping(Object obj);

        public void StartObjectLoading(IObjectMapping mapping)
        {
            Exception ex = null;
            if (TryGetArgumentNullException(mapping, nameof(mapping), ref ex))
            { throw ex; }
            if (TryGetInvalidCastException(mapping, nameof(mapping), typeof(ObjectMappingBase), ref ex))
            { throw ex; }
            ((ObjectMappingBase)mapping).SetInProgressState();
        }

        public void SetObjectInToMapping(IObjectMapping mapping, Object obj)
        {
            Exception ex = null;
            if (!CanSetObjectInToMapping(mapping, obj, ref ex))
            {
                DoSetObjectInToMapping(mapping, obj);
                return;
            }
            throw ex;

        }

        #endregion

        #region NVI implementation
        protected virtual Boolean CanSetObjectInToMapping(IObjectMapping mapping, Object obj, ref Exception ex)
        {
            return TryGetArgumentNullException(mapping, nameof(mapping), ref ex);//|| TryGetInvalidCastException(mapping, nameof(mapping), typeof(ObjectMappingBase), ref ex);
        }

        protected virtual void DoSetObjectInToMapping(IObjectMapping mapping, Object obj)
        {
            ((ObjectMappingBase)mapping).SetObjectIntoMapping(obj);
        }


        #endregion
    }
}
