﻿using EVT.Applications.Common.Components.Interfaces.ObjectMapping;
using EVT.WPF.MVVMSupport;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.VMServices.ObjectMapping
{
    public class DefinitionObjectMapping : ObjectMappingBase
    {

        public string ObjectDefinition { get; set; }

        public string ObjectName { get; set; }

        internal DefinitionObjectMapping():base()
        {
          
        }
    }

   
}
