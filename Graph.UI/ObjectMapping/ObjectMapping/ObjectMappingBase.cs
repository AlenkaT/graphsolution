﻿using EVT.Applications.Common.Components.Interfaces.ObjectMapping;
using EVT.WPF.MVVMSupport;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.VMServices.ObjectMapping
{
    public class ObjectMappingBase: BindableBase , IObjectMapping
    {
        Object _object;
        LoadingState _state;

        public Object MappedObject
        {
            get { return _object; }
            protected set
            {
                if (value == null)
                {
                    State = LoadingState.Failed;
                }
                else
                {
                    State = LoadingState.Completed;
                }
                _object = value;
                RaisePropertyChanged(nameof(MappedObject));
            }
        }
        public LoadingState State
        {
            get { return _state; } protected set { _state = value; RaisePropertyChanged(nameof(State)); }
        }

        internal void SetInProgressState()
        {
            State = LoadingState.InProgress;
        }

        internal void SetObjectIntoMapping(Object obj)
        {
            MappedObject = obj;
        }


        public ObjectMappingBase()
        {
            State = LoadingState.NotAssigned;
        }
    }

   
}
