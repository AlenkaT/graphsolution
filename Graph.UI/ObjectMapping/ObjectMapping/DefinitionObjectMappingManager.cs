﻿using EVT.Applications.Common.Components.Interfaces.ObjectMapping;
using EVT.Applications.Common.Helpers.ExceptionsGenerator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.VMServices.ObjectMapping
{
    public class DefinitionObjectMappingManager : ObjectMappingManagerBase
    {

        public IObjectMapping CreateMapping(String objectName, String objectDefinition, Object obj = null)
        {
            var result = (DefinitionObjectMapping) CreateMapping(obj);
            result.ObjectName = objectName;
            result.ObjectDefinition = objectDefinition;
           
            return result;
        }

        #region Implementation IObjectMappingManager
        public override IObjectMapping CreateMapping(Object obj)
        {
           var result=new DefinitionObjectMapping();
           if (obj!=null)
            {
                result.SetObjectIntoMapping(obj);
            }
            return result;
        }

        #endregion

        #region NVI implementation
        protected override Boolean CanSetObjectInToMapping(IObjectMapping mapping, Object obj, ref Exception ex)
        {
            return base.CanSetObjectInToMapping(mapping, obj, ref ex) || TryGetInvalidCastException(mapping, nameof(mapping), typeof(DefinitionObjectMapping), ref ex);
        }

        //protected override void DoSetObjectInToMapping(IObjectMapping mapping, Object obj)
        //{
        //    ((DefinitionObjectMapping)mapping).SetObjectIntoMapping(obj);
        //}
        #endregion
    }
}
