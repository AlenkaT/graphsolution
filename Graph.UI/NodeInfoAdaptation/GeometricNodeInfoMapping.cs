﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using EVT.Applications.GraphSolution.VMServices.ObjectMapping;
using System.Reflection;

namespace EVT.Applications.GraphSolution.EntitiesAdaptationToView.NodeInfoAdaptation
{
    public class GeometricNodeInfoMapping: NodeInfoMappingBase
    {
        public Double Length { get ; protected set; }
        public Double Width { get; protected set; }
        public Double Height { get; protected set; }
        public Double Area { get; protected set; }
        public Double Volume { get; protected set; }

        public override string ToString()
        {
            return "";
        }

        public GeometricNodeInfoMapping():base()
        {
            
        }
    }
}
