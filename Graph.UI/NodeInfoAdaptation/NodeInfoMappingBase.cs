﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using EVT.Applications.GraphSolution.VMServices.ObjectMapping;
using System.Reflection;

namespace EVT.Applications.GraphSolution.EntitiesAdaptationToView.NodeInfoAdaptation
{
    public class NodeInfoMappingBase : ObjectMappingBase
    {
        static NodeInfoMappingBase _defaultValue;
        public static NodeInfoMappingBase DefaultValue
        {
            get
            {
                if (_defaultValue==null)
                { _defaultValue= new NodeInfoMappingBase();}
                return _defaultValue;
            }
        }

        public String Caption { get ; protected set; }
        
        internal virtual void PostSetProperty(String propertyName)
        {
            RaisePropertyChanged(propertyName);
        }

        protected NodeInfoMappingBase():base()
        {
            
        }
    }
}
