﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using EVT.Applications.GraphSolution.VMServices.ObjectMapping;
using System.Reflection;

namespace EVT.Applications.GraphSolution.EntitiesAdaptationToView.NodeInfoAdaptation
{
    public class SharedNodeInfoMapping: NodeInfoMappingBase
    {
        public Double Weight { get ; protected set; }
        public String Material { get; protected set; }
        public String Section { get; protected set; }
        public String  SteelProductType { get; protected set; }
        public String ConstructionMaterialType { get; protected set; }

        public override string ToString()
        {
            return "";
        }

        public SharedNodeInfoMapping():base()
        {
            
        }
    }
}
