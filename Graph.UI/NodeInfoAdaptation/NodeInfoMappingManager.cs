﻿using EVT.Applications.Common.Helpers.ExceptionsGenerator;
using EVT.Applications.GraphSolution.Graph;
using EVT.Applications.GraphSolution.GraphInfo.Interfaces;
using EVT.Applications.GraphSolution.VMServices.ObjectMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EVT.Applications.Common.Components.Interfaces.ObjectMapping;

using EVT.Applications.GraphSolution.NodeInfoStorageManager.Interfaces;
using System.Reflection;

namespace EVT.Applications.GraphSolution.EntitiesAdaptationToView.NodeInfoAdaptation
{
    public class NodeInfoMappingManager : ObjectMappingManagerBase
    {

        private Dictionary<Type, NodeInfoMappingBase> _mappingDictionary = new Dictionary<Type, NodeInfoMappingBase>();

        protected readonly INodeInfoAccessor _storageManager;

        public T GetMapping<T>() where T: NodeInfoMappingBase, new ()
        {
            T result;
            var key = typeof(T);
            if (_mappingDictionary.ContainsKey(key))
            {
                return _mappingDictionary[key] as T;
            }

            result = new T();
            _mappingDictionary[key] = result;      
            return result;
        }


        #region Implementation IObjectMappingManager
        public override IObjectMapping CreateMapping(object obj) 
        {
            var result = NodeInfoMappingBase.DefaultValue;
            if (obj != null)
            {
                this.DoSetObjectInToMapping(result, obj);
            }
            return result;
        }

        #endregion

        #region  Override NVI implementation

        protected override Boolean CanSetObjectInToMapping(IObjectMapping mapping, Object obj, ref Exception ex)
        {
            return base.CanSetObjectInToMapping(mapping, obj, ref ex) ||
                TryGetInvalidCastException(obj, nameof(obj), typeof(Node), ref ex) ||
                TryGetInvalidCastException(mapping, nameof(mapping), typeof(NodeInfoMappingBase), ref ex);
        }

        protected override void DoSetObjectInToMapping(IObjectMapping mapping, object obj)
        {
            base.DoSetObjectInToMapping(mapping, obj);
            //Установка свойств
            var node = (Node)obj;
            //нет обработки результата , полученного от манажера
            var arrNI = _storageManager.GetNodeInfos(node);
            var arrNodeInfoTypeNames = arrNI.Select(x => x.GetType().Name);
            //рефлексия для установки всех имеющихся значений в тип обёртки
            var propertyList = mapping.GetType().GetProperties();
            foreach (var prop in propertyList)
            {
                var nodeinfo = arrNI.SingleOrDefault(x => x.GetType().Name.Contains(prop.Name));
                if (nodeinfo!=null)
                {
                    prop.SetValue(mapping, nodeinfo.Info);
                    ((GeometricNodeInfoMapping)mapping).PostSetProperty(prop.Name);
                }
            }

        }

        #endregion

        public NodeInfoMappingManager(INodeInfoAccessor manager)
        {
            _storageManager = manager;
        }
    }

}
