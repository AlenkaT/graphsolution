﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using EVT.Applications.GraphSolution.VMServices.ObjectMapping;
using System.Reflection;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;
using System.ComponentModel;
using EVT.Applications.GraphSolution.EntitiesAdaptationToView.NodeInfoAdaptation;

namespace EVT.Applications.GraphSolution.EntitiesAdaptationToView.NodeInfoAdaptation
{
    //будет привязано к проперти грид
    public class MappingCollection
    {
        NodeInfoMappingManager _manager;
        List<NodeInfoMappingBase> _mappingList;

        [ExpandableObject]
        [DisplayName("Geometry")]
        public GeometricNodeInfoMapping GeometricNodeInfo =>_manager.GetMapping<GeometricNodeInfoMapping>();

        [ExpandableObject]
        [DisplayName("Geometry")]
        public SharedNodeInfoMapping SharedNodeInfo => _manager.GetMapping<SharedNodeInfoMapping>();


        public void SetObjectIntoMapping(Object mappedObject)
        {
            _manager.SetObjectInToMapping(GeometricNodeInfo, mappedObject);
            _manager.SetObjectInToMapping(SharedNodeInfo, mappedObject);
        }

        public MappingCollection(NodeInfoMappingManager manager)
        {
            _manager = manager;
            

        }
    }
}
