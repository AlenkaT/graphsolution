﻿
using EVT.Applications.Common.Components.Interfaces.ObjectLocalStorage;
using EVT.Applications.GraphSolution.Graph;
using EVT.Applications.GraphSolution.GraphInfo.Interfaces;
using EVT.Applications.GraphSolution.NodeInfoTypes.Shared;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.BL.Infrastructure.NodeInfoWizards
{
    //Один визард на основе входящих параметров может делать несколько видов инфы
    public class  CaptionNodeInfoWizard
    {
        ILocalStorageManager<INodeInfo> _infoStorageManager;

        //для расчета спецификации необходимой является информация
        
        public void CreateInfos(Node node, String caption)
        {
            // не понятно, что делать, если метод вернул false (не добавилась инфа - на эту ноду уже есть инфа такого вида) 
            _infoStorageManager.AddObjectToStorage(new CaptionNodeInfo(node, caption));   

        }

        public CaptionNodeInfoWizard(ILocalStorageManager<INodeInfo> infoStorageManager
            )
        {
            _infoStorageManager = infoStorageManager;
        }


    }
}
