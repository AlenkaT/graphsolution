﻿
using EVT.Applications.Common.Components.Interfaces.ObjectLocalStorage;
using EVT.Applications.GraphSolution.SharedInfoTypes;
using EVT.Applications.GraphSolution.Graph;
using EVT.Applications.GraphSolution.GraphInfo;
using EVT.Applications.GraphSolution.GraphInfo.Interfaces;
using EVT.Applications.GraphSolution.NodeInfoTypes.Shared;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EVT.Applications.GraphSolution.NodeInfoTypes.Geometric;


namespace EVT.Applications.GraphSolution.BL.Infrastructure.NodeInfoWizards
{
    //Один визард на основе входящих параметров может делать несколько видов инфы
    public class RebarGost5781InfoWizard
    {
        ILocalStorageManager<INodeInfo> _storageManager;
     
        const String SteelGostName = "gost5781";
        const String SectionType = "Rebars";
        const String SectionGostName = "gost5781";


        void CreateInfos(Node node, String caption, String gradeSteel, EnSteelProductType productType, String sectionSize, Double length)
        {
            var material = Material.Create("Steel", SteelGostName, gradeSteel, 7850);
            var section = Section.Create(SectionType, SectionGostName, sectionSize);
            _storageManager.AddObjectToStorage(new CaptionNodeInfo(node, caption));
            _storageManager.AddObjectToStorage(new MaterialNodeInfo(node, material));
            _storageManager.AddObjectToStorage(new SectionNodeInfo(node, section));
            _storageManager.AddObjectToStorage(new LengthNodeInfo(node, length));
            _storageManager.AddObjectToStorage(new SteelProductTypeNodeInfo(node, productType));
        }

        public void CreateA400Infos(Node node, String caption, Byte steelProductType, String sectionSize, Double length)
        {
            var productType = (EnSteelProductType)steelProductType;
            CreateInfos(node, caption, "А400", productType, sectionSize, length);
        }

        public void CreateA400rInfos(Node node, String caption, String sectionSize, Double length)
        {
            CreateA400Infos(node, caption, 1, sectionSize, length);
        }

        public void CreateA400rsInfos(Node node, String caption, String sectionSize, Double length)
        {
            CreateA400Infos(node, caption, 2 , sectionSize, length);
        }

        public void CreateA400sInfos(Node node, String caption, String sectionSize, Double length)
        {
            CreateA400Infos(node, caption, 3, sectionSize, length);
        }


        public void CreateA240Infos(Node node, String caption, EnSteelProductType productType, String sectionSize, Double length)
        {
            CreateInfos(node, caption, "А240", EnSteelProductType.Rebar, sectionSize, length);
        }

        public void CreateA240rInfos(Node node, String caption, String sectionSize, Double length)
        {
            CreateA240Infos(node, caption, EnSteelProductType.Rebar, sectionSize, length);
        }

        public void CreateA240rsInfos(Node node, String caption, String sectionSize, Double length)
        {
            CreateA240Infos(node, caption, EnSteelProductType.RebarSteelProduct, sectionSize, length);
        }

        public void CreateA240sInfos(Node node, String caption, String sectionSize, Double length)
        {
            CreateA240Infos(node, caption, EnSteelProductType.SteelConstruction, sectionSize, length);
        }

        public RebarGost5781InfoWizard(ILocalStorageManager<INodeInfo> nodeInfoStorage )
        {
            _storageManager = nodeInfoStorage;
        }


    }
}
