﻿using EntryPoint;
using EVT.Ioc.ContainerBasedOnUnity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using EVT.Graph.UI.MainWindowView;

namespace EVT.GraphUI.EntryPoint
{
    class EntryPoint
    {
        [STAThread]
        public static void Main(String[] args)
        {
            //Товарищ контейнер, запусти , пжалста, приложение, главным окном которого
            // является экземпляр класса MainWindow под управлением экземпляра класса MainApp
            
            ContainerInjection.ApplicationRun<MainWindowView, App>();
        }

    }
}
