﻿using EVT.Applications.Common.ComponentsInterfaces.Updater;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.Fasade
{
    public interface IFasade<T>
    {
        T CreateItem(Object parametr);
        T ReadItem(Object parametr);
        void UpdateItem(T item, IUpdater<T> updater, Object parametr);
        void DeleteItem(T item);
        
    }
}
