﻿

using EVT.Applications.Common.Helpers.ExceptionsGenerator;
using EVT.Applications.GraphSolution.Graph;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EVT.Applications.GraphSolution.Common.Components.Interfaces.Updater;
using EVT.Graph.GraphBuilder.Interfaces;
using EVT.Applications.GraphSolution.StructuralConstructor.Interfaces;
using EVT.Applications.Common.Components.Interfaces.ObjectLocalStorage;
using EVT.Applications.GraphSolution.GraphInfo.Interfaces;
using EVT.Applications.GraphSolution.NodeInfoStorageManager.Interfaces;
using EVT.Applications.GraphSolution.StructuralConstructor;

namespace EVT.Applications.GraphSolution.BL.Infrastructure.Updaters.WizardCollection
{
    // AddBaseNodeToGraphWizard - добавление 1ой точки подконструкции

    public class AddBeginingNodeToGraphWizard : GraphWizardBase
    {
        private INodeInfoAccessor _manager => (INodeInfoAccessor)_Manager;

        protected override bool CanUpdate(DirectedGraph target, SourceParametrs sparams, ref Exception ex)
        {
            if (!base.CanUpdate(target, sparams, ref ex)) return false;
            if (target.GetNodes().Count == 0 && sparams.Source == null) return true;// root
            if (TryGetInvalidCastException(sparams.Source, nameof(sparams.Source), typeof(Node), ref ex))
            {
                return false;
            }
            return true;
        }

        protected override void DoUpDate(DirectedGraph target, SourceParametrs sparams)
        {
            var source = sparams.Source as Node;
            var storageState = _manager.GetNodeInfos(source); // состояние хранилища до работы метода
            _Builder.SetTargetGraph(target);

            var arr = new Node[1];

            if (source == null)
            {
                //добавление корневого элемента
                arr[0] = _Builder.AddRootNode();
            }
            else
            {
                var parentNode = source;
                arr[0] = _Builder.AddChild(parentNode);
            }

            try
            {
                foreach (var ctor in _Constructors)
                {
                    ConstructorParametr prm;
                    ctor.TryDetectConstruction(out prm, sparams.Parametrs);
                    ctor.Construct(arr, prm);
                }
            }
            catch (InvalidOperationException e)
            {
                //откат состояния хранилища и графа
                target.RemoveNode(arr[0]);
                var defectInfos = _manager.GetNodeInfos(source).Except(storageState);
                if (defectInfos.Count() > 0)
                {
                    foreach (var ni in defectInfos)
                    {
                        _Manager.RemoveObjectFromStorage(ni);
                    }
                }

            }
            finally
            {
                _Builder.ClearTarget();
            }

        }

        public AddBeginingNodeToGraphWizard(IGraphBuilder builder, INodeInfoAccessor manager, params IStructuralConstructor[] constructors) : base(builder, manager as ILocalStorageManager<INodeInfo>)
        {
            //заменяем конструкторы
            _Constructors.Clear();
            _Constructors.UnionWith(constructors);
        }
    }
}
