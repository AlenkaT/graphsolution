﻿
using EVT.Applications.Common.Components.Updater;
using EVT.Applications.Common.Helpers.ExceptionsGenerator;
using EVT.Applications.GraphSolution.Graph;
using EVT.Applications.GraphSolution.ViewModelsBase.Components;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EVT.Applications.Common.Components.Interfaces.Updater;
using EVT.Graph.GraphBuilder.Interfaces;
using EVT.Applications.GraphSolution.StructuralConstructor.Interfaces;
using EVT.Applications.GraphSolution.NodeInfoStorageManager.Interfaces;
using EVT.Applications.Common.Components.Interfaces.ObjectLocalStorage;
using EVT.Applications.GraphSolution.GraphInfo.Interfaces;

namespace EVT.Applications.GraphSolution.WizardCollection
{
   
    public class AddNodeToGraphWizard: GraphWizardBase
    {
        private INodeInfoAccessor _manager => (INodeInfoAccessor)_Manager;
        protected override bool CanUpdate(DirectedGraph target, SourceParametrs sparams, ref Exception ex)
        {
            if (!base.CanUpdate(target, sparams, ref ex)) return false;
            if (target.GetNodes().Count == 0 && sparams.Source == null) return true;
            if (TryGetInvalidCastException(sparams.Source, nameof(sparams.Source), typeof(Node), ref ex))
            {
                return false;
            }
            return true;
        }

        protected override void DoUpDate(DirectedGraph target, SourceParametrs sparams)
        {
            _Builder.SetTargetGraph(target);

            Node node;

            if (sparams.Source == null)
            {
                //добавление корневого элемента
                node = _Builder.AddRootNode();
            }
            else
            {
                var parentNode = sparams.Source as Node;
                node = _Builder.AddChild(parentNode);

            }
            _Builder.ClearTarget();
        }

        public AddNodeToGraphWizard(IGraphBuilder builder, INodeInfoAccessor manager) : base(builder, manager as ILocalStorageManager<INodeInfo>)
        {
            
        }
    }
}
