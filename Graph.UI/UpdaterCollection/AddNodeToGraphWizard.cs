﻿
using EVT.Applications.Common.Helpers.ExceptionsGenerator;

using EVT.Applications.GraphSolution.Graph;
using EVT.Graph.GraphBuilder.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EVT.Applications.GraphSolution.Common.Components.Interfaces.Updater;
using EVT.Applications.GraphSolution.StructuralConstructor.Interfaces;
using EVT.Applications.GraphSolution.NodeInfoStorageManager.Interfaces;
using EVT.Applications.GraphSolution.StructuralConstructor;
using EVT.Applications.Common.Components.Interfaces.ObjectLocalStorage;
using EVT.Applications.GraphSolution.GraphInfo.Interfaces;
using EVT.Applications.GraphSolution.BL.Infrastructure.NodeInfoWizards;


namespace EVT.Applications.GraphSolution.BL.Infrastructure.Updaters.WizardCollection
{
    //простое добавление одной ноды с информацией CaptionNodeInfo
    public class AddNodeToGraphWizard : GraphWizardBase
    {
        CaptionNodeInfoWizard _wizard;

        protected override bool CanUpdate(DirectedGraph target, SourceParametrs sparams, ref Exception ex)
        {
            if (!base.CanUpdate(target, sparams, ref ex)) return false;
            if (target.GetNodes().Count == 0 && sparams.Source == null) return true;
            if (TryGetInvalidCastException(sparams.Source, nameof(sparams.Source), typeof(Node), ref ex))
            {
                return false;
            }
            return true;
        }
        protected override void DoUpDate(DirectedGraph target, SourceParametrs sparams)
        {
            _Builder.SetTargetGraph(target);

            Node node;

            if (sparams.Source == null)
            {
                //добавление корневого элемента
                node = _Builder.AddRootNode();
               
            }
            else
            {

                var parentNode = sparams.Source as Node; //проверка приводимости в методе CanUpDate
                node = _Builder.AddChild(parentNode);

            }

            if (sparams.Parametrs.Count()>0 && sparams.Parametrs[0] is String)
            {
                _wizard.CreateInfos(node, (String)sparams.Parametrs[0]);
            }

            _Builder.ClearTarget();
            return;
        }

        #region C-tor
        // контейнер на основе именных регистраций будет инжектить заданные конструкторы
        public AddNodeToGraphWizard(IGraphBuilder builder, INodeInfoAccessor manager, CaptionNodeInfoWizard wizard) :
            base(builder, manager as ILocalStorageManager<INodeInfo>)
        {
            _wizard = wizard;
        }
        #endregion
    }
}
