﻿using EVT.Applications.Common.Components.Interfaces.ObjectLocalStorage;
using EVT.Applications.GraphSolution.Common.Components.Interfaces.Updater;
using EVT.Applications.Common.Helpers.ExceptionsGenerator;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.BL.Infrastructure.Updaters
{
    public abstract class UpdaterBase<T> : ExceptionsGeneratorBase, IUpdater<T>
    {
        
        void IUpdater<T>.Update(T target, SourceParametrs sparams)
        {
            Exception ex = null;
            if (CanUpdate(target, sparams, ref ex))
            {
                DoUpDate(target, sparams);
                return;
            }
            throw ex;
        }

        protected virtual Boolean CanUpdate(T target, SourceParametrs sparams, ref Exception ex)
        {
            if (TryGetArgumentNullException(target, nameof(target), ref ex))
            {
                return false;
            }
     
            return true;
        }
        protected abstract void DoUpDate(T target, SourceParametrs sparams);

        public UpdaterBase()
        {

        }
    }
}
