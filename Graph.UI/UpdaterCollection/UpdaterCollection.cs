﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EVT.Applications.GraphSolution.Common.Components.Interfaces.Updater;


namespace EVT.Applications.GraphSolution.BL.Infrastructure.Updaters
{
    public class UpdaterCollection<T>: IUpdaterCollection<T>
    {
        //тут нужные апдейтеры
        //public IUpdater<T> AddItemToTargetObjectUpdater { get; }
        //public IUpdater<T> RemoveItemFromTargetObjectUpdater { get; }
        //public IUpdater<T> AddRootItemToTargetObjectUpdater { get; }
        Dictionary<String, IUpdater<T>> _dictionaryUpdaters;
        public IUpdater<T> GetUpdater(String regUpdaterName)
        {
            var result = (_dictionaryUpdaters.ContainsKey(regUpdaterName)) ? _dictionaryUpdaters[regUpdaterName] : null;
            return result;
        }

        //Сюда инжектим все необходимые операции над графом
        public UpdaterCollection(KeyValuePair<String, IUpdater<T>>[] updaters)
        {
            //AddItemToTargetObjectUpdater = addItemToTargetObjectUpdater;
            //RemoveItemFromTargetObjectUpdater = removeItemFromTargetObjectUpdater;
            //AddRootItemToTargetObjectUpdater = addRootItemToTargetObjectUpdater;
            _dictionaryUpdaters = new Dictionary<string, IUpdater<T>>();
            foreach (var pair in updaters)
            {
                _dictionaryUpdaters[pair.Key] = pair.Value;
            }
        }
    }
}
