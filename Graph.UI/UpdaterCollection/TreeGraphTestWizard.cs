﻿
using EVT.Applications.Common.Helpers.ExceptionsGenerator;

using EVT.Applications.GraphSolution.Graph;
using EVT.Graph.GraphBuilder.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EVT.Applications.GraphSolution.Common.Components.Interfaces.Updater;
using EVT.Applications.GraphSolution.NodeInfoStorageManager.Interfaces;
using EVT.Applications.Common.Components.Interfaces.ObjectLocalStorage;
using EVT.Applications.GraphSolution.GraphInfo.Interfaces;


namespace EVT.Applications.GraphSolution.BL.Infrastructure.Updaters.WizardCollection
{
    public class TreeGraphTestWizard : GraphWizardBase
    {
        
        protected override bool CanUpdate(DirectedGraph target, SourceParametrs sparams, ref Exception ex)
        {
            if (!base.CanUpdate(target, sparams, ref ex)) return false;
            if (target.GetNodes().Count == 0 && sparams.Source == null) return true;
            if (TryGetInvalidCastException(sparams.Source, nameof(sparams.Source), typeof(Node), ref ex))
            {
                return false;
            }
            return true;
        }

        protected override void DoUpDate(DirectedGraph target, SourceParametrs sparams)
        {
            _Builder.SetTargetGraph(target);
            Node parentNode;
            if (sparams.Source == null)
            {
                //добавление корневого элемента
                _Builder.AddRootNode();
                parentNode = (Node)target.GetChildren().Single();
            }
            else
            {
                parentNode = sparams.Source as Node;
            }
            for (var i = 0; i < 10; i++)
            {
                _Builder.AddChild(parentNode);
            }

            _Builder.ClearTarget();
            return;
        }

        public TreeGraphTestWizard(IGraphBuilder builder, INodeInfoAccessor manager) :base(builder, manager as ILocalStorageManager<INodeInfo>) { }
    }
}
