﻿

using EVT.Applications.Common.Helpers.ExceptionsGenerator;
using EVT.Applications.GraphSolution.Graph;
using EVT.Applications.GraphSolution.ViewModelsBase.Components;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EVT.Applications.GraphSolution.Common.Components.Interfaces.Updater;
using EVT.Applications.GraphSolution.BL.Infrastructure.Updaters;

namespace EVT.Applications.GraphSolution.BL.Infrastructure.Updaters.WizardCollection
{
    
    public class RemoveNodeFromGraphUpdater: UpdaterBase<DirectedGraph>
    {
        protected override bool CanUpdate(DirectedGraph target, SourceParametrs sparams, ref Exception ex)
        {
            if (!base.CanUpdate(target, sparams, ref ex)) return false;
            if (TryGetInvalidCastException(sparams.Source, nameof(sparams.Source), typeof(Node), ref ex))
            {
                return false;
            }
            return true;
        }
        protected override void DoUpDate(DirectedGraph target, SourceParametrs sparams)
        {
            var parentNode = sparams.Source as Node;
            target.RemoveNode(parentNode);
        }

        public RemoveNodeFromGraphUpdater() { }
    }
}
