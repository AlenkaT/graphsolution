﻿
using EVT.Applications.Common.Components.Interfaces.ObjectLocalStorage;

using EVT.Applications.GraphSolution.BL.Infrastructure.Updaters;
using EVT.Applications.GraphSolution.Graph;
using EVT.Applications.GraphSolution.GraphInfo.Interfaces;
using EVT.Applications.GraphSolution.NodeInfoStorageManager.Interfaces;
using EVT.Applications.GraphSolution.StructuralConstructor.Interfaces;
using EVT.Graph.GraphBuilder.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.BL.Infrastructure.Updaters.WizardCollection
{
    //визард формирует структуру графа, создает узлы и шаблоны для конструкторов
    abstract public class GraphWizardBase: UpdaterBase<DirectedGraph>
    {
        protected readonly ILocalStorageManager<INodeInfo> _Manager;
        //для проектирования визарда
        protected readonly IGraphBuilder _Builder; // создание нод
        
        #region C-tor

        public GraphWizardBase(IGraphBuilder builder, ILocalStorageManager<INodeInfo> manager)
        {
            _Builder = builder;         
            _Manager = manager;
        }
        #endregion

    }
}
