﻿using EVT.Applications.Common.Helpers.ExceptionsGenerator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EVT.Applications.Common.Components.Interfaces.ObjectLocalStorage;
using EVT.Applications.Common.Components.Interfaces.Factories;
using EVT.Applications.GraphSolution.Common.Components.Interfaces.Updater;
using EVT.Applications.Common.Components.Interfaces.Fasade;
//7using EVT.Applications.Common.ComponentsInterfaces.EventAggregator;

namespace EVT.Applications.GraphSolution.Fasade
{
    public class AppFasade<T> : ExceptionsGeneratorBase, IFasade<T>
    {
        #region Fields
        protected readonly ILocalStorageManager<T> _storageManager;
        protected readonly ISimpleFactory<T> _simpleFactory;
        #endregion


        #region Implimentation Interface

        public T CreateItem(Object parametr)
        {
            Exception ex = null;
            if (CanCreateItem(parametr, ref ex))
            {
                var result=DoCreateItem(parametr);
                if (result != null)
                {
                    _storageManager.AddObjectToStorage(result);
                }
                return result;
            }
            throw ex;
        }

        public void DeleteItem(T item)
        {
            Exception ex = null;
            if (CanDeleteItem(item, ref ex))
            {
                DoDeleteItem();

                return;
            }
            throw ex;
        }

        public T ReadItem(Object parametr )
        {
            Exception ex = null;
           
            if (CanReadItem(parametr, ref ex))
            {
                var item = DoReadItem(parametr);
                return item;
            }
            throw ex;

        }

        public void UpdateItem(T targetObject, IUpdater<T> updater, SourceParametrs sparams)
        {
            Exception ex = null;
   
            if (CanUpdateItem(targetObject, updater, sparams, ref ex))
            {
                DoUpdateItem(targetObject, updater, sparams);
                return;
            }
            throw ex;
        }
        #endregion

        #region NVI
        protected virtual Boolean CanCreateItem(Object parametr, ref Exception ex)
        {
            return true; 
        }

        protected virtual T DoCreateItem(Object parametr)
        {
            var result = _simpleFactory.CreateProduct(parametr);
            
            return result;
        }

        protected virtual Boolean CanDeleteItem(T item, ref Exception ex)
        {
            ex = new Exception();
            return false;
        }

        protected virtual void DoDeleteItem()
        {
            _storageManager.RemoveObjectFromStorage(default(T));
        }

        protected virtual Boolean CanReadItem(Object parametr, ref Exception ex)
        {
            //Проверка парамента на валидность

            if (TryGetArgumentException(parametr, nameof(parametr), ref ex))
            {
                return false;
            }
            return true;
        }

        protected virtual T DoReadItem(Object parametr)
        {
            var id = Convert.ToInt64(parametr);
            var result = _storageManager.ReadObjectByID(id);
            
            return result;
        }

        protected virtual Boolean CanUpdateItem(T item, IUpdater<T> updater, SourceParametrs sparams, ref Exception ex)
        {
            if (TryGetArgumentNullException(updater, nameof(updater), ref ex))
            {
                return false;
            }
            return true;
        }

        protected virtual void DoUpdateItem(T targetObject, IUpdater<T> updater, SourceParametrs sparams)
        {

                updater.Update(targetObject, sparams);

        }
        #endregion

        #region C-tor
        public AppFasade(ILocalStorageManager<T> storageManager, ILocalStorage<T> storage,  ISimpleFactory<T> factory)
        {
            _storageManager = storageManager;
            _simpleFactory = factory;
            _storageManager.AttachStorage(storage);

        }
        #endregion
    }
}
