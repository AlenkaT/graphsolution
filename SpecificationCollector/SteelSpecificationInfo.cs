﻿using EVT.Applications.GraphSolution.CommonNodeInfos;
using EVT.Applications.GraphSolution.GraphInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EVT.Applications.GraphSolution.GraphInfo.Interfaces;

namespace EVT.Applications.GraphSolution.SteelSpecificationCollecting
{
    public class SteelSpecificationInfo: INodeInfo
    {
        //Обертка свойств, требуемых визитором для работы
        public MaterialInfo MaterialInfo { get; }
        public SectionInfo SectionInfo { get; }
        public ProductTypeInfo ProductTypeInfo { get;}
        public LengthInfo LengthInfo { get; }
        public WidthInfo WidthInfo { get; }
        public AreaInfo AreaInfo { get; }
        
        internal SteelSpecificationInfo(MaterialInfo materialInfo, SectionInfo sectionInfo, ProductTypeInfo productTypeInfo,
            LengthInfo lengthInfo, WidthInfo widthInfo, AreaInfo areaInfo)
        {
            MaterialInfo = materialInfo;
            SectionInfo = sectionInfo;
            ProductTypeInfo = productTypeInfo;
            LengthInfo = lengthInfo;
            WidthInfo = widthInfo;
            AreaInfo = areaInfo;

        }
    }
}
