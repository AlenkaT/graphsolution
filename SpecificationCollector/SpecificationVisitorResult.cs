﻿using EVT.Applications.GraphSolution.SharedInfoTypes;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.SteelSpecificationCollecting
{
    public class SpecificationVisitorResult
    {
        //Составить свойства по результатам сортировки
        public EnSteelProductType ProductType;
        public String SteelGostName { get; }
        public String SteelGrade { get; }
        public String SectionType { get; }
        public String SectionGostName { get; }
        public String SectionSize { get; }
        public Double Massa { get; }

        internal SpecificationVisitorResult(EnSteelProductType productType, String steelGostName, String steelGrade,
            String sectionType, String sectionGostName, String sectionSize, Double massa)
        {
            ProductType = productType;
            SteelGostName = steelGostName;
            SteelGrade = steelGrade;
            SectionType = sectionType;
            SectionGostName = sectionGostName;
            SectionSize = sectionSize;
            Massa = massa;
        }
    }
}
