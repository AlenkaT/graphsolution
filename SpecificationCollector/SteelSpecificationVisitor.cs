﻿using System;
using System.Collections.Generic;
using System.Linq;

using EVT.Applications.GraphSolution.SharedInfoTypes;

using EVT.Applications.GraphSolution.Graph;

using EVT.Applications.GraphSolution.NodeVisitor;

using EVT.Applications.GraphSolution.NodeInfoTypes.Shared;
using EVT.Applications.GraphSolution.NodeInfoTypes.Geometric;
using EVT.Applications.GraphSolution.NodeInfoStorageManager.Interfaces;

namespace EVT.Applications.GraphSolution.SteelSpecificationCollecting
{
    // сборщик спецификации
    class SteelSpecificationVisitor : ResultNodeVisitorBase<IEnumerable<SpecificationVisitorResult>, SpecificationVisitorResult>
    {
        protected readonly INodeInfoStorageManager<SectionNodeInfo> _sections;
        protected readonly INodeInfoStorageManager<SteelProductTypeNodeInfo> _productTypes;
        protected readonly INodeInfoStorageManager<MaterialNodeInfo> _materials;
        protected readonly INodeInfoStorageManager<AreaNodeInfo> _areas;
        protected readonly INodeInfoStorageManager<LengthNodeInfo> _lengths;
        protected readonly INodeInfoStorageManager<WidthNodeInfo> _widths;

        #region Private Methods
        private Double CalculateMassa(Section s, Double l, Double w=0, Double a=0)
        {
            Double result = 0;

            Double size;
            if (a != 0)
            {
                //площадь проката уже задана
                size = a;
            }
            else
            {
                if (l != 0)
                {
                    if (w != 0)
                    {
                        // задано ширина листовых элементов
                        size = w * l;
                    }
                    else
                    {
                        // задана только длина - для не плоских прокатов
                        size = l;
                    }
                }
                else
                { size = 0; }
            }
            result = size * s.Properties.UnitWeight;
            return result;

        }

        #endregion

        #region Protected Override
        protected override SpecificationVisitorResult DoVisitNode(Node node)
        {
            var s = _sections.GetInfo(node) as Section;
            if (s == null) return null;
            var m = _materials.GetInfo(node) as Material;
            if (m == null) return null;
            var pt = _productTypes.GetInfo(node);
            if (pt == null) return null;

            var l = _lengths.GetInfo(node);
            var l0 = (l == null) ? 0 : (Double)l;

            var w = _widths.GetInfo(node);
            var w0 = (w == null) ? 0 : (Double)w;

            var a = _areas.GetInfo(node);
            var a0 = (a == null) ? 0 : (Double)a;

            var massa = CalculateMassa(s, l0, w0, a0);
            if (massa != 0)
            {
                return new SpecificationVisitorResult((EnSteelProductType)pt, m.MaterialGostName,
                    m.Properties.Grade, s.Properties.Caption,
                    s.GostSectionName, s.Properties.Size, massa);
            }

            return null;
        }


        protected override IEnumerable<SpecificationVisitorResult> DoGetResult(IEnumerable<SpecificationVisitorResult> array)
        {
            return array;
        }

        #endregion

        #region C-tor

        //internal NodeSpecificationVisitor(NodeInfoStorage storage) : base( list => { return list.ToList(); })
        /*запрашиваем в конструктор не всё хранилище, а только те списки данных, которые необходимы
        / для работы визитора!
        /это делает вызов конструктора new NodeVisitor более понятным в плане того, с какими данными
        /планирует работать данный визитор
        */
        internal SteelSpecificationVisitor(
            INodeInfoStorageManager<SectionNodeInfo> sections,
            INodeInfoStorageManager<SteelProductTypeNodeInfo> productTypes,
            INodeInfoStorageManager<MaterialNodeInfo> materials,
            INodeInfoStorageManager<AreaNodeInfo> areas,
            INodeInfoStorageManager<LengthNodeInfo> lengths,
            INodeInfoStorageManager<WidthNodeInfo> widths)
        {
            _sections = sections;
            _productTypes = productTypes;
            _materials = materials;
            _lengths = lengths;
            _widths = widths;
            _areas = areas;
        }


        #endregion


        //2.4) Реализовать метод VisitNode(Node node) как
        //public void VisitNode(Node node)
        //{
        //    //NodeWeightInfo nodeInfo = _nodeWeightInfos.FirstOrDefault(info => info.Node == node);
        //    //if (nodeInfo != null)
        //    //{
        //    //    TotalWeight += nodeInfo.Info;
        //    //}
        //}

    }
}
