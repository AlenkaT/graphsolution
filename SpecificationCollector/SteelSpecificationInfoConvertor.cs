﻿using EVT.Applications.GraphSolution.GraphInfo.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using EVT.Applications.GraphSolution.CommonNodeInfos;
using EVT.Graph.GraphVisitor.Interfaces;
using EVT.Graph.GraphVisitor;
using EVT.Applications.GraphSolution.SteelSpecificationCollecting;

namespace EVT.Applications.GraphSolution.NodeInfos.SteelSpecificationCollecting
{
    public class SteelSpecifictionInfoConvertor : IInfoConvertor<SteelSpecificationInfo>
    {
        public SteelSpecificationInfo ConvertInfo(IEnumerable<INodeInfo> infoList)
        {
          
            var section = (SectionInfo)infoList.SingleOrDefault(x => x is SectionInfo);
            var material = (MaterialInfo)infoList.SingleOrDefault(x => x is MaterialInfo);
            var prodType = (ProductTypeInfo)infoList.SingleOrDefault(x => x is ProductTypeInfo);
            var length = (LengthInfo)infoList.SingleOrDefault(x => x is LengthInfo);
            var width = (WidthInfo)infoList.SingleOrDefault(x => x is WidthInfo);
            var area = (AreaInfo)infoList.SingleOrDefault(x => x is AreaInfo);
            var result = new SteelSpecificationInfo(material, section, prodType, length, width, area);
            return result;
        
        }

        internal SteelSpecifictionInfoConvertor() { }
    }
}
