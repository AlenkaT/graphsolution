﻿using EVT.Applications.Common.Helpers.ExceptionsGenerator;
using EVT.Applications.GraphSolution.Graph;
using EVT.Graph.GraphBuilder.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Graph.GraphBuilder
{
    public abstract class GraphBuilderBase<TNode>: ExceptionsGeneratorBase, IGraphBuilder where TNode : Node, new ()
    {
        DirectedGraph _graph;
        protected DirectedGraph _Graph
        {
            get
            {
                if (_graph == null) throw new NullReferenceException("Property _Gratph is not set");
                return _graph;
            }
        }
        
       

        public void SetTargetGraph(DirectedGraph graph)
        {
            Exception ex = null;
            if (CanSetGraph(graph, ref ex))
            {
                _graph = graph;
                //CurrentNode = graph.GetChildren().FirstOrDefault();
                return;
            }
            throw ex;
        }

        protected virtual Boolean CanSetGraph(DirectedGraph graph, ref Exception ex)
        {
            return !TryGetArgumentNullException(graph, nameof(graph), ref ex);
        }

        public abstract Node AddRootNode();

        public abstract Node AddChild(Node parentNode);
        public abstract Node[] AddChildren(Node parentNode, Int32 childrenCount);

        public void ClearTarget()
        {
            _graph = null;
           // CurrentNode = null;
        }

        public virtual void BindNodes(Node from, Node to)
        {
            throw new InvalidOperationException(); 
        }

        public virtual void AddParent(Node targetNode)
        {
            throw new InvalidOperationException();
        }

        internal GraphBuilderBase()
        {
            
        }
    }
}
