﻿
using EVT.Applications.GraphSolution.Graph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Graph.GraphBuilder.Interfaces
{
    public interface IGraphBuilder
    {
        Node AddRootNode();
        Node AddChild(Node parentNode);
        Node[] AddChildren(Node parentNode, Int32 childrenCount);
        void SetTargetGraph(DirectedGraph graph);
        void ClearTarget();
        void BindNodes(Node from, Node to); //для связанных графов будет генерить исключения
        void AddParent(Node target);//для связанных графов будет генерить исключения
    }
}
