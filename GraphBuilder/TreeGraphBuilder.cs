﻿using EVT.Applications.GraphSolution.Graph;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Graph.GraphBuilder
{
    public class TreeGraphBuilder<TNode> : GraphBuilderBase<TNode> where TNode : Node, new()
    {
        public override Node AddRootNode()
        {
            var newnode = new TNode();
            _Graph.AddNode(newnode);
            return newnode;
        }
        public override Node AddChild(Node parentNode)
        {
            var newnode = new TNode();
            _Graph.AddEdge(parentNode, newnode);
            return newnode;
        }

        public override Node[] AddChildren(Node parentNode, Int32 childrenCount)
        {
            var newnodes = new Node[childrenCount];
            for (var i = 0; i < childrenCount; i++)
            {
                newnodes[i] = new TNode();
                _Graph.AddEdge(parentNode, newnodes[i]);
            }
            return newnodes;
        }

        protected override bool CanSetGraph(DirectedGraph graph, ref Exception ex)
        {
            return base.CanSetGraph(graph, ref ex) &&
                !TryGetInvalidEqualCastException<TreeGraph>(graph, nameof(graph), ref ex);
        }


        public TreeGraphBuilder()
        {
        }
    }
}
