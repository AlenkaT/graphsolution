﻿
using EVT.Applications.GraphSolution.Graph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.GraphInfo
{
    
    public class EdgeInfo<TEdgeInfo>: Interfaces.IEdgeInfo<TEdgeInfo>
    {
        public Edge Edge { get; }
        public TEdgeInfo Info { get; }

        internal EdgeInfo(Edge edge, TEdgeInfo edgeInfo)
        {
            Edge = edge;
            Info = edgeInfo;
        }
    }
}
