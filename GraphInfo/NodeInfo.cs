﻿
using EVT.Applications.GraphSolution.Graph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EVT.Applications.GraphSolution.GraphInfo.Interfaces;

namespace EVT.Applications.GraphSolution.GraphInfo
{

    public class NodeInfo<T> : INodeInfo<T>, INodeInfo
    {
        public Node Node { get; }
        public T Info {get;}

       
        Object INodeInfo.Info => Info;

        //public String NodeInfoType { get; }

        public NodeInfo(Node node, T nodeInfo)
        { 
            Node = node;
            Info = nodeInfo;
           // NodeInfoType = nodeInfoType;
        }
    }
}
