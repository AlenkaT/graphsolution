﻿
using EVT.Applications.GraphSolution.Graph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.GraphInfo.Interfaces
{
    
    public interface IEdgeInfo<TEdgeInfo>
    {
        Edge Edge { get; }
        TEdgeInfo Info { get; }
    }
}
