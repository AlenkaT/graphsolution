﻿using EVT.Applications.GraphSolution.Graph;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.GraphInfo.Interfaces
{
    public interface INodeInfo<out T>
    {
        Node Node { get; }
        T Info { get; }
    }

    public interface INodeInfo
    {
        Node Node { get; }
        Object Info { get; }
     //   String NodeInfoType { get; }
    }
}
