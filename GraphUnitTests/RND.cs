﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphUnitTests
{
    class RND
    {
        internal static readonly Random InstRND = new Random();
        internal static byte GetPositiveRNDByte(int maxValue = byte.MaxValue) { return (byte)InstRND.Next(1, (byte)maxValue); }
        internal static byte GetNextByte(int minValue, int maxValue = byte.MaxValue) { return (byte)InstRND.Next((byte)minValue, (byte)maxValue); }



    }
}
