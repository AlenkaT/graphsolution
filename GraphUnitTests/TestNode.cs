﻿
using EVT.Applications.GraphSolution.Graph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphUnitTests
{
    class Counter
    {
        Int32 NodeCounter;
        internal Int32 IncreaseNodeCounter()
        {
            return NodeCounter++;
        }
    }
    class TestNode: Node
    {
        internal readonly Int32 ID;
        internal static TestNode CreateTestNode()
        {
            return new TestNode(NodeCounters.TestNodeCounter.IncreaseNodeCounter());
        }
        protected TestNode(Int32 id)
        {
            ID = id;
        }

        public override bool Equals(object obj)
        {
            var node1 = obj as TestNode;
            return node1 == null ? false: node1.ID == ID;
        }

    }
    static class NodeCounters
    {
        internal static readonly Counter TestNodeCounter = new Counter();
        
    }
}
