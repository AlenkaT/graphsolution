﻿
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using EVT.Applications.GraphSolution.Graph;

namespace GraphUnitTests
{
    #region Test Helper
    class DirectedAcyclicGraphTestHelper : DirectedAcyclicGraph
    {
        internal IEnumerable<Node> Nodes { get { return base._Nodes; } }
        internal IEnumerable<Edge> Edges { get { return base._Edges; } }


        internal new Boolean CanAddEdge(Node from, Node to, ref Exception ex)
        {
            return base.CanAddEdge(from, to, ref ex);
        }

        //internal new bool IsAlternativePathExist(IEnumerable<Node> from, IEnumerable<Node> to, Boolean isDirect)
        //{
        //    return base.IsAlternativePathExist(from, to, isDirect);
        //}

        internal new bool DoesAlternativePathExist(Edge edge, IEnumerable<Edge> exceptEdges)
        {
            return base.DoesAlternativePathExist(edge, exceptEdges);
        }

        internal new bool DoesDirectedPathExist(Node from, Node to)
        {
            return base.DoesDirectedPathExist(from, to);
        }

        internal new ReadOnlyCollection<Node> GetConnectedNodes(Node node, bool isDirected, IEnumerable<Node> exceptNodes = null)
        {
            return base.GetConnectedNodes(node, isDirected, exceptNodes);
        }

        #region C-tors
        internal DirectedAcyclicGraphTestHelper(IEnumerable<Node> nodes = null, IEnumerable<Edge> edges = null)
        {
            if (nodes != null)
            {
                _Nodes.AddRange(nodes);
            }
            if (edges != null)
            {
                _Edges.AddRange(edges);
            }

        }

        #endregion

    }
    #endregion

    [TestFixture]
    class DirectedAcyclicGraphTests
    {
        [Test]
        public void CanAddEdge_WhenReferencesAreEqual_ArgumentExceptionExpaxted()
        {
            //Arrange
            var graph = TestGraphHelper.CreateRandomDAG(true); //делаем связаный граф

            Exception ex = null;

            //Act
            var result = graph.CanAddEdge(graph.Nodes.Last(), graph.Nodes.Last(), ref ex);

            //Assertion
            Assert.IsTrue(!result && ex is ArgumentException);
        }

        [Test]
        public void CanAddEdge_WhenInversePathExists_ArgumentExceptionExpaxted()
        {
            //Arrange
            var graph = TestGraphHelper.CreateRandomDAG(true); //делаем связаный граф

            Exception ex = null;

            //Act
            var result = graph.CanAddEdge(graph.Nodes.Last(), graph.Nodes.First(), ref ex);

            //Assertion
            Assert.IsTrue(!result && ex is ArgumentException);
        }

        [Test]
        public void CanAddEdge_WhenInverseEdgeExists_ArgumentExceptionExpaxted()
        {
            //Arrange
            var graph = TestGraphHelper.CreateRandomDAG(true); //делаем связаный граф

            Exception ex = null;

            //Act
            var result = graph.CanAddEdge(graph.Edges.Last().To, graph.Edges.Last().From, ref ex);

            //Assertion
            Assert.IsTrue(!result && ex is ArgumentException);
        }

        [Test]
        public void CanAddEdge_WhenInversePathDoesNotExist_TrueExpaxted()
        {
            //Arrange
            var graph = TestGraphHelper.CreateRandomDAG(false); //делаем НЕсвязаный граф
            Exception ex = null;

            //Act
            var result = graph.CanAddEdge(graph.Nodes.Last(), graph.Nodes.First(), ref ex);

            //Assertion
            Assert.IsTrue(result && ex == null);
        }


        [Test]
        public void DoesAlternativePathExist_WhenItDoesWithoutExceptNodes_TrueExpaxted()
        {
            //Arrange
            var graph1 = TestGraphHelper.CreateRandomDAG(true); //делаем связаный граф
            var nodes = graph1.Nodes;
            var edge = new Edge(nodes.First(), nodes.Last());
            
            //добавляем прямую связь между первой и последней вершинами
            var graph = new DirectedAcyclicGraphTestHelper(nodes, graph1.Edges.Union(
                new Edge[] { edge }));

            //Act
            var result = graph.DoesAlternativePathExist(edge, null);

            //Assertion
            Assert.IsTrue(result && graph.LoopCounter > 0);

        }

        [Test]
        public void DoesAlternativePathExist_WhenItDoesNotWithoutExceptNodes_FalseExpaxted()
        {
            //Arrange
            var graph1 = TestGraphHelper.CreateRandomDAG(false); //делаем несвязаный граф
            var nodes = graph1.Nodes;
            var edge = new Edge(nodes.First(), nodes.Last());
            //добавляем прямую связь между первой и последней вершинами
            var graph = new DirectedAcyclicGraphTestHelper(nodes, graph1.Edges.Union(
                new Edge[] { edge }));

            //Act
            var result = graph.DoesAlternativePathExist(edge, null);

            //Assertion
            Assert.IsFalse(result );
            Assert.Greater(graph.LoopCounter, 0);
        }

        [Test]
        public void DoesAlternativePathExist_WhenItDoesWithExceptNodes_TrueExpaxted()
        {
            //Arrange
            var graph1 = TestGraphHelper.CreateRandomDAG(true); //делаем связаный граф
            var nodes = graph1.Nodes;
            var exceptNode = graph1.Edges.Last().From;
            var edge = new Edge(nodes.First(), nodes.Last());
            var exceptEdge = new Edge(nodes.First(), exceptNode);
            //добавляем прямую связь между первой и последней вершинами
            var graph = new DirectedAcyclicGraphTestHelper(nodes, graph1.Edges.Union(
                new Edge[] { edge, exceptEdge}));

            //Act
            var result = graph.DoesAlternativePathExist(edge, new Edge[] { exceptEdge});

            //Assertion
            Assert.IsTrue(result && graph.LoopCounter > 0);

        }

        [Test]
        public void DoesAlternativePathExist_WhenItDoesNotWithExceptNodes_FalseExpaxted()
        {
            //Arrange
            var graph1 = TestGraphHelper.CreateRandomDAG(false); //делаем несвязаный граф
            var nodes = graph1.Nodes;
            var exceptNode = graph1.Edges.Last().From;
            //добавляем прямую связь между первой и последней вершинами
            var edge = new Edge(nodes.First(), nodes.Last());
            var exceptEdge = new Edge(nodes.First(), exceptNode);
            var graph = new DirectedAcyclicGraphTestHelper(nodes, graph1.Edges.Union(
                new Edge[] { edge, exceptEdge }));

            //Act
            var result = graph.DoesAlternativePathExist(edge, new Edge[] { exceptEdge });

            //Assertion
            Assert.IsFalse(result);
            Assert.Greater(graph.LoopCounter,0);
        }

        [Test]
        public void DoesDirectedPathExist_WhenItDoes_TrueExpaxted()
        {
            //Arrange
            var graph = TestGraphHelper.CreateRandomDAG(true); //делаем связаный граф

            //Act
            var result = graph.DoesDirectedPathExist(graph.Nodes.First(), graph.Nodes.Last());

            //Assertion
            Assert.IsTrue(result);

        }

        [Test]
        public void DoesDirectedPathExist_WhenItDoesNot_FalseExpaxted()
        {
            //Arrange
            var graph = TestGraphHelper.CreateRandomDAG(false); //делаем несвязаный граф

            //Act
            var result = graph.DoesDirectedPathExist(graph.Nodes.First(), graph.Nodes.Last());

            //Assertion
            Assert.IsFalse(result);

        }

        //[Test]
        //public void IsAlternativePathExist_WhenItExistsWithoutDirect2_TrueExpaxted()
        //{
        //    //солнышко , последний уровень которого дополнительно соединен по периметру
        //    // при разрыве прямых связей любого уровня (меньше чем количество точек уровня)
        //    // будет существовать другой путь между точками
        //    //Arrange
        //    byte level = 10;
        //    var graph1 = TestGraphHelper.CreateCDAG(level); //делаем солнышко
        //    var nodes = new List<Node>(graph1.GetNodes());
        //    var edges = new List<Edge>(graph1.GetEdges());
        //    var count = (byte)((graph1.GetNodes().Count() - 1) / level); // количество точек уровня
        //    //обвязка солнышка в кружок
        //    var k = TestGraphHelper.RND.Next(1, level - 1); //определяет связи какого уровня будут разрываться, считая с конца лучей
        //    nodes.Reverse();
        //    var from = new List<Node>(nodes.Skip(count*(k-1)).Take(count-1));
        //    var to = new List<Node>(nodes.Skip(count*k).Take(count-1));
        //    for (var i = 0; i < count - 1; i++)
        //    {
        //        edges.Add(new Edge(nodes.ElementAt(i), nodes.ElementAt(i + 1))); //обратная обвязка
        //    }
        //    nodes.Reverse();
        //    var graph = new DirectedAcyclicGraphTestHelper(nodes, edges);
        //    //Act
        //    var result = graph.IsAlternativePathExist(from, to, false);

        //    //Assertion
        //    Assert.IsTrue(result);

        //}


        //[Test]
        //public void IsAlternativePathExist_WhenItDoesNotExistWithoutDirect2_FalseExpaxted()
        //{
        //    //Arrange
        //    byte level = 10;
        //    var graph1 = TestGraphHelper.CreateCDAG(level); //делаем солнышко
        //    var nodes = new List<Node>(graph1.GetNodes());
        //    var edges = new List<Edge>(graph1.GetEdges());
        //    var count = (byte)((graph1.GetNodes().Count() - 1) / level ); // количество точек уровня
        //    //обвязка солнышка в кружок
        //    var k = TestGraphHelper.RND.Next(1, level-1); //определяет связи какого уровня будут разрываться, считая с конца лучей
        //    nodes.Reverse();
        //    var from = new List<Node>(nodes.Skip(count*(k-1)).Take(count));
        //    var to = new List<Node>(nodes.Skip(count*k).Take(count));
        //    for (var i = 0; i < count-1; i++)
        //    {
        //        edges.Add(new Edge(nodes.ElementAt(i), nodes.ElementAt(i + 1))); //обратная обвязка
        //    }
        //    nodes.Reverse();
        //    var graph = new DirectedAcyclicGraphTestHelper(nodes, edges);
        //    //Act
        //    var result = graph.IsAlternativePathExist(from, to, false);

        //    //Assertion
        //    Assert.IsFalse(result);

        //}

        [Test]
        public void GetConnectedNodes_WhenExceptNodesIsNotNullAndSearchIsNotDirected_ResultDoesNotContainExceptNodesExpacted()
        {
            //Arrange
            byte level = 1; //!!!! тест  будет проходить только при 1, так как узлы exNodes ИСКЛЮЧАЮТСЯ из обхода, и подключенные к ним узлы в результат не попадают
            var n1 = RND.GetNextByte(2, 5); //количество точек уровня
            var graph = TestGraphHelper.CreateCDAG(level, n1); //солнышко - 1 уровень
            var oldedges = graph.GetEdges();
            var edges = new List<Edge>();
            var nodes = new List<Node>();
            foreach (var edge in oldedges)
            {
                //дополняем солнышко входящими лучами
                nodes.Add(TestNode.CreateTestNode());
                edges.Add(new Edge(nodes.Last(), edge.From));
            }
            var graph1 = new DirectedAcyclicGraphTestHelper(nodes.Union(graph.GetNodes()).OrderBy(node=>((TestNode)node).ID), edges.Union(oldedges));
            var exNodes = new List<Node>();

            var countExNodes = RND.GetPositiveRNDByte(n1 - 1) * 2;
            for (var i = 0; i < countExNodes; i++)
            {
                exNodes.Add(graph1.Nodes.ElementAt(RND.GetPositiveRNDByte(2 * n1)));
            }

            var startNode = graph.GetNodes().ElementAt(0);
            //Act
            //получение смежных точек, исключая на выходе  exNodes

            var result = graph1.GetConnectedNodes(startNode, false, exNodes);

            var expacted = graph1.Nodes.Except(exNodes.Distinct().Union(new Node[] { startNode}));

            //Assert
            CollectionAssert.AreEquivalent(expacted, result);

        }


        //[Test]
        //тест не нужен, результат вызова аналогичен родительскому
        //public void GetConnectedNodes_WhenExceptNodesIsNotNullAndSearchIsDirected_ResultDoesNotContainExceptNodesExpacted()
       

        [Test]
        public void GetConnectedNodes_WhenExceptNodesIsNullAndSearchIsNotDirected_ResultContainsAllGraphExpacted()
        {
            //Arrange
            byte level = 1; //!!!! тест  будет проходить только при 1, так как узлы exNodes ИСКЛЮЧАЮТСЯ из обхода, и подключенные к ним узлы в результат не попадают
            var n1 = RND.GetNextByte(2, 15); //количество точек уровня
            var graph = TestGraphHelper.CreateCDAG(level, n1); //солнышко - 1 уровень
            var oldedges = graph.GetEdges();
            var edges = new List<Edge>();
            var nodes = new List<Node>();
            foreach (var edge in oldedges)
            {
                //дополняем солнышко
                nodes.Add(TestNode.CreateTestNode());
                edges.Add(new Edge(nodes.Last(), edge.From));
            }
            var graph1 = new DirectedAcyclicGraphTestHelper(nodes.Union(graph.GetNodes()), edges.Union(oldedges));

            var startNode = graph.GetNodes().ElementAt(0);
            //Act
            //получение смежных точек, исключая на выходе  exNodes

            var result = graph1.GetConnectedNodes(startNode, false, null);

            var expacted = graph1.Nodes.Except(new Node[] { startNode });

            //Assert
            CollectionAssert.AreEquivalent(expacted, result);

        }



       // [Test]
       // Тест не нужен, так как результат вызова аналогичен родительскому
        //public void GetConnectedNodes_WhenExceptNodesIsNullAndSearchIsDirected_ResultDoesNotContainParentsNodesExpacted()


    }
}
