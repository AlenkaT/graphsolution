﻿
using EVT.Applications.GraphSolution.Graph;
using EVT.Applications.GraphSolution.Graph.Messages;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphUnitTests
{
    #region Test Helper
    class ConnectedDirectedAcyclicGraphTestHelper : ConnectedDirectedAcyclicGraph
    {
        EnTestMode _testMode;

        internal ReadOnlyCollection<Node> Nodes { get { return base._Nodes.ToList().AsReadOnly(); } }
        internal ReadOnlyCollection<Edge> Edges { get { return base._Edges.ToList().AsReadOnly(); } }



        internal new void DoAddNode(Node node)
        {
            base.DoAddNode(node);
        }
        internal new Boolean CanAddNode(Node node, ref Exception ex)
        {
            return base.CanAddNode(node, ref ex);
        }



        internal new Boolean CanAddEdge(Node from, Node to, ref Exception ex)
        {
            return base.CanAddEdge(from, to, ref ex);
        }

        internal new Boolean CanRemoveEdge(Edge edge, out int nodeNumber, ref Exception ex)
        {
            WhichNodeWillBeRemove node;
            var result=base.CanRemoveEdge(edge, out node, ref ex);
            nodeNumber = (int)node;
            return result;
        }

        internal new Boolean CanRemoveNode(Node node, ref Exception ex)
        {
            return base.CanRemoveNode(node, ref ex);

        }

       
        internal new IEnumerable<Edge> FindEdges(Node node, Boolean isDirected, Edge exceptEdge)
        {
            return base.FindEdges(node, isDirected, exceptEdge);
        }



        #region C-tors
        internal ConnectedDirectedAcyclicGraphTestHelper(IEnumerable<Node> nodes = null, IEnumerable<Edge> edges = null, EnTestMode testMode = EnTestMode.normal)
        {
            if (nodes != null)
            {
                _Nodes.AddRange(nodes);
            }
            if (edges != null)
            {
                _Edges.AddRange(edges);
            }
            _testMode = testMode;
        }

        #endregion
    }
    #endregion


    [TestFixture]
    class ConnectedDirectedAcyclicGraphTests
    {
        [Test]
        public void CanAddNode_WhenNodesIsNotEmpty_InvalidOperationExceptionExpacted()
        {
            //Arrange
            var n1 = new Node();

            var graph = new ConnectedDirectedAcyclicGraphTestHelper(nodes: new Node[] { n1 });
            Exception ex = null;

            //Act
            var result = graph.CanAddNode(new Node(), ref ex);

            //Assertion
            Assert.IsTrue(!result && ex is InvalidOperationException);
        }

        [Test]
        public void CanAddNode_WhenNodeIsNull_ArgumentNullExceptionExpacted()
        {
            //Arrange
            var graph = new ConnectedDirectedAcyclicGraphTestHelper();
            Exception ex = null;

            //Act
            var result = graph.CanAddNode(null, ref ex);

            //Assertion
            Assert.IsTrue(!result && ex is ArgumentNullException);
        }

        [Test]
        public void CanAddNode_WhenNodesIsEmpty_TrueExpacted()
        {
            //Arrange
            var graph = new ConnectedDirectedAcyclicGraphTestHelper();
            Exception ex = null;

            //Act
            var result = graph.CanAddNode(new Node(), ref ex);

            //Assertion
            Assert.IsTrue(result && ex == null);
        }

        //Вследствие того, что происходит проверка существования узла летит эксепшн
        [Test]
        public void CanAddEdge_WhenAnyNodeIsContained_TrueExpacted()
        {
            //Arrange
            var n1 = new Node();
            var graph = new ConnectedDirectedAcyclicGraphTestHelper(nodes: new Node[] { n1 });
            Exception ex = null;

            //Act
            var result = graph.CanAddEdge(new Node(), n1, ref ex);

            //Assertion
            Assert.IsTrue(result && ex == null);
        }

        [Test]
        public void CanAddEdge_WhenBothNodesAreNotContained_ArgumentExceptionExpacted()
        {
            //Arrange
            var graph = new ConnectedDirectedAcyclicGraphTestHelper();
            Exception ex = null;

            //Act
            var result = graph.CanAddEdge(new Node(), new Node(), ref ex);

            //Assertion
            Assert.IsTrue(!result && ex is ArgumentException);
        }



        [Test]
        public void CanRemoveEdge_WhenEdgeIsEnding_TrueExpacted()
        {
            //Arrange
            var graph1 = TestGraphHelper.CreateRandomDAG(true); //делаем связаный граф

            var edge = graph1.Edges.Last();
            var graph = new ConnectedDirectedAcyclicGraphTestHelper(nodes: graph1.Nodes, edges: graph1.Edges);

            //Act
            int nodeNumber;
            Exception ex = null;
            var result=graph.CanRemoveEdge(edge, out nodeNumber, ref ex);

            //Assert
            Assert.IsTrue(result);
            Assert.AreEqual(1, nodeNumber);
        }

        [Test]
        public void CanRemoveEdge_WhenEdgeIsBegining_TrueExpacted()
        {
            //Arrange
            var graph1 = TestGraphHelper.CreateRandomDAG(true); //делаем связаный граф

            var edge = new Edge(TestNode.CreateTestNode(), graph1.Nodes.Last());
            var graph = new ConnectedDirectedAcyclicGraphTestHelper(nodes: graph1.Nodes, edges: graph1.Edges.Union(new Edge[] { edge }));

            //Act
            int nodeNumber;
            Exception ex = null;
            var result = graph.CanRemoveEdge(edge, out nodeNumber, ref ex);

            //Assert
            Assert.IsTrue(result);
            Assert.AreEqual(0, nodeNumber);
        }

        [Test]
        public void CanRemoveEdge_WhenEdgeIsOnlyOne_TrueExpacted()
        {
            //Arrange

            var from = new Node();
            var to = new Node();
            var edge = new Edge(from, to);
            var graph = new ConnectedDirectedAcyclicGraphTestHelper(nodes: new Node[] { from, to}, edges: new Edge[] { edge});

            //Act
            int nodeNumber;
            Exception ex = null;
            var result = graph.CanRemoveEdge(edge, out nodeNumber, ref ex);

            //Assert
            Assert.IsTrue(result);
            Assert.AreEqual(1, nodeNumber);
            //CollectionAssert.IsEmpty(graph.Edges);
            //CollectionAssert.IsEmpty(graph.Nodes);
        }

        [Test]
        public void CanRemoveEdge_WhenAlternativePathExists_TrueExpacted()
        {
            //Arrange
            var graph1 = TestGraphHelper.CreateRandomDAG(true); //делаем связаный граф
            //Альтернативная связь
            var newedge = new Edge(graph1.Nodes.First(), graph1.Nodes.Last());
            var graph = new ConnectedDirectedAcyclicGraphTestHelper(nodes: graph1.Nodes, edges: graph1.Edges.Union(new Edge[] { newedge }));

            //Act
            int nodeNumber;
            Exception ex = null;
            var result = graph.CanRemoveEdge(newedge, out nodeNumber, ref ex);

            //Assert
            Assert.IsTrue(result);
            Assert.AreEqual(2, nodeNumber);

        }
        [Test]
        public void CanRemoveEdge_WhenAlternativePathDoesNotExist_FalseExpacted()
        {
            //Arrange
            var graph1 = TestGraphHelper.CreateRandomDAG(false); //делаем НЕсвязаный граф
            //Необходимая связь
            var newedge = new Edge(graph1.Nodes.First(), graph1.Nodes.Last());
            var graph = new ConnectedDirectedAcyclicGraphTestHelper(nodes: graph1.Nodes, edges: graph1.Edges.Union(new Edge[] { newedge }));

            //Act
            int nodeNumber;
            Exception ex = null;
            var result = graph.CanRemoveEdge(newedge, out nodeNumber, ref ex);

            //Assert
            Assert.IsTrue(!result && ex is ArgumentException);
            Assert.AreEqual(2, nodeNumber);

        }

        /*
        //[Test]
        //public void DoRemoveEdge_WhenFromNodeIsEndNode_RemovingNodeAndEdgeExpacted()
        //{
        //    //Arrange
        //    var n1 = new Node();
        //    var n2 = new Node();
        //    var n3 = new Node();
        //    var edge1 = new Edge(n1, n2);
        //    var edge2 = new Edge(n2, n3);
        //    var graph = new ConnectedDirectedAcyclicGraphTestHelper(nodes: new Node[] { n1, n2, n3 }, edges: new Edge[] { edge1, edge2 });

        //    //Act
        //    //graph.DoRemoveEdge(edge1);

        //    //Assert
        //    CollectionAssert.DoesNotContain(graph.Edges, edge1);
        //    CollectionAssert.DoesNotContain(graph.Nodes, n1);
        //    CollectionAssert.Contains(graph.Edges, edge2);
        //    CollectionAssert.Contains(graph.Nodes, n2);
        //}

        //[Test]
        //public void DoRemoveEdge_WhenConnectionIsAvailable_RemovingNodeAndEdgeExpacted()
        //{
        //    //Arrange
        //    var n1 = new Node();
        //    var n2 = new Node();
        //    var n3 = new Node();
        //    var nds = new Node[] { n1, n2, n3 };
        //    var edge1 = new Edge(n1, n2);
        //    var edge2 = new Edge(n2, n3);
        //    var edge3 = new Edge(n1, n3);
        //    var graph = new ConnectedDirectedAcyclicGraphTestHelper(nodes: nds, edges: new Edge[] { edge1, edge2, edge3 }, testMode: EnTestMode.valid);

        //    //Act
        //    //graph.DoRemoveEdge(edge1);

        //    //Assert
        //    //Assert
        //    CollectionAssert.DoesNotContain(graph.Edges, edge1);
        //    CollectionAssert.Contains(graph.Edges, edge3);
        //    CollectionAssert.AreEquivalent(nds, graph.Nodes);
        //}

        //[Test]
        //public void DoRemoveEdge_WhenConnectionIsNotAvailable_ArgumentExceptionExpacted()
        //{
        //    //Arrange
        //    var n1 = new Node();
        //    var n2 = new Node();
        //    var n3 = new Node();
        //    var n4 = new Node();
        //    var nds = new Node[] { n1, n2, n3, n4 };
        //    var edge1 = new Edge(n1, n2);
        //    var edge2 = new Edge(n2, n3);
        //    var edge3 = new Edge(n3, n4);
        //    var graph = new ConnectedDirectedAcyclicGraphTestHelper(nodes: nds, edges: new Edge[] { edge1, edge2, edge3 }, testMode: EnTestMode.invalid);

        //    //Act
        //    var ex1 = Assert.Throws<ArgumentException>(delegate
        //    {
        //        //graph.DoRemoveEdge(edge2);
        //    });

        //    //Assert
        //    Assert.That(ex1.ParamName, Is.EqualTo("edge"));
        //}

    */

        [Test]
        public void CanRemoveNode_WhenNodeHasOneEdge_TrueExpacted()
        {

            //Arrange
            var graph1 = TestGraphHelper.CreateRandomDAG(true); //делаем связаный граф
            
            var lastNode = graph1.Nodes.Last();

            var graph = new ConnectedDirectedAcyclicGraphTestHelper(nodes: graph1.Nodes, edges: graph1.Edges);
            Exception ex = null;

            //Act
            var result = graph.CanRemoveNode(lastNode, ref ex);

            //Assert
            Assert.IsTrue(result && ex == null);
            Assert.AreEqual(0,graph.LoopCounter);
        }


        [Test]
        public void CanRemoveNode_WhenNodeIsEnding_TrueExpacted()
        {
            
            //Arrange
            var graph1 = TestGraphHelper.CreateRandomDAG(true); //делаем связаный граф
            var firstNode = graph1.Nodes.First();
            var lastNode = graph1.Nodes.Last();
            //Доп связи
            var newedges = new List<Edge>() { new Edge(firstNode, lastNode) };
            var newNodes = new List<Node>();
            var count = RND.GetPositiveRNDByte(20);
            for (var i=0; i<count; i++)
            {
                var newNode = TestNode.CreateTestNode();
                newNodes.Add(newNode);
                newedges.Add(new Edge(firstNode, newNode));
                newedges.Add( new Edge(newNode, lastNode));
            }

            var graph = new ConnectedDirectedAcyclicGraphTestHelper(nodes: graph1.Nodes.Union(newNodes), edges: graph1.Edges.Union(newedges) );
            Exception ex = null;

            //Act
            var result = graph.CanRemoveNode(lastNode, ref ex);

            //Assert
            Assert.IsTrue(result && ex ==null);
            Assert.AreEqual(0, graph1.LoopCounter);
        }

        [Test]
        public void CanRemoveNode_WhenGraphWillStayConnected_TrueExpacted()
        {

            //Arrange
            var graph1 = TestGraphHelper.CreateRandomDAG(true); //делаем связаный граф
            var firstNode = graph1.Nodes.First();
            var lastNode = graph1.Nodes.Last();
            //Доп связи
            var newedges = new List<Edge>() { };
            var newNode = TestNode.CreateTestNode();

            var count = 3;
            for (var i = 0; i < count; i++)
            {
                newedges.Add(new Edge(graph1.Nodes.ElementAt(i), newNode));
                newedges.Add(new Edge(newNode, graph1.Nodes.ElementAt(graph1.Nodes .Count()- 1 - i)));
            }

            var graph = new ConnectedDirectedAcyclicGraphTestHelper(nodes: graph1.Nodes.Union(new Node[] { newNode}), edges: graph1.Edges.Union(newedges));
            Exception ex = null;

            //Act
            var result = graph.CanRemoveNode(newNode, ref ex);

            //Assert
            Assert.IsTrue(result && ex == null);
            Assert.Greater(graph.LoopCounter,0);
        }

        [Test]
        public void CanRemoveNode_WhenGraphWillNotStayConnected_FalseExpacted()
        {

            //Arrange
            var graph1 = TestGraphHelper.CreateRandomDAG(false); //делаем связаный граф
            var firstNode = graph1.Nodes.First();
            var lastNode = graph1.Nodes.Last();
            //Доп связи
            var newedges = new List<Edge>() { };
            var newNode = TestNode.CreateTestNode();

            var count = 3;
            for (var i = 0; i < count; i++)
            {
                newedges.Add(new Edge(graph1.Nodes.ElementAt(i), newNode));
                newedges.Add(new Edge(newNode, graph1.Nodes.ElementAt(graph1.Nodes.Count() - 1 - i)));
            }

            var graph = new ConnectedDirectedAcyclicGraphTestHelper(nodes: graph1.Nodes.Union(new Node[] { newNode }), edges: graph1.Edges.Union(newedges));
            Exception ex = null;

            //Act
            var result = graph.CanRemoveNode(newNode, ref ex);

            //Assert
            Assert.IsTrue(!result && ex is ArgumentException);
            Assert.Greater(graph.LoopCounter, 0);
        }

        [Test]
        public void FindEdges_WhenIsDirectEqualsTrueWitoutExceptEdge1_CorrectEdgesExpacted()
        {
            //Arrange

            var graph1= TestGraphHelper.CreateCDAG(1, RND.GetNextByte(3,15));
            var graph = new ConnectedDirectedAcyclicGraphTestHelper(graph1.GetNodes(), graph1.GetEdges());

            //Act
            var result = graph.FindEdges(graph.GetNodes().First(), true, null);

            //Assertion
            CollectionAssert.AreEquivalent(graph.GetEdges(), result);
        }

        [Test]
        public void FindEdges_WhenIsDirectEqualsTrueWitoutExceptEdge2_CorrectEdgesExpacted()
        {
            //Arrange

            var graph1 = TestGraphHelper.CreateCDAG(5, RND.GetNextByte(3,10));
            var graph = new ConnectedDirectedAcyclicGraphTestHelper(graph1.GetNodes(), graph1.GetEdges());
            //точка кроме root и крайних (концевых)
            var node = graph.GetEdges().Last().From;

            //Act
            var result = graph.FindEdges(node, true, null);

            //Assertion
            Assert.AreEqual(1, result.Count()); // только одна зависимая вершина

        }

        [Test]
        public void FindEdges_WhenIsDirectEqualsFalseWitoutExceptEdge1_CorrectEdgesExpacted()
        {
            //Arrange

            var graph1 = TestGraphHelper.CreateCDAG(5, RND.GetNextByte(3,50));
            var graph = new ConnectedDirectedAcyclicGraphTestHelper(graph1.GetNodes(), graph1.GetEdges());
            //точка кроме root и крайних (концевых)
            var node = graph.GetEdges().Last().From;

            //Act
            var result = graph.FindEdges(node, false, null);

            //Assertion
            Assert.AreEqual(2, result.Count()); // одна зависимая вершина и одна , ссылающаяся на данную

        }
        [Test]
        public void FindEdges_WhenIsDirectEqualsTrueWithExceptEdge_CorrectEdgesExpacted()
        {
            //тест верен только если проходит тест WithountExcept
            //Arrange

            var graph1 = TestGraphHelper.CreateCDAG(5, RND.GetNextByte(3, 50));
            var graph = new ConnectedDirectedAcyclicGraphTestHelper(graph1.GetNodes(), graph1.GetEdges());
            //точка кроме root и крайних (концевых)
            var node = graph.GetEdges().Last().From;

            //Act
            var result = graph.FindEdges(node, true, graph.GetEdges().Last());

            //Assertion
            CollectionAssert.DoesNotContain(result, graph.GetEdges().Last());

        }

        [Test]
        public void FindEdges_WhenIsDirectEqualsFalseWithExceptEdge1_CorrectEdgesExpacted()
        {
            //тест верен только если проходит тест WithountExcept
            //Arrange

            var graph1 = TestGraphHelper.CreateCDAG(5, RND.GetNextByte(3, 50));
            var graph = new ConnectedDirectedAcyclicGraphTestHelper(graph1.GetNodes(), graph1.GetEdges());
            //точка кроме root и крайних (концевых)
            var node = graph.GetEdges().Last().From;

            //Act
            var result = graph.FindEdges(node, false, graph.GetEdges().Last());

            //Assertion
            CollectionAssert.DoesNotContain(result, graph.GetEdges().Last());

        }

        #region Internal Methods Tests
        [Test]
        public void AddNode_WhenGraphAlreadyContainsNodes_InvalidOperationExceptionExpacted()
        {
            //Arrange

            var graph = TestGraphHelper.CreateCDAG(1, RND.GetNextByte(3, 50));

            //Act
            var ex1 = Assert.Throws<InvalidOperationException>(delegate
            {
                ((DirectedGraph)graph).AddNode(new Node());
            });

            //Assert
            Assert.That(ex1.Message, Is.EqualTo(ErrorMessages.NodeCantBeAdded));
        }

        [Test]
        public void AddEdge_WhenAnyNodeIsNull_ArgumentNullExceptionExpacted()
        {
            //Arrange

            var graph = TestGraphHelper.CreateCDAG(1, RND.GetNextByte(3, 50));

            //Act
            var ex1 = Assert.Throws<ArgumentNullException>(delegate
            {
                ((DirectedGraph)graph).AddEdge(null, TestNode.CreateTestNode());
            });

            //Assert
            Assert.That(ex1.ParamName, Is.EqualTo("from"));

            //Act
            ex1 = Assert.Throws<ArgumentNullException>(delegate
            {
                ((DirectedGraph)graph).AddEdge(TestNode.CreateTestNode(), null);
            });

            //Assert
            Assert.That(ex1.ParamName, Is.EqualTo("to"));
        }

        [Test]
        public void AddEdge_WhenBothNodesAreNotContained_ArgumentExceptionExpacted()
        {
            //Arrange

            var graph = TestGraphHelper.CreateCDAG(1, RND.GetNextByte(3, 50));

            //Act
            var ex1 = Assert.Throws<ArgumentException>(delegate
            {
                graph.AddEdge(TestNode.CreateTestNode(), TestNode.CreateTestNode());
            });

            //Assert
            Assert.That(ex1.ParamName, Is.EqualTo("from&to"));

        }

        [Test]
        public void AddEdge_WhenInversePathIsAlreadyExists_ArgumentExceptionExpacted()
        {
            //Arrange

            var graph = TestGraphHelper.CreateCDAG(5, RND.GetNextByte(3, 50));

            //Act
            var ex1 = Assert.Throws<ArgumentException>(delegate
            {
                graph.AddEdge(graph.GetNodes().Last(), graph.GetNodes().First());
            });

            //Assert
            Assert.That(ex1.Message, Is.EqualTo("Relation between nodes To-From already exists"));

        }

        [Test]
        public void AddEdge_WhenNodesReferencesAreEqual_ArgumentExceptionExpacted()
        {
            //Arrange

            var graph = TestGraphHelper.CreateCDAG(1, RND.GetNextByte(3, 50));

            //Act
            var ex1 = Assert.Throws<ArgumentException>(delegate
            {
                ((DirectedGraph)graph).AddEdge(graph.GetNodes().Last(), graph.GetNodes().Last());
            });

            //Assert
            Assert.That(ex1.Message, Is.EqualTo(ErrorMessages.ArgRefEqual));

        }

        [Test]
        public void AddEdge_WhenAnyNodesIsNotContained_NodesContainsAddingNodeAndEdgesContainsAddingEdgeExpacted()
        {
            //Arrange

            var graph = TestGraphHelper.CreateCDAG(1, RND.GetNextByte(3, 50));
            var nodes = graph.GetNodes();
            var newNode = TestNode.CreateTestNode();
            var n1 = nodes.Last();

            //Act

            ((DirectedGraph)graph).AddEdge(n1, newNode);

            //Assert
            CollectionAssert.Contains(nodes, newNode);
            var edge = graph.GetEdges().Where(ed => ed.From.Equals(n1)).Where(ed => ed.To.Equals(newNode)).Single();
            Assert.IsNotNull(edge);

        }

        [Test]
        public void AddEdge_WhenBothNodesAreContained_EdgesContainsAddingEdgeExpacted()
        {
            //но прямого пути From-To нет
            //Arrange

            var graph = TestGraphHelper.CreateCDAG(2, RND.GetNextByte(3, 50));

            var n1 = graph.GetNodes().First();
            var n2 = graph.GetNodes().Last();

            //Act

            ((DirectedGraph)graph).AddEdge(n1, n2);

            //Assert

            var edge = graph.GetEdges().Where(ed => ed.From.Equals(n1)).Where(ed => ed.To.Equals(n2)).Single();
            Assert.IsNotNull(edge);

        }

        [Test]
        public void AddEdge_WhenFromToEdgeAlreadyExists_EdgesIsNotChangedExpacted()
        {
            //Arrange

            var graph = TestGraphHelper.CreateCDAG(3, RND.GetNextByte(3, 50));
            var expacted = graph.GetEdges().ToList();
            //Act
            var edge = graph.GetEdges().First();
            var newNode = TestNode.CreateTestNode();

            graph.AddEdge(edge.From, edge.To);

            //Assert
            CollectionAssert.AreEquivalent(expacted, graph.GetEdges());
            
        }

        [Test]
        public void RemoveEdge_WhenConnectedNodeIsNotComposite_EdgesDoesNotContainEdgeAndNodesDoesNotContainNodeExpacted()
        {
            //Arrange

            var graph = TestGraphHelper.CreateCDAG(3, RND.GetNextByte(3, 50));
            var lastEdge = graph.GetEdges().Last();
            var lastNode = lastEdge.To;
            //Act

            graph.RemoveEdge(lastEdge.From, lastEdge.To);

            //Assert

            CollectionAssert.DoesNotContain(graph.GetEdges(), lastEdge);
            CollectionAssert.DoesNotContain(graph.GetNodes(), lastNode);
        }

        [Test]
        public void RemoveEdge_WhenGraphRemainsConnected_EdgesDoesNotContainEdgeExpacted()
        {
            //Arrange
            byte level = 5;
            var count = RND.GetNextByte(3, 50);
            var graph1 = TestGraphHelper.CreateCDAG(level, count);
            
            var nodes = new List<Node>(graph1.GetNodes());
            var edges = new List<Edge>(graph1.GetEdges());
            //обвязка солнышка в кружок

            for (var i=0; i<count-1;i++)
            {
                edges.Add(new Edge(nodes.ElementAt(nodes.Count-1-i), nodes.ElementAt(nodes.Count - 1 - i-1))); //обратная обвязка
            }

            var graph = new ConnectedDirectedAcyclicGraphTestHelper(graph1.GetNodes(), edges);
            var edge = graph.GetEdges().ElementAt(RND.GetPositiveRNDByte(count));


            //Act

            graph.RemoveEdge(edge.From, edge.To); //удаляем любую из связей первого уровня солнышка

            //Assert

            CollectionAssert.DoesNotContain(graph.GetEdges(), edge);
        }

        [Test]
        [Repeat(10)]
        public void RemoveEdge_WhenGraphWillNotBeConnected_ArgumentExceptionExpacted()
        {
            //Arrange
            byte level = 5;
            var count = RND.GetNextByte(3, 50);
            var graph1 = TestGraphHelper.CreateCDAG(level, count);          
            var graph = new ConnectedDirectedAcyclicGraphTestHelper(graph1.GetNodes(), graph1.GetEdges());
           
            //любая связь , кроме первого и последнего уровня
            var edge = graph.GetEdges().ElementAt(RND.GetNextByte(count, graph.GetEdges().Count-count-1));

            //Act
            var ex1 = Assert.Throws<ArgumentException>(delegate
            {
                graph.RemoveEdge(edge.From, edge.To); 
            });

            //Assert
            CollectionAssert.IsSubsetOf(ErrorMessages.CantBeDeleted, ex1.Message);
        }

        [Test]
        
        public void RemoveNode_WhendNodeIsEndingNode_EdgesDoesNotContainConnectedEdgesAndNodesDoesNotContainNodeExpacted()
        {
            //Arrange

            var graph1 = TestGraphHelper.CreateCDAG(1, RND.GetNextByte(3, 50));
            var count = graph1.GetEdges().Count;

            var newNode = TestNode.CreateTestNode();
            var nodes = graph1.GetNodes().Union(new Node[] { newNode });
            var newedges = new List<Edge>();
            for (var i= 0; i < count;i++)
            {
                newedges.Add(new Edge(nodes.ElementAt(1 + i), newNode));
            }
            var graph = new ConnectedDirectedAcyclicGraphTestHelper(nodes, graph1.GetEdges().Union(newedges));

            //Act

            graph.RemoveNode(newNode);

            //Assert

            CollectionAssert.AreEquivalent(graph1.GetEdges(), graph.GetEdges());
            CollectionAssert.DoesNotContain(graph.GetNodes(), newNode);
        }


        [Test]
        public void RemoveNode_WhenGraphWillNotBeConnected_ArgumentExceptionExpacted()
        {
            //Arrange
            byte level = 5;
            var count = RND.GetNextByte(3, 50);
            var graph1 = TestGraphHelper.CreateCDAG(level, count);
            var graph = new ConnectedDirectedAcyclicGraphTestHelper(graph1.GetNodes(), graph1.GetEdges());
            //удаляем любой узел, кроме корня и ущлов последнего уровня
            var node = graph.GetNodes().ElementAt(RND.GetNextByte(1, graph.GetNodes().Count-1-count));

            //Act
            var ex1 = Assert.Throws<ArgumentException>(delegate
            {
                graph.RemoveNode(node);
            });

            //Assert
            CollectionAssert.IsSubsetOf(ErrorMessages.CantBeDeleted, ex1.Message);

        }

        [Test]
        public void RemoveNode_WhenGraphRemainsConnected_EdgesDoesNotContainConnectedEdgesAndNodesDoesNotContainNodeExpacted()
        {
            //Arrange
            byte level = 5;
            var count = RND.GetNextByte(3, 50);
            var graph1 = TestGraphHelper.CreateCDAG(level, count);
            var nodes = new List<Node>(graph1.GetNodes());
            var edges = new List<Edge>(graph1.GetEdges());
            //обвязка солнышка в кружок

            for (var i = 0; i < count - 1; i++)
            {
                edges.Add(new Edge(nodes.ElementAt(nodes.Count - 1 - i), nodes.ElementAt(nodes.Count - 1 - i - 1))); //обратная обвязка
            }

            var graph = new ConnectedDirectedAcyclicGraphTestHelper(nodes, edges);
            //удаляем любой узел, кроме корня и ущлов последнего уровня
            var node = graph.GetNodes().ElementAt(RND.GetNextByte(1, graph.GetNodes().Count - 1 - count));
            var connectededges=graph.FindEdges(node, false, null);
            //Act

            graph.RemoveNode(node); 

            //Assert

            CollectionAssert.DoesNotContain(graph.GetNodes(), node);
            CollectionAssert.IsEmpty(graph.GetEdges().Intersect(connectededges));
        }


        [Test]
        public void RemoveNode_WhenGraphWillNotBeConnwcted_ArgumentExceptionExpacted()
        {
            //специально написан как сценарий с множеством Assert
            // удаляем точки по очереди, когда удаляется последняя в ряду - летит исключение
            //Arrange
            byte level = 10;
            var count = RND.GetNextByte(3, 50);
            var graph1 = TestGraphHelper.CreateCDAG(level, count);
            var nodes = new List<Node>(graph1.GetNodes());
            var edges = new List<Edge>(graph1.GetEdges());
            //обвязка солнышка в кружок

            for (var i = 0; i < count - 1; i++)
            {
                edges.Add(new Edge(nodes.ElementAt(nodes.Count - 1 - i), nodes.ElementAt(nodes.Count - 1 - i - 1))); //обратная обвязка
            }

            var graph = new ConnectedDirectedAcyclicGraphTestHelper(nodes, edges);
            //уровень, с которого удаляем вершины
            var k = RND.GetNextByte(1, level-1); // 
            var delNodes = graph.GetNodes().Skip(1 + (k - 1) * count).Take(count).ToList();
            //Act
            for( var i=0; i<count-1; i++)
            {
                //тут нет эксепшна
                graph.RemoveNode(delNodes[i]);
                CollectionAssert.DoesNotContain(graph.GetNodes(), delNodes[i]);
            }


            //Act
            var ex1 = Assert.Throws<ArgumentException>(delegate
            {
                graph.RemoveNode(delNodes[count-1]);
            });

            //Assert
            CollectionAssert.IsSubsetOf(ErrorMessages.CantBeDeleted, ex1.Message);

        }

        #endregion



    }


}
