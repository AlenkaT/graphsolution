﻿using System;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;

using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using EVT.Applications.GraphSolution.Graph;

namespace GraphUnitTests
{

    static class TestConstants
    {
        static internal string TestExceptionString = "DirectedGraphTestsException";
    }

    #region helper class
    class DirectedGraphTestHelper : DirectedGraph
    {

       
        EnTestMode _testMode;

        //internal int Loopcounter { get { return base.LoopCounter; } }

        internal IEnumerable<Node> Nodes { get { return base._Nodes; } }
        internal IEnumerable<Edge> Edges { get { return base._Edges; } }

        internal new void DoAddNode(Node node)
        {
            base.DoAddNode(node);
        }

        internal Boolean CanAddNodeTest(Node node, ref Exception ex)
        {
            return base.CanAddNode(node, ref ex);
            
        }

        protected override Boolean CanAddNode(Node node, ref Exception ex)
        {
            switch (_testMode)
            {
                case EnTestMode.normal:
                    return base.CanAddNode(node, ref ex);
                case EnTestMode.invalid:
                    ex = new Exception(TestConstants.TestExceptionString);
                    return false;
                case EnTestMode.valid:
                    return true;
            }
            throw new NotImplementedException();
        }

        internal new void DoRemoveNode(Node node)
        {
            base.DoRemoveNode(node);
        }

        protected override Boolean CanRemoveNode(Node node, ref Exception ex)
        {
            switch (_testMode)
            {
                case EnTestMode.normal:
                    return base.CanRemoveNode(node, ref ex);
                case EnTestMode.invalid:
                    ex = new Exception(TestConstants.TestExceptionString);
                    return false;
                case EnTestMode.valid:
                    return true;
            }
            throw new NotImplementedException();
        }

        protected override Boolean CanAddEdge(Node from, Node to, ref Exception ex)
        {
            switch (_testMode)
            {
                case EnTestMode.normal:
                    return base.CanAddEdge(from, to, ref ex);
                case EnTestMode.invalid:
                    ex = new Exception(TestConstants.TestExceptionString);
                    return false;
                case EnTestMode.valid:
                    return true;
            }
            throw new NotImplementedException();

        }

        protected override bool CanRemoveEdge(Edge edge, out WhichNodeWillBeRemove node, ref Exception ex)
        
        {
            switch (_testMode)
            {
                case EnTestMode.normal:
                    return base.CanRemoveEdge(edge, out node, ref ex);
                case EnTestMode.invalid:
                    ex = new Exception(TestConstants.TestExceptionString);
                    node = WhichNodeWillBeRemove.NoOne;
                    return false;
                case EnTestMode.valid:
                    node = WhichNodeWillBeRemove.NoOne;
                    return true;
            }
            throw new NotImplementedException();

        }

        internal Boolean CanRemoveEdgeTest(Edge edge, Object anything, ref Exception ex)
        {
            WhichNodeWillBeRemove node;
            return base.CanRemoveEdge(edge, out node, ref ex);
        }

        internal Boolean CanAddEdgeTest(Node from, Node to, ref Exception ex)
        {
            return base.CanAddEdge(from, to, ref ex);
        }

        internal new Edge FindEdge(Node from, Node to)
        {
            return base.FindEdge(from, to);
        }

        //для тестирования перегрузки
        //internal new ReadOnlyCollection<Node> GetConnectedNodes(IEnumerable<Node> fromNodes, bool isDirect, IEnumerable<Node> exceptNodes = null)
        //{
        //    return base.GetConnectedNodes(fromNodes, isDirect, exceptNodes);
        //}

        internal new ReadOnlyCollection<Node> GetConnectedNodes(Node node, bool isDirected, IEnumerable<Node> exceptNodes = null)
        {
            return base.GetConnectedNodes(node, isDirected, exceptNodes);
        }


        #region C-tors
        internal DirectedGraphTestHelper(IEnumerable<Node> nodes = null, IEnumerable<Edge> edges = null, EnTestMode testMode = EnTestMode.normal)
        {
            if (nodes != null)
            {
                _Nodes.AddRange(nodes);
            }
            if (edges != null)
            {
                _Edges.AddRange(edges);
            }
            _testMode = testMode;
        }

        #endregion
    }
    #endregion


    [TestFixture]
    public class DirectedGraphTests
    {

        #region Internal Methods

        [Test]
        public void AddNode_WhenNodeIsInvalid_ExceptionExpacted()
        {
            //Arrange
            var graph = new DirectedGraphTestHelper(testMode: EnTestMode.invalid);
           
            //Act
            var ex1 = Assert.Throws<Exception>(delegate
            {
                graph.AddNode(null);
            });

            //Assert
            Assert.AreEqual(TestConstants.TestExceptionString, ex1.Message);
        }

        [Test]
        public void AddNode_WhenNodeIsValid_DoesNotThrowExceptionExpacted()
        {

            var graph = new DirectedGraphTestHelper(testMode: EnTestMode.valid);

            Assert.DoesNotThrow(
                delegate
                {
                    graph.AddNode(new Node());
                });

        }

        
        [Test]
        public void RemoveNode_WhenNodeIsInvalid_ExceptionExpacted()
        {
            Node n1 = new Node();
            var graph = new DirectedGraphTestHelper(new Node[] { n1 }, testMode: EnTestMode.invalid);

            //Act
            var ex1 = Assert.Throws<Exception>(delegate
            {
                graph.RemoveNode(null);
            });

            //Assert
            Assert.AreEqual(TestConstants.TestExceptionString, ex1.Message);
            
        }

        [Test]
        public void RemoveNode_WhenNodeIsValid_DoesNotThrowExceptionExpacted()
        {
            Node n1 = new Node();
            var graph = new DirectedGraphTestHelper(new Node[] { n1 }, testMode: EnTestMode.valid);
            var expacted = graph.Nodes.Count();
            Node n2 = new Node();

            Assert.DoesNotThrow(
                delegate
                {
                    graph.RemoveNode(null);
                });
        }

        //[Test]
        //public void RemoveNode_WhenGraphContainsNode_RemovingNodeAndConnectedEdgesExpacted()
        //{
        //    var nodes = new List<Node> { new Node() };
        //    var edges = new List<Edge>();


        //    var count = RND.GetPositiveRNDByte(Byte.MaxValue);
        //    for (var i = 0; i < count; i++)
        //    {
        //        nodes.Add(new Node());
        //        edges.Add(new Edge(nodes[0], nodes[i]));
        //    }

        //    var graph = new DirectedGraphTestHelper(nodes, edges);
        //    var expacted = graph.Nodes.Count() - 1;
        //    graph.RemoveNode(nodes[0]);

        //    //В тесте по хорошему делается одна проверка
        //    Assert.AreEqual(expacted, graph.Nodes.Count());

        //    CollectionAssert.IsEmpty(graph.Edges);
        //}


        [Test]
        public void AddEdge_WhenAnyNodeIsInvalid_ExceptionExpacted()
        {

            var graph = new DirectedGraphTestHelper(testMode: EnTestMode.invalid);

            var ex1 = Assert.Throws<Exception>(delegate
            {
                graph.AddEdge(null, new Node());
            });

            Assert.AreEqual(TestConstants.TestExceptionString, ex1.Message);

            //Пусть будет туть
            ex1 = Assert.Throws<Exception>(delegate
            {
                graph.AddEdge(new Node(), null);
            });

            Assert.AreEqual(TestConstants.TestExceptionString, ex1.Message);
        }

        [Test]
        public void AddEdge_WhenNodesAreValid_DoesNotThrowExceptionExpacted()
        {

            var graph = new DirectedGraphTestHelper(testMode: EnTestMode.valid);

            Assert.DoesNotThrow(
                delegate
                {
                    graph.AddEdge(new Node(), new Node());
                });

        }


        [Test]
        public void RemoveEdge_WhenEdgeIsNotFound_EdgesRemainsUnchanged()
        {
            //Arrange
            var n1 = new Node();
            var n2 = new Node();
            var edge = new Edge(n1, n2);
            var graph = new DirectedGraphTestHelper(edges: new Edge[] { edge });
            var expacted = graph.Edges.Count();

            //Act
            graph.RemoveEdge(new Node(), new Node());

            //Assert
            Assert.AreEqual(expacted, graph.Edges.Count());
        }

        [Test]
        public void RemoveEdge_WhenAnyNodeIsNull_EdgesRemainsUnchanged()
        {
            //Arrange
            var n1 = new Node();
            var n2 = new Node();
            var edge = new Edge(n1, n2);
            var graph = new DirectedGraphTestHelper(edges: new Edge[] { edge });
            var expacted = graph.Edges.Count();

            //Act
            graph.RemoveEdge(null, new Node());

            //Assertion
            Assert.AreEqual(expacted, graph.Edges.Count());


            //Act
            graph.RemoveEdge(new Node(), null);

            //Assertion
            Assert.AreEqual(expacted, graph.Edges.Count());
        }

        [Test]
        public void RemoveEdge_WhenEdgeIsFoundAndEdgeIsInvalid_ExceptionExpacted()
        {
            //Arrange
            var n1 = new Node();
            var n2 = new Node();
            var edge = new Edge(n1, n2);
            var graph = new DirectedGraphTestHelper(edges: new Edge[] { edge }, testMode: EnTestMode.invalid);
            //var expacted = graph.Edges.Count() - 1;

            ////Act
            //graph.RemoveEdge(n1, n2);

            ////Assertion
            //Assert.AreEqual(expacted, graph.Edges.Count());

            //Act
            var ex1 = Assert.Throws<Exception>(delegate
            {
                graph.RemoveEdge(n1, n2);
            });

            //Assert
            Assert.AreEqual(TestConstants.TestExceptionString, ex1.Message);


        }

        [Test]
        public void RemoveEdge_WhenEdgeIsFoundAndEdgeIsValid_DoesNotThrowExceptionExpacted()
        {
            //Arrange
            var n1 = new Node();
            var n2 = new Node();
            var edge = new Edge(n1, n2);
            var graph = new DirectedGraphTestHelper(edges: new Edge[] { edge }, testMode: EnTestMode.valid);
            
            //Act & Assert
            Assert.DoesNotThrow(
             delegate
             {
                 graph.RemoveEdge(n1, n2);
             });


        }


        #endregion

        #region Protected Virtual

        [Test]
        public void CanAddNode_WhenNodeIsNull_ArgumentNullExceptionExpacted()
        {
            //Arrange
            var graph = new DirectedGraphTestHelper();
            Exception ex = null;

            //Act
            var result = graph.CanAddNodeTest(null, ref ex);

            //Assert
            Assert.IsTrue(!result && ex is ArgumentNullException);

        }

        [Test]
        public void CanAddNode_WhenNodeIsNotNull_TrueExpacted()
        {
            var graph = new DirectedGraphTestHelper();
            Exception ex = null;

            var result = graph.CanAddNodeTest(new Node(), ref ex);

            Assert.IsTrue(result && ex == null);

        }

        [Test]
        public void CanRemoveEdge()
        {
            var graph = new DirectedGraphTestHelper();
            Exception ex = null;

            var result = graph.CanRemoveEdgeTest(null, null, ref ex);

            Assert.IsTrue(result);

        }

        [Test]
        public void CanAddEdge_WhenAnyNodeIsNull_ArgumentNullExceptionExpacted()
        {
            //Arrange
            var graph = new DirectedGraphTestHelper();
            Exception ex = null;

            //Act
            var result = graph.CanAddEdgeTest(null, new Node(), ref ex);

            //Assert
            Assert.IsTrue(!result && ex is ArgumentNullException);

            //Act
            result = graph.CanAddEdgeTest(new Node(), null, ref ex);

            //Assert
            Assert.IsTrue(!result && ex is ArgumentNullException);
        }

        [Test]
        public void CanAddEdge_WhenNodesAreNotNull_TrueExpacted()
        {
            var graph = new DirectedGraphTestHelper();
            Exception ex = null;

            var result = graph.CanAddEdgeTest(new Node(), new Node(), ref ex);

            Assert.IsTrue(result && ex == null);

        }

        [Test]
        public void DoAddNode_WhenNodeIsAlreadyContained_NodesRemainsUnchanged()
        {
            //Arrange
            var n1 = new Node();
            var graph = new DirectedGraphTestHelper(new Node[] { n1 });
            var expacted = graph.Nodes.Count();

            //Act
            graph.AddNode(n1);

            //Assert
            Assert.AreEqual(expacted, graph.Nodes.Count());

        }

        [Test]
        public void DoAddNode_WhenNodeIsNotContained_NodeWillBeAddedExpacted()
        {
            //Arrange
            var n1 = new Node();
            var graph = new DirectedGraphTestHelper();
            var expacted = graph.Nodes.Count() + 1;

            //Act
            graph.DoAddNode(n1);


            Assert.AreEqual(expacted, graph.Nodes.Count());
        }


        [Test]
        public void DoAddEdge_WhenEdgeIsNotContainedYet_EdgeWillBeAddedExpacted()
        {
            //Arrange
            var n1 = new Node();
            var n2 = new Node();
            var edge = new Edge(n1, n2);
            var graph = new DirectedGraphTestHelper(edges: new Edge[] { edge });
            var expacted = graph.Edges.Count() + 1;

            //Act
            graph.AddEdge(new Node(), new Node());

            //Assertion
            Assert.AreEqual(expacted, graph.Edges.Count());

        }

        [Test]
        public void DoAddEdge_WhenEdgeIsAlreadyContained_EdgesRemainsUnchanged()
        {
            //Arrange
            var n1 = new Node();
            var n2 = new Node();
            var edge = new Edge(n1, n2);
            var graph = new DirectedGraphTestHelper(edges: new Edge[] { edge }, nodes: new Node[] { n1, n2 });
            var expacted = graph.Edges.Count();

            //Act
            graph.AddEdge(n1, n2);

            //Assertion
            Assert.AreEqual(expacted, graph.Edges.Count());

        }

        [Test]
        public void DoRemoveNode_NodeAndConnectedEdgesWillBeRemovedExpacted()
        {
            //Arrange
            byte level = 1; //!!!! тест  будет проходить только при 1, так как узлы exNodes ИСКЛЮЧАЮТСЯ из обхода, и подключенные к ним узлы в результат не попадают
            var n1 = RND.GetNextByte(2, 10); //количество точек уровня
            var graph = TestGraphHelper.CreateCDAG(level, n1); //солнышко - 1 уровень
            var oldedges = graph.GetEdges();
            var edges = new List<Edge>();
            var nodes = new List<Node>();
            foreach (var edge in oldedges)
            {
                //дополняем солнышко
                nodes.Add(TestNode.CreateTestNode());
                edges.Add(new Edge(nodes.Last(), edge.From));
            }
            var graph1 = new DirectedGraphTestHelper(nodes.Union(graph.GetNodes()), edges.Union(oldedges));
            
            var startNode = graph.GetNodes().ElementAt(0);
            //Act
            //получение смежных точек, исключая на выходе  exNodes

            graph1.DoRemoveNode(startNode);

            //Assert
            CollectionAssert.IsEmpty(graph1.Edges);
            CollectionAssert.DoesNotContain(graph1.Nodes, startNode);
        }

        #endregion

        [Test]
        public void FindEdge_WhenEdgeIsMissing_NullExpacted()
        {
            //Arrange
            var n1 = new Node();
            var n2 = new Node();
            var edge = new Edge(n1, n2);
            var graph = new DirectedGraphTestHelper(edges: new Edge[] { edge });


            //Act
            var result = graph.FindEdge(new Node(), n1);

            //Assertion
            Assert.IsNull(result);

        }

        [Test]
        public void FindEdge_WhenEdgeIsContained_CorrectEdgeExpacted()
        {
            //Arrange
            var n1 = new Node();
            var n2 = new Node();
            var edge = new Edge(n1, n2);
            var graph = new DirectedGraphTestHelper(edges: new Edge[] { edge });


            //Act
            var result = graph.FindEdge(n1, n2);

            //Assertion
            Assert.AreSame(edge, result);
        }




        [Test]
        public void GetConnectedNodes_WhenExceptNodesIsNotNullAndSearchIsNotDirected_ResultDoesNotContainExceptNodesExpacted()
        {
            //Arrange
            byte level = 1; //!!!! тест  будет проходить только при 1, так как узлы exNodes ИСКЛЮЧАЮТСЯ из обхода, и подключенные к ним узлы в результат не попадают
            var n1 = RND.GetNextByte(2, 15); //количество точек уровня
            var graph = TestGraphHelper.CreateCDAG(level, n1); //солнышко - 1 уровень
            var oldedges = graph.GetEdges();
            var edges = new List<Edge>();
            var nodes = new List<Node>();
            foreach (var edge in oldedges)
            {
                //дополняем солнышко
                nodes.Add(TestNode.CreateTestNode());
                edges.Add(new Edge(nodes.Last(), edge.From));
            }
            var graph1 = new DirectedGraphTestHelper(nodes.Union(graph.GetNodes()), edges.Union(oldedges));
            var exNodes = new List<Node>();

            var countExNodes = RND.GetPositiveRNDByte(n1 - 1) * 2;
            for (var i = 0; i < countExNodes; i++)
            {
                exNodes.Add(graph1.Nodes.ElementAt(RND.GetPositiveRNDByte(2 * n1)));
            }

            var startNode = graph.GetNodes().ElementAt(0);
            //Act
            //получение смежных точек, исключая на выходе  exNodes

            var result = graph1.GetConnectedNodes(startNode, false, exNodes);

            var expacted = graph1.Nodes.Except(exNodes.Distinct());

            //Assert
            CollectionAssert.AreEquivalent(expacted, result);

        }


        [Test]
        public void GetConnectedNodes_WhenExceptNodesIsNotNullAndSearchIsDirected_ResultDoesNotContainExceptNodesExpacted()
        {
            //Arrange
            byte level = 1; //!!!! тест  будет проходить только при 1, так как узлы exNodes ИСКЛЮЧАЮТСЯ из обхода, и подключенные к ним узлы в результат не попадают
            var n1 = RND.GetNextByte(2, 15); //количество точек уровня
            var graph = TestGraphHelper.CreateCDAG(level, n1); //солнышко - 1 уровень
            var oldedges = graph.GetEdges();
            var edges = new List<Edge>();
            var nodes = new List<Node>();
            foreach (var edge in oldedges)
            {
                //дополняем солнышко
                nodes.Add(TestNode.CreateTestNode());
                edges.Add(new Edge(nodes.Last(), edge.From));
            }
            var graph1 = new DirectedGraphTestHelper(nodes.Union(graph.GetNodes()), edges.Union(oldedges));
            var exNodes = new List<Node>();

            var countExNodes = RND.GetPositiveRNDByte(n1 - 1) * 2;
            for (var i = 0; i < countExNodes; i++)
            {
                exNodes.Add(graph1.Nodes.ElementAt(RND.GetPositiveRNDByte(2 * n1)));
            }

            var startNode = graph.GetNodes().ElementAt(0);
            //Act
            //получение смежных точек, исключая на выходе  exNodes

            var result = graph1.GetConnectedNodes(startNode, true, exNodes);

            var expacted = graph1.Nodes.Except(exNodes.Union(nodes)).Where(x => x != startNode); // в детях нет стартовой точки в солнышке

            //Assert
            CollectionAssert.AreEquivalent(expacted, result);


            
        }

        [Test]
        public void GetConnectedNodes_WhenExceptNodesIsNullAndSearchIsNotDirected_ResultContainsAllGraphExpacted()
        {
            //Arrange
            byte level = 1; //!!!! тест  будет проходить только при 1, так как узлы exNodes ИСКЛЮЧАЮТСЯ из обхода, и подключенные к ним узлы в результат не попадают
            var n1 = RND.GetNextByte(2, 15); //количество точек уровня
            var graph = TestGraphHelper.CreateCDAG(level, n1); //солнышко - 1 уровень
            var oldedges = graph.GetEdges();
            var edges = new List<Edge>();
            var nodes = new List<Node>();
            foreach (var edge in oldedges)
            {
                //дополняем солнышко
                nodes.Add(TestNode.CreateTestNode());
                edges.Add(new Edge(nodes.Last(), edge.From));
            }
            var graph1 = new DirectedGraphTestHelper(nodes.Union(graph.GetNodes()), edges.Union(oldedges));

            var startNode = graph.GetNodes().ElementAt(0);
            //Act
            //получение смежных точек, исключая на выходе  exNodes

            var result = graph1.GetConnectedNodes(startNode, false, null);

            var expacted = graph1.Nodes;

            //Assert
            CollectionAssert.AreEquivalent(expacted, result);

        }



        [Test]
        public void GetConnectedNodes_WhenExceptNodesIsNullAndSearchIsDirected_ResultDoesNotContainParentsNodesExpacted()
        {
            //Arrange
            byte level = 1; //!!!! тест  будет проходить только при 1, так как узлы exNodes ИСКЛЮЧАЮТСЯ из обхода, и подключенные к ним узлы в результат не попадают
            var n1 = RND.GetNextByte(2, 15); //количество точек уровня
            var graph = TestGraphHelper.CreateCDAG(level, n1); //солнышко - 1 уровень
            var oldedges = graph.GetEdges();
            var edges = new List<Edge>();
            var nodes = new List<Node>();
            foreach (var edge in oldedges)
            {
                //дополняем солнышко
                nodes.Add(TestNode.CreateTestNode());
                edges.Add(new Edge(nodes.Last(), edge.From));
            }
            var graph1 = new DirectedGraphTestHelper(nodes.Union(graph.GetNodes()), edges.Union(oldedges));

            var startNode = graph.GetNodes().ElementAt(0);
            //Act
            //получение смежных точек, исключая на выходе  exNodes

            var result = graph1.GetConnectedNodes(startNode, true, null);

            var expacted = graph1.Nodes.Except(nodes).Where(x=>x!=startNode);

            //Assert
            CollectionAssert.AreEquivalent(expacted, result);

        }



        //[Test]
        //public void GetConnectedNodes_WhenExceptNodesIsNull_ResultContainsAllGraphExpacted()
        //{
        //    //Arrange
        //    byte level = 3;
        //    var graph = TestGraphHelper.CreateCDAG(level, RND.GetNextByte(3, 15)); //солнышко - 1 уровень
        //    var graph1 = new DirectedGraphTestHelper(graph.GetNodes(), graph.GetEdges());

        //    var nodes = graph1.Nodes;
        //    var startNode = graph1.Nodes.Take(1);

        //    //Act
        //    //получение подключенных точек для всего графа точек без исключений
        //    var result = graph1.GetConnectedNodes(startNode, false, null);
        //    //Assert
        //    var expacted = nodes.Except(TestGraphHelper.FindFreeNodes(graph1).Union(startNode));
        //    CollectionAssert.AreEquivalent(expacted, result);

        //}




    }
}
