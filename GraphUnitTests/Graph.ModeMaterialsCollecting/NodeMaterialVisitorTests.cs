﻿using EVT.Graph;
using EVT.Graph.GraphProcessing;
using EVT.Graph.GraphVisitor.Interfaces;
using EVT.Graph.NodeMaterialsCollecting;
using EVT.Graph.NodeVisitor;
using EVT.Graph.NodeWeightCollecting;
using GraphUnitTests;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaterialsCollectingTests
{
    [TestFixture]
    public class NodeMaterialsVisitorTests 
    {
        [Test]
        public void ProcessDepthFirst_SetOfMaterialsExpacted()
        {
            //Arrange

            //сделать тестовый граф DirectedGraph directedGraph
            byte level = 10;
            byte count = RND.GetNextByte(3, 20);
            DirectedGraph graph = TestGraphHelper.CreateCDAG(level, count, 20); //делаем солнышко
            var nodes = graph.GetNodes();

            // список материалов
            var materialsList = new List<Material>();
            for (var i=0; i<count-1;i++)
            {
                materialsList.Add(new Material("Material_" + i));
            }

            var infos = new List<NodeMaterialInfo>();
            foreach (TestNode node in nodes)
            {
                infos.Add(new NodeMaterialInfo(node, materialsList[RND.GetPositiveRNDByte(count)-1]));
            }

            //создать экземпляр класса NodeGraphProcessor nodeGraphProcessor
            var processor = new NodeGraphProcessor();

            //создать экземпляр класса NodeNumberVisitor nodeNumberVisitor
            var visitor = new NodeMaterialVisitor(infos);

            //Act
            //вызвать nodeGraphProcessor.ProcessBreadthFirst(directedGraph, startNode, nodeNumberVisitor, null)
            processor.ProcessDepthFirst(graph, nodes[0], new List<INodeVisitor>() { visitor }, null);

            //Assert
            // проверить nodeNumberVisitor.Numbers
            CollectionAssert.AreEquivalent(materialsList, (IEnumerable<Material>)visitor.Result);
        }
    }
}
