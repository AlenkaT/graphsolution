﻿
using EVT.Applications.GraphSolution.CommonNodeInfos;

using EVT.Applications.GraphSolution.GraphProcessing;
using EVT.Applications.GraphSolution.GraphVisitor.Interfaces;
using EVT.Applications.GraphSolution.GraphVisitor;

using EVT.Applications.GraphSolution.NodeWeightCollecting;
using GraphUnitTests;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EVT.Applications.GraphSolution.Graph;
using EVT.Applications.GraphSolution.NodeMaterialsCollecting;
using EVT.Applications.GraphSolution.GraphInfo.Interfaces;
using EVT.Applications.GraphSolution.NodeInfoTypes.Shared;
using EVT.Applications.GraphSolution.NodeInfoStorageManager.Interfaces;

namespace MaterialsCollectingTests
{
    [TestFixture]
    public class NodeMaterialsVisitorIntergationTests
    {
        class _TestManager : INodeInfoStorageManager<MaterialNodeInfo>
        {
            Dictionary<Node, Material> _dictionary;
            public IEnumerable<Material> Materials => _dictionary.Values;

            public object GetInfo(Node node)
            {
                return _dictionary[node];
            }


            #region
            public IReadOnlyList<MaterialNodeInfo> GetNodeInfos(IEnumerable<Node> nodes)
            {
                throw new NotImplementedException();
            }

            public MaterialNodeInfo GetNodeInfo(Node node)
            {
                throw new NotImplementedException();
            }

           
           

            #endregion

            public _TestManager(IEnumerable<Node> nodes)
            {
                _dictionary = new Dictionary<Node, Material>();
                foreach (var node in nodes)
                {
                    var material = Material.Create("Steel", "gost6713", "15ХСНД", RND.GetPositiveRNDByte());
                    _dictionary[node]=material;
                }
            }
        }

        [Test]
        public void ProcessDepthFirst_SetOfMaterialsExpacted()
        {
            //Arrange

            //сделать тестовый граф DirectedGraph directedGraph
            byte level = 10;
            byte count = RND.GetNextByte(3, 20);
            DirectedGraph graph = TestGraphHelper.CreateCDAG(level, count, 3); //делаем солнышко
            var nodes = graph.GetNodes();

            // список материалов
            var materials = new _TestManager(nodes);

            //создать экземпляр класса NodeGraphProcessor nodeGraphProcessor
            var processor = new NodeGraphProcessor();

            //создать экземпляр класса NodeNumberVisitor nodeNumberVisitor
            var visitor = new NodeMaterialVisitor(materials);

            //Act
            //вызвать nodeGraphProcessor.ProcessBreadthFirst(directedGraph, startNode, nodeNumberVisitor, null)
            processor.ProcessDepthFirst(graph, nodes[0], new List<INodeVisitor>() { visitor }, null);

            //Assert
            // проверить nodeNumberVisitor.Numbers
            CollectionAssert.AreEquivalent(materials.Materials, visitor.Result);
        }
    }
}
