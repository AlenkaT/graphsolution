﻿using EVT.Applications.GraphSolution.Graph;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphUnitTests
{
    static class TestGraphHelper
    {
        
        internal static DirectedGraph CreateCDAG(Byte sunLevel, Byte countLevelNodes)
        {
            //Солнышко
            var n1 = countLevelNodes;
            var nds = new TestNode[sunLevel * n1 + 1];
            nds[0] = TestNode.CreateTestNode();
            var edges = new Edge[sunLevel * n1];
            

            for (var i = 0; i < n1; i++)
            {
                nds[i + 1] = TestNode.CreateTestNode();
                edges[i] = new Edge(nds[0], nds[i + 1]);
                for (var j = 1; j < sunLevel; j++)
                {
                    //
                    nds[n1*j+i+1] = TestNode.CreateTestNode();
                    //
                    edges[n1 * j + i] = new Edge(nds[n1 * (j - 1) + i + 1], nds[n1 * j + i + 1]);
                }
            }

            return new ConnectedDirectedAcyclicGraph(nds, edges);
            
        }

        internal static DirectedGraph CreateCDAG(Byte sunLevel, Byte countFirstLevel, Byte countChildren)
        {
            //Солнышко
            int n1 = countFirstLevel;
            var nds = new List<TestNode>() { TestNode.CreateTestNode() }; //центр
            var ndsL = new List<TestNode>(nds);
            var edges = new List<Edge>();
            TestNode newNode;

            for (var level = 0; level < sunLevel; level++)
            {
                foreach (var node in ndsL)
                {
                    for (var i = 0; i < n1; i++)
                    {
                        newNode = TestNode.CreateTestNode();
                        nds.Add(newNode);
                        edges.Add(new Edge(node, newNode));
                    }
                    n1 = countChildren;
                }
            }
            
            return new ConnectedDirectedAcyclicGraph(nds, edges);

        }

        //internal static DirectedGraph CreateCDAG2(Byte sunLevel)
        //{
        //    //Солнышко2->цветочек
        //    var n1 = RND.Next(3, 15); // количество точек уровня
        //    var nds = new LinkedList<TestNode>();
        //    nds.AddFirst(TestNode.CreateTestNode());
        //    var edges = new LinkedList<Edge>();



        //    for (var i = 0; i < n1; i++)
        //    {
        //        //уровень 1
        //        nds.AddLast(TestNode.CreateTestNode());
        //        edges.AddLast(new Edge(nds.Last(), nds.First()));
        //        for (var j = 1; j < sunLevel; j++)
        //        {
        //            //
        //            nds.AddLast(TestNode.CreateTestNode());
        //            //
        //            edges.
        //            edges[n1 * j*2 + i] = new Edge(nds[n1 * (j - 1) + i + 1], nds[n1 * j + i + 2]);
        //            edges[n1 * j*3 + i] = new Edge(nds[n1 * (j - 1) + i + 1], nds[n1 * j + i + 0]);
        //        }
        //    }

        //    return new ConnectedDirectedAcyclicGraph(nds, edges);

        //}
        internal static DirectedAcyclicGraphTestHelper CreateRandomDAG(Boolean connected )
        {

            var n1 = RND.GetNextByte(5, 7);
            var n2 = RND.GetNextByte(5, 7);
            var nds = new List<TestNode>() { TestNode.CreateTestNode() };
            
            var edges = new List<Edge>();

            for (byte i = 1; i < n1; i++)
            {
                nds.Add(TestNode.CreateTestNode());
                edges.Add(new Edge(nds[RND.GetNextByte(1, i) - 1], nds[i]));
            }

            nds.Add(TestNode.CreateTestNode());

            if (connected)
            {
                edges.Add(new Edge(nds[n1 - 1], nds[n1]));
            }
            //else
            //{
            //    edges[n1 - 1] = new Edge(nds[0], nds[n1 - 1]);
            //}
            for (var i = n1 + 1; i < n1 + n2; i++)
            {
                nds.Add(TestNode.CreateTestNode());
                edges.Add(new Edge(nds[RND.GetNextByte(n1 + 1, i) - 1], nds[i]));
            }
            return new DirectedAcyclicGraphTestHelper(nodes: nds, edges: edges);

        }


        static internal DirectedGraphTestHelper CreateRandomDG()
        {

            var n1 = RND.GetPositiveRNDByte(15);
            var nds = new TestNode[n1];
            var n2 = RND.GetPositiveRNDByte();
            var edges = new Edge[n2];

            for (var i = 0; i < n1; i++)
            {
                nds[i] = TestNode.CreateTestNode();
            }

            for (var i = 0; i < n2; i++)
            {
                edges[i] = new Edge(nds[RND.GetPositiveRNDByte(n1)], nds[RND.GetPositiveRNDByte(n1)]);
            }
            return new DirectedGraphTestHelper(nodes: nds, edges: edges);
        }



        static internal IEnumerable<Node> FindFreeNodes(DirectedGraph graph)
        {
            var dyngraph = (dynamic)graph;
            IEnumerable<Node> nodes = dyngraph.Nodes;
            IEnumerable<Edge> edges = dyngraph.Edges;
            return nodes.Except(edges.Select(ed => ed.From)).Except(edges.Select(ed => ed.To));


        }
    }
}
