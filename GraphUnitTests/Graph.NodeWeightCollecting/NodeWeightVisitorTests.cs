﻿using EVT.Graph;
using EVT.Graph.GraphProcessing;
using EVT.Graph.GraphVisitor.Interfaces;
using EVT.Graph.NodeVisitor;
using EVT.Graph.NodeWeightCollecting;
using GraphUnitTests;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeightCollectingTests
{
    [TestFixture]
    public class NodeWeightVisitorTests 
    {
        [Test]
        public void ProcessDepthFirst_SumOfWeightsExpacted()
        {
            //Arrange

            //сделать тестовый граф DirectedGraph directedGraph
            byte level = 10;
            byte count = RND.GetNextByte(3, 150);
            DirectedGraph graph = TestGraphHelper.CreateCDAG(level, count, 5); //делаем солнышко
            var nodes = graph.GetNodes();

            var infos = new List<NodeWeightInfo>();
            foreach (TestNode node in nodes)
            {
                infos.Add(new NodeWeightInfo(node, node.ID));
            }

            //создать экземпляр класса NodeGraphProcessor nodeGraphProcessor
            var processor = new NodeGraphProcessor();

            //создать экземпляр класса NodeNumberVisitor nodeNumberVisitor
            var visitor = new NodeWeightVisitor(infos);

            //Act
            //вызвать nodeGraphProcessor.ProcessBreadthFirst(directedGraph, startNode, nodeNumberVisitor, null)
            processor.ProcessDepthFirst(graph, nodes[0], new List<INodeVisitor>() { visitor }, null);

            //Assert
            // проверить nodeNumberVisitor.Numbers
            Assert.AreEqual(infos.Select(x=>x.Info).Sum(), visitor.Result);
        }
    }
}
