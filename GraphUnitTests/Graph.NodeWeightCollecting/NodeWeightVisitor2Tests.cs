﻿using EVT.Applications.GraphSolution.GraphProcessing;
using EVT.Applications.GraphSolution.GraphVisitor.Interfaces;
using EVT.Applications.GraphSolution.GraphVisitor;
using EVT.Applications.GraphSolution.NodeWeightCollecting;
using GraphUnitTests;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EVT.Applications.GraphSolution.Graph;

using EVT.Applications.GraphSolution.GraphInfo.Interfaces;
using EVT.Applications.GraphSolution.NodeInfoStorageManager.Interfaces;
using EVT.Applications.GraphSolution.NodeInfoTypes.Shared;

namespace WeightCollectingTests
{
    [TestFixture]
    public class NodeWeightVisitorIntegrationTests
    {
        class _TestManager : INodeInfoStorageManager<WeightNodeInfo>
        {
            Dictionary<Node, Double> _dictionary;
            public IEnumerable<Double> Values => _dictionary.Values;

            public object GetInfo(Node node)
            {
                return _dictionary[node];
            }

            #region
            public IReadOnlyList<WeightNodeInfo> GetNodeInfos(IEnumerable<Node> nodes)
            {
                throw new NotImplementedException();
            }

            public WeightNodeInfo GetNodeInfo(Node node)
            {
                throw new NotImplementedException();
            }

           
#endregion

            public _TestManager(IEnumerable<Node> nodes)
            {
                _dictionary = new Dictionary<Node, Double>();
                foreach (var node in nodes)
                {
                    var length = RND.GetPositiveRNDByte();
                    var weight = 0.222 * length;
                    
                    _dictionary[node] = weight;
                }
            }
        }

        [Test]
        public void ProcessDepthFirst_SumOfWeightsExpacted()
        {
            //Arrange

            //сделать тестовый граф DirectedGraph directedGraph
            byte level = 10;
            byte count = RND.GetNextByte(3, 20);
            DirectedGraph graph = TestGraphHelper.CreateCDAG(level, count, 2); //делаем солнышко
            var nodes = graph.GetNodes();

       
            var weights= new _TestManager(nodes);

            //создать экземпляр класса NodeGraphProcessor nodeGraphProcessor
            var processor = new NodeGraphProcessor();

            //создать экземпляр класса NodeNumberVisitor nodeNumberVisitor
            var visitor = new NodeWeightVisitor(weights);

            //Act
            //вызвать nodeGraphProcessor.ProcessBreadthFirst(directedGraph, startNode, nodeNumberVisitor, null)
            processor.ProcessDepthFirst(graph, nodes[0], new List<INodeVisitor>() { visitor }, null);

            //Assert
            // проверить nodeNumberVisitor.Numbers
            Assert.AreEqual(weights.Values.Sum(), visitor.Result, Math.Pow(0.1,4));
        }
    }
}
