﻿using EVT.Graph;
using EVT.Graph.GraphProcessing;
using EVT.Graph.GraphVisitor.Interfaces;
using EVT.Graph.NodeNumberVisitor;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphUnitTests
{
    [TestFixture]
    class NodeGraphProcessorIntegrationTests
    {
        [Test]
        public void ProcessBreadthFirst_WhenInfoIsNodeID_ListIDNodesExpaxted()
        {
            //Arrange

            //сделать тестовый граф DirectedGraph directedGraph
            byte level = 15;
            DirectedGraph graph = TestGraphHelper.CreateCDAG(level, RND.GetNextByte(3,10)); //делаем солнышко
            var nodes = graph.GetNodes();

            var infos = new List<NodeNumberInfo>();
            foreach (TestNode node in nodes)
            {
                infos.Add(new NodeNumberInfo(node, node.ID));
            }


            //создать экземпляр класса NodeGraphProcessor nodeGraphProcessor
            var processor = new NodeGraphProcessor();

            //создать экземпляр класса NodeNumberVisitor nodeNumberVisitor
            var visitor = new NodeNumberVisitor(infos);

            //Act
            //вызвать nodeGraphProcessor.ProcessBreadthFirst(directedGraph, startNode, nodeNumberVisitor, null)
            processor.ProcessBreadthFirst(graph, nodes[0], new List<INodeVisitor>() { visitor }, null);

            //Assert
            // проверить nodeNumberVisitor.Numbers
            CollectionAssert.AreEquivalent(infos.Select(info => info.Info), visitor.GetInfos());
        }

        [Test]
        public void ProcessDepthFirst_WhenInfoIsNodeID_ListIDNodesExpaxted()
        {
            //Arrange

            //сделать тестовый граф DirectedGraph directedGraph
            byte level = 15;
            DirectedGraph graph = TestGraphHelper.CreateCDAG(level, RND.GetNextByte(3,10)); //делаем солнышко
            var nodes = graph.GetNodes();

            var infos = new List<NodeNumberInfo>();
            foreach (TestNode node in nodes)
            {
                infos.Add(new NodeNumberInfo(node, node.ID));
            }


            //создать экземпляр класса NodeGraphProcessor nodeGraphProcessor
            var processor = new NodeGraphProcessor();

            //создать экземпляр класса NodeNumberVisitor nodeNumberVisitor
            var visitor = new NodeNumberVisitor(infos);

            // 4) выбрать какую-либо вершину графа как начальную Node startNode
            var startNode = nodes[0];

            //Act
            //5) вызвать nodeGraphProcessor.ProcessDepthFirst(directedGraph, startNode, nodeNumberVisitor, null)
            processor.ProcessDepthFirst(graph, startNode, new List<INodeVisitor>() { visitor }, null);

            //Assert
            // проверить nodeNumberVisitor.Numbers
            CollectionAssert.AreEquivalent(infos.Select(info => info.Info), visitor.GetInfos());
        }
    }
}
