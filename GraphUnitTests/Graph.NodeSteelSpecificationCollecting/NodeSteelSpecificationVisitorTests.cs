﻿
using EVT.Applications.GraphSolution.CommonNodeInfos;

using EVT.Applications.GraphSolution.GraphProcessing;
using EVT.Applications.GraphSolution.GraphVisitor.Interfaces;
using EVT.Applications.GraphSolution.SteelSpecificationCollecting;
using EVT.Applications.GraphSolution.NodeVisitor;
using EVT.Applications.GraphSolution.NodeWeightCollecting;
using GraphUnitTests;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EVT.Applications.GraphSolution.Graph;
using EVT.Applications.GraphSolution.GraphInfo.Interfaces;
using EVT.Applications.GraphSolution.NodeInfoStorageManager.Interfaces;
using EVT.Applications.GraphSolution.NodeInfoTypes.Shared;
using EVT.Applications.GraphSolution.NodeInfoTypes.Geometric;

namespace SteelSpecificationCollectingTests
{
    class _TestManager : INodeInfoStorageManager<MaterialNodeInfo>,
            INodeInfoStorageManager<SectionNodeInfo>, 
            INodeInfoStorageManager<SteelProductTypeNodeInfo>,
            INodeInfoStorageManager<LengthNodeInfo>,
            INodeInfoStorageManager<WidthNodeInfo>,
            INodeInfoStorageManager<AreaNodeInfo>
    {
        Dictionary<Node, Material> _dictionaryMaterials = new Dictionary<Node, Material>();
        public IEnumerable<Material> Materials => _dictionaryMaterials.Values;

        Dictionary<Node, Section> _dictionarySections = new Dictionary<Node, Section>();
        public IEnumerable<Section> Sections => _dictionarySections.Values;

        Dictionary<Node, EnSteelProductType> _dictionaryEnSteelProductTypes = new Dictionary<Node, EnSteelProductType>();
        public IEnumerable<EnSteelProductType> EnSteelProductTypes => _dictionaryEnSteelProductTypes.Values;

        Dictionary<Node, Double> _dictionarLengths = new Dictionary<Node, double>();
        public IEnumerable<Double> Lengths => _dictionarLengths.Values;

        Dictionary<Node, Double> _dictionaryWidths = new Dictionary<Node, double>();
        public IEnumerable<Double> Widths => _dictionaryWidths.Values;

        Dictionary<Node, Double> _dictionaryAreas = new Dictionary<Node, double>();
        public IEnumerable<Double> Areas => _dictionaryAreas.Values;

        public Double[] TotalLength { get; private set; }
        Object INodeInfoStorageManager<MaterialNodeInfo>.GetInfo(Node node)
        {
            return _dictionaryMaterials[node];
        }

        Object INodeInfoStorageManager<SectionNodeInfo>.GetInfo(Node node)
        {
            return _dictionarySections[node];
        }

        Object INodeInfoStorageManager<SteelProductTypeNodeInfo>.GetInfo(Node node)
        {
            return _dictionaryEnSteelProductTypes[node];
        }

        Object INodeInfoStorageManager<LengthNodeInfo>.GetInfo(Node node)
        {

            return _dictionarLengths[node];
        }

        object INodeInfoStorageManager<WidthNodeInfo>.GetInfo(Node node)
        {
            return null;
        }

        object INodeInfoStorageManager<AreaNodeInfo>.GetInfo(Node node)
        {
            return null;
        }

        #region NotImplementinterface
        public IReadOnlyList<MaterialNodeInfo> GetNodeInfos(IEnumerable<Node> nodes)
        {
            throw new NotImplementedException();
        }

        public MaterialNodeInfo GetNodeInfo(Node node)
        {
            throw new NotImplementedException();
        }

        IReadOnlyList<SectionNodeInfo> INodeInfoStorageManager<SectionNodeInfo>.GetNodeInfos(IEnumerable<Node> nodes)
        {
            throw new NotImplementedException();
        }

        SectionNodeInfo INodeInfoStorageManager<SectionNodeInfo>.GetNodeInfo(Node node)
        {
            throw new NotImplementedException();
        }

        IReadOnlyList<SteelProductTypeNodeInfo> INodeInfoStorageManager<SteelProductTypeNodeInfo>.GetNodeInfos(IEnumerable<Node> nodes)
        {
            throw new NotImplementedException();
        }

        SteelProductTypeNodeInfo INodeInfoStorageManager<SteelProductTypeNodeInfo>.GetNodeInfo(Node node)
        {
            throw new NotImplementedException();
        }

        IReadOnlyList<LengthNodeInfo> INodeInfoStorageManager<LengthNodeInfo>.GetNodeInfos(IEnumerable<Node> nodes)
        {
            throw new NotImplementedException();
        }

        LengthNodeInfo INodeInfoStorageManager<LengthNodeInfo>.GetNodeInfo(Node node)
        {
            throw new NotImplementedException();
        }

        IReadOnlyList<WidthNodeInfo> INodeInfoStorageManager<WidthNodeInfo>.GetNodeInfos(IEnumerable<Node> nodes)
        {
            throw new NotImplementedException();
        }

        WidthNodeInfo INodeInfoStorageManager<WidthNodeInfo>.GetNodeInfo(Node node)
        {
            throw new NotImplementedException();
        }


        IReadOnlyList<AreaNodeInfo> INodeInfoStorageManager<AreaNodeInfo>.GetNodeInfos(IEnumerable<Node> nodes)
        {
            throw new NotImplementedException();
        }

        AreaNodeInfo INodeInfoStorageManager<AreaNodeInfo>.GetNodeInfo(Node node)
        {
            throw new NotImplementedException();
        }
        #endregion

        public _TestManager(IEnumerable<Node> nodes)
        {
            TotalLength = new Double[10]; 
            Int32 position = -1;
            String sectionSize;
            String sectionType;
            String gostName;
            String gostMaterial;
            String grade;
            

            foreach (TestNode node in nodes)
            {
                position++;
                var length = RND.GetPositiveRNDByte();
                switch (position)
                {
                    case 0:
                        {
                            sectionType = "Rebars";
                            gostName = "gost34028";
                            gostMaterial = "gost34028";
                            grade = "А240";
                            sectionSize = "d6";
                            TotalLength[0] += length;
                            break;
                        }

                    case 1:
                        {
                            sectionType = "Rebars";
                            gostName = "gost34028";
                            gostMaterial = "gost34028";
                            grade = "А240";
                            sectionSize = "d8";
                            TotalLength[1] += length;
                            break;
                        }
                    case 2:
                        {
                            sectionType = "Rebars";
                            gostName = "gost34028";
                            gostMaterial = "gost34028";
                            grade = "А400";
                            sectionSize = "d10";
                            TotalLength[2] += length;
                            break;
                        }
                    case 3:
                        {
                            sectionType = "Rebars";
                            gostName = "gost34028";
                            gostMaterial = "gost34028";
                            grade = "А400";
                            sectionSize = "d8";
                            TotalLength[3] += length;
                            break;
                        }
                    case 4:
                        {
                            sectionType = "Rebars";
                            gostName = "gost34028";
                            gostMaterial = "gost34028";
                            grade = "А500";
                            sectionSize = "d6";
                            TotalLength[4] += length;
                            break;
                        }
                    case 5:
                        {
                            sectionType = "EqualLegAngles";
                            gostName = "gost8509";
                            gostMaterial = "gost27772";
                            grade = "С245";
                            sectionSize = "L2_t3";
                            TotalLength[5] += length;
                            break;
                        }
                    case 6:
                        {
                            sectionType = "EqualLegAngles";
                            gostName = "gost8509";
                            gostMaterial = "gost27772";
                            grade = "С345";
                            sectionSize = "L2_t4";
                            TotalLength[6] += length;
                            break;
                        }
                    case 7:
                        {
                            sectionType = "Pipes";
                            gostName = "gost8732";
                            gostMaterial = "gost27772";
                            grade = "С345";
                            sectionSize = "d20_t2.5";
                            TotalLength[7] += length;
                            break;
                        }
                    case 8:
                        {
                            sectionType = "Pipes";
                            gostName = "gost8732";
                            gostMaterial = "gost6713";
                            grade = "16Д";
                            sectionSize = "d20_t2.5";
                            TotalLength[8] += length;
                            break;
                        }
                    default:
                        {
                            sectionType = "Pipes";
                            gostName = "gost8732";
                            gostMaterial = "gost6713";
                            grade = "15ХСНД";
                            sectionSize = "d22_t2.5";
                            TotalLength[9] += length;
                            position = -1;
                            break;
                        }

                }


                _dictionarySections[node]= Section.Create(sectionType, gostName, sectionSize) ;
                _dictionaryMaterials[node] = Material.Create("Steel", gostMaterial, grade, 0);
                _dictionaryEnSteelProductTypes[node]= EnSteelProductType.Rebar;
                _dictionarLengths[node] = length;

            }
        }
    }

    [TestFixture]
    public class SteelSpecificationVisitorIntegrationTests
    {
        [Test]
        public void ProcessDepthFirst_SpecificationVisitorResultCollectionExpacted()
        {
            //Arrange

            //сделать тестовый граф DirectedGraph directedGraph
            byte level = 10;
            byte count = RND.GetNextByte(3, 20);
            DirectedGraph graph = TestGraphHelper.CreateCDAG(level, count, 2); //делаем солнышко
            var nodes = graph.GetNodes();

            //Создаем хранилище данных

            var manager = new _TestManager(nodes);
         

            //создать экземпляр класса NodeGraphProcessor nodeGraphProcessor
            var processor = new NodeGraphProcessor();

            //создать экземпляр Visitor
            var visitor = new SteelSpecificationVisitor(manager, manager, manager, manager, manager, manager);

            //Act
            //вызвать nodeGraphProcessor.ProcessBreadthFirst(directedGraph, startNode, nodeNumberVisitor, null)
            processor.ProcessDepthFirst(graph, nodes[0], new List<INodeVisitor>() { visitor }, null);

            //Assert
            // проверить nodeNumberVisitor.Numbers
            var groups1 = visitor.Result.GroupBy(x => x.SectionSize);
            var massaD6 = groups1.SingleOrDefault(g => g.Key == "d6").Select(x => x.Massa).Sum();
            var massaD8 = groups1.SingleOrDefault(g => g.Key == "d8").Select(x => x.Massa).Sum();
            var massaD20t2_5= groups1.SingleOrDefault(g => g.Key == "d20_t2.5").Select(x => x.Massa).Sum();
            var massaL2_t3 = groups1.SingleOrDefault(g => g.Key == "L2_t3").Select(x => x.Massa).Sum();

            Assert.AreEqual(0.222 * (manager.TotalLength[0] + manager.TotalLength[4]), massaD6, 0.0001);
            Assert.AreEqual(0.395 * (manager.TotalLength[1] + manager.TotalLength[3]), massaD8, 0.0001);
            Assert.AreEqual(1.08 * (manager.TotalLength[7] + manager.TotalLength[8]), massaD20t2_5, 0.0001);
            Assert.AreEqual(0.89 * manager.TotalLength[5] , massaL2_t3, 0.0001);


            var groups2 = visitor.Result.GroupBy(x => x.SteelGrade);
            var massaA240  = groups2.SingleOrDefault(g => g.Key == "А240").Select(x => x.Massa).Sum();
            var massaA400 = groups2.SingleOrDefault(g => g.Key == "А400").Select(x => x.Massa).Sum();
            var massaA500 = groups2.SingleOrDefault(g => g.Key == "А500").Select(x => x.Massa).Sum();
            var massaC245= groups2.SingleOrDefault(g => g.Key == "С245").Select(x => x.Massa).Sum();
            var massaC345 = groups2.SingleOrDefault(g => g.Key == "С345").Select(x => x.Massa).Sum();
           
            Assert.AreEqual(0.222*manager.TotalLength[0] + 0.395*manager.TotalLength[1], massaA240, 0.0001);
            Assert.AreEqual(0.617 * manager.TotalLength[2] + 0.395 * manager.TotalLength[3], massaA400, 0.0001);
            Assert.AreEqual(0.222 * manager.TotalLength[4], massaA500, 0.0001);
            Assert.AreEqual(0.89 * manager.TotalLength[5], massaC245, 0.0001);
            Assert.AreEqual(1.15 * manager.TotalLength[6] + 1.08 * manager.TotalLength[7], massaC345, 0.0001);

            var groups3 = visitor.Result.GroupBy(x => x.SteelGostName);
            var massa6713 = groups3.SingleOrDefault(g => g.Key == "gost6713").Select(x => x.Massa).Sum();
            var massa34028 = groups3.SingleOrDefault(g => g.Key == "gost34028").Select(x => x.Massa).Sum();
            var massa27772 = groups3.SingleOrDefault(g => g.Key == "gost27772").Select(x => x.Massa).Sum();

            Assert.AreEqual(1.08 * manager.TotalLength[8] + 1.2 * manager.TotalLength[9] , massa6713, 0.0001);
            Assert.AreEqual(0.222 *( manager.TotalLength[0] + manager.TotalLength[4]) + 0.395 * (manager.TotalLength[1]+ manager.TotalLength[3])+0.617*manager.TotalLength[2], massa34028, 0.0001);
            Assert.AreEqual(0.89 * manager.TotalLength[5] + 1.15*manager.TotalLength[6] + 1.08 * manager.TotalLength[7], massa27772, 0.0001);

            var groups4 = visitor.Result.GroupBy(x => x.SectionType);
            var massaPipes = groups4.SingleOrDefault(g => g.Key == "Труба").Select(x => x.Massa).Sum();
            var massaAngle = groups4.SingleOrDefault(g => g.Key == "Уголок равнополочный").Select(x => x.Massa).Sum();
            var massaRebar= groups4.SingleOrDefault(g => g.Key == "Арматура").Select(x => x.Massa).Sum();

            Assert.AreEqual(1.08 * (manager.TotalLength[7]+manager.TotalLength[8] )+ 1.2 * manager.TotalLength[9], massaPipes, 0.0001);
            Assert.AreEqual(0.89 * manager.TotalLength[5]  + 1.15 * manager.TotalLength[6], massaAngle, 0.0001);
            Assert.AreEqual(massa34028, massaRebar, 0.0001);

            var groups5 = visitor.Result.GroupBy(x => x.ProductType);
            var massaRebarType = groups5.SingleOrDefault(g => g.Key == EnSteelProductType.Rebar).Select(x => x.Massa).Sum();

            Assert.AreEqual(massaRebar+massaAngle+massaPipes, massaRebarType, 0.0001);


        }
    }
}
