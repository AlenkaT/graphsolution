﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Graph.NodeMaterialsCollecting
{
    public class MaterialOld
    {
        public string Caption { get; }
        public Int16 Gamma { get; } // объемный вес кг/м3

        public MaterialOld(String caption)
        {
            Caption = caption;
        }
    }
}
