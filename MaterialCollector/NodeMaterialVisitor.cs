﻿using EVT.Graph.GraphVisitor.Interfaces;
using EVT.Graph.NodeVisitor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EVT.Graph.GraphInfo;
using EVT.Graph.GraphInfoStorage;
using EVT.Graph.CommonNodeInfos;

namespace EVT.Graph.NodeMaterialsCollecting
{
    //2) Добавим посетителя NodeNumberVisitor
    class NodeMaterialVisitor : ResultNodeVisitorBase<IEnumerable<MaterialInfo>, MaterialInfo>
    {
        //2.1) Добавить приватное поле List<NodeWeightInfo> _nodeWeightInfos
        //List<NodeWeightInfo> _nodeWeightInfos;
        //2.2) Добавить в класс поле List<float> TotalWeight
        //List<float> _numbers;

        protected override MaterialInfo DoVisitNode(IEnumerable<INodeInfo> listInfo)
        {
            var info = listInfo.SingleOrDefault(x => x is MaterialInfo);

                if (info!=null)
                {
                return ((MaterialInfo)info);
                }
            
            return null;
        }

        protected override IEnumerable<MaterialInfo> DoGetResult(IEnumerable<MaterialInfo> array)
        {
            return _Infos;
        }

        #region C-tor
        //2.3) Добавить в конструктор параметр nodeWeightInfos: List<NodeWeightInfo>, инициализировать _nodeWeightInfos.Инициализировать TotalWeight нулем
        internal NodeMaterialVisitor(Dictionary<Node, MaterialInfo> nodeMaterialMapping)
        {
            _Storage = nodeMaterialMapping.Select(x => new KeyValuePair<Node, List<INodeInfo>>(x.Key, new List<INodeInfo>() { x.Value })).ToDictionary(x => x.Key);
            
        }

        #endregion


        //2.4) Реализовать метод VisitNode(Node node) как
        //public void VisitNode(Node node)
        //{
        //    //NodeWeightInfo nodeInfo = _nodeWeightInfos.FirstOrDefault(info => info.Node == node);
        //    //if (nodeInfo != null)
        //    //{
        //    //    TotalWeight += nodeInfo.Info;
        //    //}
        //}

    }
}
