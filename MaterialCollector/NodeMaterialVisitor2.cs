﻿
using EVT.Applications.GraphSolution.CommonNodeInfos;
using EVT.Applications.GraphSolution.Graph;
using EVT.Applications.GraphSolution.GraphInfo.Interfaces;
using EVT.Applications.GraphSolution.NodeInfoStorageManager.Interfaces;
using EVT.Applications.GraphSolution.NodeInfoTypes.Shared;
using EVT.Applications.GraphSolution.NodeVisitor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace EVT.Applications.GraphSolution.NodeMaterialsCollecting
{
    //2) Добавим посетителя NodeNumberVisitor
    class NodeMaterialVisitor : ResultNodeVisitorBase<IEnumerable<Material>, Material>
    {
        //2.1) Добавить приватное поле List<NodematerialInfo> _nodematerialInfos
        //List<NodematerialInfo> _nodematerialInfos;
        //2.2) Добавить в класс поле List<float> Totalmaterial
        //List<float> _numbers;

        protected readonly INodeInfoStorageManager<MaterialNodeInfo> _materials;

        #region Protected Override
        protected override Material DoVisitNode(Node node)
        {
            //var info = listInfo.SingleOrDefault(x => x is Material);

            return _materials.GetInfo(node) as Material;
        }

        protected override IEnumerable<Material> DoGetResult(IEnumerable<Material> array)
        {
            return _ProcessResults.Distinct();
            
        }

        #endregion

        #region C-tor
        //2.3) Добавить в конструктор параметр nodematerialInfos: List<NodematerialInfo>, инициализировать _nodematerialInfos.Инициализировать Totalmaterial нулем
        //internal NodeMaterialVisitor(Dictionary<Node, Material> nodeMaterialMapping)
        //{
        //    _Storage = nodeMaterialMapping.Select(x => new KeyValuePair<Node, List<INodeInfo>>(x.Key, new List<INodeInfo>() { x.Value })).ToDictionary(x => x.Key);
            
        //}

        internal NodeMaterialVisitor(INodeInfoStorageManager<MaterialNodeInfo> materials) : base ()
        {
            _materials = materials;
        }

        #endregion


        //2.4) Реализовать метод VisitNode(Node node) как
        //public void VisitNode(Node node)
        //{
        //    //NodematerialInfo nodeInfo = _nodematerialInfos.FirstOrDefault(info => info.Node == node);
        //    //if (nodeInfo != null)
        //    //{
        //    //    Totalmaterial += nodeInfo.Info;
        //    //}
        //}

    }
}
