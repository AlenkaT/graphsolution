﻿using EVT.Graph;
using EVT.Graph.GraphInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//    Добавить проект для сбора общего веса
namespace EVT.Graph.NodeMaterialsCollecting
{
    //1) добавить класс NodeWeightInfo: NodeInfo<float>
    public class NodeMaterialInfo : NodeInfo<MaterialOld>
    {
        public NodeMaterialInfo(Node node, MaterialOld nodeInfo) : base(node, nodeInfo)
        {
        }
    }

}

