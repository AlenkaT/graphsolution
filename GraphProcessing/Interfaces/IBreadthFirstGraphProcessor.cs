﻿
using EVT.Applications.GraphSolution.Graph;
using EVT.Applications.GraphSolution.GraphVisitor.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.GraphProcessing.Interfaces
{
    interface IBreadthFirstGraphProcessor
    {
        void ProcessBreadthFirst(DirectedGraph graph, Node startNode, IEnumerable<INodeVisitor> nodeVisitor, IEnumerable<IEdgeVisitor> edgeVisitor);

    }
}
