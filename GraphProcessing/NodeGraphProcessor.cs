﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EVT.Applications.GraphSolution.Graph;
using EVT.Applications.GraphSolution.GraphVisitor.Interfaces;
using EVT.Applications.GraphSolution.Graph.Messages;
using EVT.Applications.GraphSolution.GraphProcessing.Interfaces;

namespace EVT.Applications.GraphSolution.GraphProcessing
{
    class NodeGraphProcessor : IBreadthFirstGraphProcessor, IDepthFirstGraphProcessor
    {
        #region ProcessBreadth
        public void ProcessBreadthFirst(DirectedGraph graph, Node startNode, IEnumerable<INodeVisitor> nodeVisitors, IEnumerable<IEdgeVisitor> edgeVisitors)
        {
            Exception ex = null;
            if (DoIsParametrsArePossible(graph, startNode, nodeVisitors, edgeVisitors, ref ex))
            {
                DoProcessBreadthFirst(graph, startNode, nodeVisitors, edgeVisitors);
                return;
            }
            throw ex;
        }

        protected virtual Boolean DoIsParametrsArePossible(DirectedGraph graph, Node startNode, IEnumerable<INodeVisitor> nodeVisitors, IEnumerable<IEdgeVisitor> edgeVisitors, ref Exception ex)
        {
            if (TryGetArgNullRefException(graph, nameof(graph), ref ex))
            {
                return false;
            }
            if (TryGetArgNullRefException(startNode, nameof(startNode), ref ex))
            {
                return false;
            }
            if (TryGetArgNullRefException(nodeVisitors, nameof(nodeVisitors), ref ex))
            {
                return false;
            }
            return true;
        }

        protected virtual void DoProcessBreadthFirst(DirectedGraph graph, Node startNode, IEnumerable<INodeVisitor> nodeVisitors, IEnumerable<IEdgeVisitor> edgeVisitors)
        {
            //1) в начале методов ProcessDepthFirst и ProcessBreadthFirst завести две bool переменные: shouldTerminateNodeVisiting, shouldTerminateEdgeVisiting
            Boolean shouldTerminateNodeVisiting = false;
            Boolean shouldTerminateEdgeVisiting = false;

            //Реализовать обход в ширину в методе ProcessBreadthFirst;
            //1) заводим очередь вершин и дуг Queue<Node> nodesQueue
            //3) берем первую вершину графа(startNode), кладем в очередь

            var nodesQueue = new Queue<Node>();
            nodesQueue.Enqueue(startNode);
            //2) заводим список посещенных вершин List<Node> seenNodes - необходимо, чтобы не зайти в одну и ту же вершину дважды
            var seenNodes = new List<Node>();

            //4) идем по каждому элементу очереди, и делаем:
            do
            {
                //4.1) вызываем посетителя для вершины nodeVisitor.VisitNode(currentNode)
                var node = nodesQueue.Dequeue();
                nodeVisitors.ToList().ForEach(visitor => visitor.VisitNode(node)); // проверить предварительно nvr.ShouldTerminateNodeVisiting, заходить в те, у кого это свойство false

                //не понятно, почему зачем -> shouldTerminateNodeVisiting || 
                shouldTerminateNodeVisiting = (nodeVisitors.All(nvr => nvr.ShouldTerminateNodeVisiting));
                if (shouldTerminateNodeVisiting)
                {
                    return;
                }
                //nodeVisitor.VisitNode(node);
                seenNodes.Add(node);
                //4.2) берем всех детей вершины, и кладем тех, которые не были посещены(не содержатся в seenNodes) в очередь nodesQueue
                var children = graph.GetChildren(node).Except(seenNodes);

                foreach (var newnode in children)
                {
                    nodesQueue.Enqueue(newnode);
                }
            }
            //5) продолжаем, пока очередь не закончится 
            while (nodesQueue.Count > 0);
        }
        #endregion

        #region ProcessDepth

        public void ProcessDepthFirst(DirectedGraph graph, Node startNode, IEnumerable<INodeVisitor> nodeVisitors, IEnumerable<IEdgeVisitor> edgeVisitors)
        {
            Exception ex = null;
            if (DoIsParametrsArePossible(graph, startNode, nodeVisitors, edgeVisitors, ref ex))
            {
                DoProcessDepthFirst(graph, startNode, nodeVisitors, edgeVisitors);
                return;
            }
            throw ex;
        }

        protected virtual void DoProcessDepthFirst(DirectedGraph graph, Node startNode, IEnumerable<INodeVisitor> nodeVisitors, IEnumerable<IEdgeVisitor> edgeVisitors)
        {
            //1) заводим список посещенных вершин List<Node> seenNodes -необходимо, чтобы не зайти в одну и ту же вершину дважды
            var seenNodes = new List<Node>();
            //2) берем первую вершину графа (startNode), запускаем для нее метод
            //void ProcessNode(Node node, DirectedGraph graph)
            var nvtrs = nodeVisitors.ToList();
            Boolean shouldTerminateNodeVisiting = false;
            ProcessNode(startNode, ref graph, ref seenNodes, ref nvtrs, ref shouldTerminateNodeVisiting);
            
        }

        void ProcessNode(Node node, ref DirectedGraph graph, ref List<Node> seenNodes, ref List<INodeVisitor> nodeVisitors, ref Boolean shouldTerminateNodeVisiting)
        {
            //3) метод ProcessNode делает следующее:
            //3.1) кладет вершину node в seenNodes
            seenNodes.Add(node);
            //3.2) вызывает nodeVisitor.VisitNode(node)
            nodeVisitors.ForEach(nvr=>nvr.VisitNode(node));
            shouldTerminateNodeVisiting = (nodeVisitors.All(nvr => nvr.ShouldTerminateNodeVisiting)); // дописать where!
            if (shouldTerminateNodeVisiting)
            {
                return;
            }
            var children = graph.GetChildren(node);
            var notvisitedchildren = children.Except(seenNodes);
            //3.3) в цикле для каждого непосещенного ребенка (отсутствующей в seenNodes), вызываем ProcessNode
            foreach (var n in notvisitedchildren)
            {
                ProcessNode(n, ref graph, ref seenNodes, ref nodeVisitors, ref shouldTerminateNodeVisiting);
                if (shouldTerminateNodeVisiting)
                {
                    return;
                }
            }
        }
        #endregion

        #region C-tor
        internal NodeGraphProcessor()
        { }
        #endregion

        #region Exceptions

        protected Boolean TryGetArgNullRefException(Object parametr, String parName, ref Exception ex)
        {
            if (parametr == null)
            {
                ex = new ArgumentNullException(parName, ErrorMessages.NullArg);
                return true;
            }
            return false;
        }
        #endregion
    }



}
