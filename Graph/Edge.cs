﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.Graph
{
    public class Edge
    {
        #region Properties
        public Node From { get; }
        public Node To { get; }
        #endregion

        #region C-tor
        internal Edge (Node from, Node to)
        {
            From = from;
            To = to;
        }
        #endregion
    }
}
