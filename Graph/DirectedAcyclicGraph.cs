﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.Graph
{
    public class DirectedAcyclicGraph : DirectedGraph
    {
        //tested
        //TOTO дополнить тесты *1
        protected override Boolean CanAddEdge(Node from, Node to, ref Exception ex)
        {
            if (!base.CanAddEdge(from, to, ref ex))
            { return false; }

            //данно случае вершина может быть зациклена на себя
            // перенесено в директедграф
            //*1
            if (Object.ReferenceEquals(from, to))
            {
                ex = new ArgumentException(Messages.ErrorMessages.ArgRefEqual);
                return false;
            }

            // Если нет - делаем проверку, что не появляется цикл.
            //Для этого нужно проверить, нет ли пути из вершины to в вершину from.
            //Если цикл появляется - кидаем Exception и не добавляем дугу

            if (DoesDirectedPathExist(to, from)) //если существует направленный путь
            {
                ex = new ArgumentException(Messages.ErrorMessages.PathAlreadyExists);
                return false;
            }
            return true;
        }

        // tested
        protected Boolean DoesDirectedPathExist(Node from, Node to)
        {
            //используется для контроля циклиности

            // изменения по результатам встречи 06.08
            // проверка существования направленного пути (в т.ч. через дугу)
            // по типу обхода в ширину
            var nodeQueue = new Queue<Node>();
            nodeQueue.Enqueue(from);
            var seenNodes = new List<Node>();
            Node currentNode = null;
            do
            {
                currentNode = nodeQueue.Dequeue();
                seenNodes.Add(currentNode);

                var cns = GetConnectedNodes(currentNode, true, seenNodes); // связанные точки через детей
                if (cns.Contains(to))
                {
                    return true;
                }
                foreach (var newnode in cns)
                {
                    nodeQueue.Enqueue(newnode);
                }
            }
            while (nodeQueue.Count > 0);

            return false;
        }

        //по сравнение с методом IsPath в коде поменялась только одна строчка
        //var cns = GetConnectedNodes(currentNode, false); // связанные точки через детей

        //tested
        protected Boolean DoesAlternativePathExist(Edge edge, IEnumerable<Edge> exceptEdges = null)
        {
            // изменения по результатам встречи 06.08
            // проверка существования любого пути, кроме edge (если есть прямой путь через дугу, он не рассматиривается). А также не заходить в точки exceptNodes
            //чтобы пути из edge.From в exceptNodes также не учитывались при первом поиске смежных точек
            // по типу обхода в ширину
            var startNode = edge.From;
            var nodeQueue = new Queue<Node>();
            
            var _exceptEdges = exceptEdges == null ? new List<Edge>() { edge } : new List<Edge>(exceptEdges) { edge };

            //при первом обращении исключаем вершину To - так как ищуются пути помимо прямой дуги From-To
            IEnumerable<Edge> ces = FindEdges(startNode, false, _exceptEdges); // получаем смежные точки начальной вершины, кроме неё самой
            var seenNodes = new List<Node>() { startNode };

            var cns = ces.Select(x => x.From).Union(ces.Select(x => x.To)).Except(seenNodes);

            //если cns.Count() =0 , то в цикл не зайдет и выйдет с false
            //if (cns.Count() == 0)
            //{
            //    //нет смежных точек
            //    return false;
            //}

            while (nodeQueue.Count > 0 || cns.Count() > 0)
            {
#if DEBUG
                IncreaseLoopCounter();
#endif

                foreach (var newnode in cns)
                {
                    nodeQueue.Enqueue(newnode);
                }

                ces = FindEdges(nodeQueue.Peek(), false, exceptEdges);
                cns= ces.Select(x => x.From).Union(ces.Select(x => x.To)).Except(seenNodes);
                if (cns.Contains(edge.To))
                {
                    return true;
                }
                seenNodes.Add(nodeQueue.Dequeue());

            }

            return false;
        }

        /*
        //protected Boolean IsAlternativePathExist(IEnumerable<Node> from, IEnumerable<Node> to, Boolean isDirect)
        //{
        //    var nodes = from.Union(to);
        //    if (!nodes.All(x => _Nodes.Contains(x))) return false; //возможно тут нужно исключение
        //    IEnumerable<Node> cns = from;
        //    var ens = new List<Node>(nodes);

        //    //Существование альтернативного пути
        //    Int32 loopCounter = 0;
        //    cns = GetConnectedNodes(cns, ens, true, isDirect); // если какие либо точки связаны дугами, то эти связи исключаются
        //    ens = ens.Except(to).ToList();
        //    checked
        //    {
        //        while (cns.Count() != 0)  //если список не пустой
        //        {
        //            cns = GetConnectedNodes(cns, ens, true, isDirect);
        //            to = to.Except(cns.Intersect(to)).ToList(); // исключить достигнутые в cns целевые точки
        //            if (to.Count() == 0) // все заданные целевые точки достигнуты
        //            //if (cns.Contains(to))
        //            {
        //                return true;
        //            }
        //            ens.AddRange(cns);
        //            loopCounter++;
        //        };
        //    }
        //    return false;
        //}
        */

        //tested

        protected override ReadOnlyCollection<Node> GetConnectedNodes(Node node, Boolean isDirected, IEnumerable<Node> exceptNodes = null)
        {
            var _except = new List<Node>() { node };
            if (exceptNodes != null)
            {
                _except.AddRange(exceptNodes);
            }
            var result = base.GetConnectedNodes(node, isDirected, _except);
            return result;
        }
    }
}
