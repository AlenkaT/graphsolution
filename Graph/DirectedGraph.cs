﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.Graph
{
    public class DirectedGraph
    {
#if DEBUG
        //для отслеживания циклов при необходимости рекурсии на тестах
        int _loopCounter;
        internal int LoopCounter { get { return _loopCounter; } }
        protected void IncreaseLoopCounter() { _loopCounter++; }
        internal void LoopCounterReset() { _loopCounter = 0; }
#endif

        #region Fields
        protected List<Node> _Nodes;// список вершин
        protected List<Edge> _Edges; // список дуг
        #endregion

        #region Public Methods
        public ReadOnlyCollection<Node> GetChildren(Node node = null)
        {
            if (node == null)
            {

                //как получить корневые элементы - у которых нет родителей
                //взять все узлы from и и выключить из них все узлы to
                if (_Edges.Count == 0) //если нет связей
                { return GetNodes(); }
                var roots = _Edges.Select(edge => edge.From).Except(_Edges.Select(ed1 => ed1.To)).ToList();
                return roots.AsReadOnly();
            }

            return GetConnectedNodes(node, true);

        }

        //tested
        public void AddNode(Node node)
        {
            Exception ex = null;
            if (CanAddNode(node, ref ex))
            {
                DoAddNode(node);
                return;
            }

            throw ex;
        }


        //tested
        public void RemoveNode(Node node)
        {
            Exception ex = null;
            //var connectedEdges = FindEdges(node);
            if (CanRemoveNode(node, ref ex))
            {
                //Удаляем связанные с вершиной дуги из списка дуг
                DoRemoveNode(node);
                return;
            }
            throw ex;

        }


        //tested
        public void AddEdge(Node from, Node to)
        {
            //Если одна из вершин(обе)отсутствуют в списке вершин, добавляем ее(их) в список вершин

            Exception ex = null;
            if (CanAddEdge(from, to, ref ex))
            {
                DoAddEdge(from, to);
                return;
            }
            throw ex;
        }

        //tested
        public void RemoveEdge(Node from, Node to)
        {
            //Ищем дугу
            var edge = FindEdge(from, to);
            if (edge == null) return;

            Exception ex = null;
            WhichNodeWillBeRemove whichNode;
            Node node = null;
            if (CanRemoveEdge(edge, out whichNode, ref ex))
            {
                switch (whichNode)
                {
                    case WhichNodeWillBeRemove.StartNode:
                        node = from;
                        break;
                    case WhichNodeWillBeRemove.EndNode:
                        node = to;
                        break;
                }

                DoRemoveEdge(edge, node);
                return;
            }
            throw ex;
        }

        //not tested  (simple method)
        public ReadOnlyCollection<Node> GetNodes()
        {
            //Возвращает read-only коллекцию всех вершин
            return _Nodes.AsReadOnly();
        }


        //not tested  (simple method)
        public ReadOnlyCollection<Edge> GetEdges()
        {
            //Возвращает read-only коллекцию всех дуг
            return _Edges.AsReadOnly();
        }


        #endregion

        private Boolean TryAddNode(Node node)
        {
            //Если вершина уже есть в списке вершин, игнорируем вызов
            if (_Nodes.Contains(node))
            {
                return false;
            }
            //Добавляем вершину в список вершин

            _Nodes.Add(node);
            return true;
        }

        #region Protected Methods
        //tested
        protected virtual void DoAddNode(Node node)
        {
            TryAddNode(node);
            //Дуг НЕ добавляем
        }

        //tested
        protected virtual Boolean CanAddNode(Node node, ref Exception ex)
        {
            return !TryGetArgNullRefException(node, nameof(node), ref ex);
        }

        //tested

        protected virtual Boolean CanAddEdge(Node from, Node to, ref Exception ex)
        {
            return !(TryGetArgNullRefException(from, nameof(from), ref ex) ||
                TryGetArgNullRefException(to, nameof(to), ref ex));
            //{
            //    return false;
            //}

            //return true;
        }

        protected enum WhichNodeWillBeRemove
        {
            StartNode,
            EndNode,
            NoOne
        }

        // tested
        protected virtual Boolean CanRemoveEdge(Edge edge, out WhichNodeWillBeRemove node, ref Exception ex)
        {
            node = WhichNodeWillBeRemove.NoOne;
            return true;
        }

        //not tested
        protected virtual Boolean CanRemoveNode(Node node, ref Exception ex)
        {
            //проверку на null можно не делать
            return true;
        }


        //not tested 
        protected virtual void DoRemoveNode(Node node)
        {
            var connectedEdges = FindEdges(node, false); // можно вынести из метода, что бы не вызывать при проверке и удалении дважды
            foreach (var edge in connectedEdges)
            {
                _Edges.Remove(edge);
            }

            _Nodes.Remove(node);
        }


        //not tested (simple method)

        protected virtual void DoRemoveEdge(Edge edge, Node node = null)
        {
            _Edges.Remove(edge);
            //Вершины удаляем по запросу
            _Nodes.Remove(node);
        }

        //tested
        protected virtual void DoAddEdge(Node from, Node to)
        {
            if (!TryAddNode(from) && !TryAddNode(to))
            {
                //Если дуга уже есть в списке дуг, игнорируем вызов
                if (FindEdge(from, to) != default(Edge))
                {
                    //дуга есть
                    return;
                }
            }
            //Добавляем дугу в список дуг
            var edge = new Edge(from, to);
            _Edges.Add(edge);
        }


        // tested
        protected Edge FindEdge(Node from, Node to)
        {
            var result = _Edges.SingleOrDefault(ed1 => ed1.To.Equals(to) && ed1.From.Equals(from));
            return result;
        }


        // not tested 

        protected ReadOnlyCollection<Edge> FindEdges(Node node, Boolean isDirected, IEnumerable<Edge> exceptEdges = null)
        {
            var eds = _Edges.Where(ed => ed.From.Equals(node));
            if (!isDirected)
            {
                eds = eds.Union(_Edges.Where(ed => ed.To.Equals(node)));
            }
            if (exceptEdges != null)
            {
                eds = eds.Except(exceptEdges);
            }
            return eds.ToList().AsReadOnly();
        }

        protected ReadOnlyCollection<Edge> FindEdges(Node node, Boolean isDirected, Edge exceptEdge)
        {
            return FindEdges(node, isDirected, new Edge[] { exceptEdge });
        }

        //tested
        protected virtual ReadOnlyCollection<Node> GetConnectedNodes(Node node, Boolean isDirected, IEnumerable<Node> exceptNodes = null)
        {
            // Получение смежных вершин для одной входящей точки
            //только для узлов заданных в fromNodes

            var connectedEdges = FindEdges(node, isDirected);
            var connectedNodes = connectedEdges.Select(e1 => e1.To);
            if (!isDirected)
            {
                connectedNodes = connectedNodes.Union(connectedEdges.Select(e1 => e1.From));
            }

            if (exceptNodes != null)
            {
                connectedNodes = connectedNodes.Except(exceptNodes);
            }
            var result = connectedNodes.ToList();

            return result.AsReadOnly();
        }

        //not tested
        //TODO сделать тесты
        protected ReadOnlyCollection<Node> GetConnectedNodes(IEnumerable<Node> fromNodes, Boolean isDirected = false, IEnumerable<Node> exceptNodes = null)
        {
            // определение узлов, связанных только с fromNodes

            var result = new List<Node>();

            //только для узлов заданных в fromNodes
            foreach (var node in fromNodes)
            {
                var connectedNodes = GetConnectedNodes(node, isDirected, exceptNodes); //вершины, связанные этими связями

                if (connectedNodes.Count() > 0)
                {
                    result.AddRange(connectedNodes);
                }
            }

            return result.AsReadOnly();
        }

        //        //tested
        //        protected IEnumerable<Node> GetConnectedNodes(IEnumerable<Node> fromNodes, IEnumerable<Node> exceptNodes = null, Boolean OnlyForFromNodes = true, Boolean isDirected = false)
        //        {// определение узлов, связанных с fromNodes
        //         //OnlyForFromNodes=false - поиск по всему дереву, =true -поиск только для узлов из fromNodes
        //#if DEBUG
        //            IncreaseLoopCounter();
        //#endif

        //            var result = new List<Node>();
        //            if (exceptNodes == null)
        //            {
        //                exceptNodes = new List<Node>();
        //            }


        //            if (OnlyForFromNodes)
        //            {
        //                //только для узлов заданных в fromNodes
        //                foreach (var node in fromNodes)
        //                {
        //                    var connectedNodes = GetConnectedNodes(node, isDirected).Except(exceptNodes); //вершины, связанные этими связями

        //                    //var fromNodes1 = connectedNodes.ToArray();
        //                    if (connectedNodes.Count() > 0)
        //                    {
        //                        result.AddRange(connectedNodes);
        //                    }
        //                }
        //                return result;
        //            }


        //            // поиск пути по всему графу
        //            foreach (var node in fromNodes)
        //            {
        //                var connectedNodes = GetConnectedNodes(node, isDirected).Except(exceptNodes); //вершины, связанные этими связями

        //                //var fromNodes1 = connectedNodes.ToArray();

        //                if (connectedNodes.Count() > 0)
        //                {
        //                    result.AddRange(connectedNodes);
        //                    result.AddRange(GetConnectedNodes(connectedNodes, exceptNodes.Union(fromNodes), isDirected));
        //                }



        //            }


        //            return result;
        //        }


        #endregion

        #region Exceptions

        protected Boolean TryGetArgNullRefException(Object parametr, String parName, ref Exception ex)
        {
            if (parametr == null)
            {
                ex = new ArgumentNullException(parName, Messages.ErrorMessages.NullArg);
                return true;
            }
            return false;
        }
        #endregion

        #region C-tor

        internal DirectedGraph()
        {
            _Nodes = new List<Node>();
            _Edges = new List<Edge>();
        }

        //protected DirectedGraph(IEnumerable<Node> nodes)
        //{
        //    _Nodes = nodes.ToList();
        //    _Edges = new List<Edge>();
        //}
        #endregion
    }
}
