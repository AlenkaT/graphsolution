﻿using EVT.Applications.GraphSolution.Graph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.Graph.GraphUnitTests
{
    class Counter
    {
        Int32 NodeCounter;
        internal Int32 IncreaseNodeCounter()
        {
            return NodeCounter++;
        }
       
    }
    public class TestNode: Node
    {
        internal readonly Int32 ID;
        //internal static TestNode CreateTestNode()
        //{
        //    return new TestNode(NodeCounters.TestNodeCounter.IncreaseNodeCounter());
        //}
        public TestNode()
        {
            var id = NodeCounters.TestNodeCounter.IncreaseNodeCounter();
            ID = id;
        }

        public override bool Equals(object obj)
        {
            var node1 = obj as TestNode;
            return node1 == null ? false: node1.ID == ID;
        }

        public override string ToString()
        {
            return "TestNode ID:" + ID;
        }

    }
    static class NodeCounters
    {
        internal static readonly Counter TestNodeCounter = new Counter();
        
    }
}
