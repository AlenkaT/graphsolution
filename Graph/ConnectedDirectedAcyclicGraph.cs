﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.Graph
{
    public class ConnectedDirectedAcyclicGraph : DirectedAcyclicGraph
    {


        #region Protected Methods
        //tested
        protected override bool CanAddNode(Node node, ref Exception ex)
        {
            if (!base.CanAddNode(node, ref ex))
            { return false; }

            //при попытке добавления вершины в связанный граф - exception

            // всё равно не происходит
            if (_Nodes.Count > 0)
            {
                ex = new InvalidOperationException(Messages.ErrorMessages.NodeCantBeAdded);
                return false;
            }
            return true;

        }

        //not tested (simple methods)
        protected override void DoAddNode(Node node)
        {
            _Nodes.Add(node);
        }

        //tested
        protected override bool CanAddEdge(Node from, Node to, ref Exception ex)
        {
            if (!base.CanAddEdge(from, to, ref ex))
            { return false; }
            if (!_Nodes.Contains(from) && !_Nodes.Contains(to))
            {
                ex = new ArgumentException("You try to add unconnected Edge", nameof(from) + "&" + nameof(to));
                return false;
            }
            return true;
        }

        //tested
        protected override bool CanRemoveEdge(Edge edge, out WhichNodeWillBeRemove node, ref Exception ex)
        {

            if (!base.CanRemoveEdge(edge, out node, ref ex)) return false;
            //Если to/ from - одиночная вершина(имеет только одну дугу), удаляем эту дугу и вершину to/ from

            var arr = new Node[] { edge.From, edge.To };
            for (var i = 1; i > -1; i--)
            {
                if (GetConnectedNodes(arr[i], false).Count==1)
                {
                    node = (WhichNodeWillBeRemove)i;
                    //точка крайняя, не содержит детей
                    return true;
                }
            }

            node = WhichNodeWillBeRemove.NoOne;
            if (DoesAlternativePathExist(edge))
            {
                return true;
            }
            ex = new ArgumentException(Messages.ErrorMessages.CantBeDeleted, nameof(edge));
            return false;
        }

        //not tested
        protected override Boolean  CanRemoveNode(Node node, ref Exception ex)
        {
            if (!base. CanRemoveNode(node, ref ex))
            {
                return false;
            }

            IEnumerable<Edge> connectedEdges = FindEdges(node, false); // можно вынести из метода, что бы не вызывать при проверке и удалении дважды

            //если узел имеет только одну связь
            if (connectedEdges.Count()==1)
            {
                return true;
            }
            //предполагается, что нет узлов, у которых нет родителей, кроме
            // единственного корневого узла, поэтому 
            // нет проверки - что node для всех дуг начальная
            // а начальную точку просто так удалить нельзя, нет гарантии, что граф будет связаным
            if (connectedEdges.All(x => x.To.Equals(node))) //node для всех дуг - конечная
            {
                return true;
            }

            //смешанная вершина, имеющая детей и родителей
            var exceptEdges = new List<Edge>();
            //Убираем последнюю связь, так как если цикл до неё дойдет, то вершину удалить можно
            connectedEdges = connectedEdges.Skip(1);
            var t = 0;
            foreach (var edge in connectedEdges)
            {
#if DEBUG
                IncreaseLoopCounter();
                t++;
#endif
                // удаляемые дуги добавить в массив Exept
                //Иначе если после удаления дуги граф перестает быть связным
                if (!DoesAlternativePathExist(edge, exceptEdges))
                {
                    ex = new ArgumentException(Messages.ErrorMessages.CantBeDeleted, nameof(node));
                    return false;
                }
                //после удаления edge граф будет связным
                //var exceptNode = (new Node[] { edge.From, edge.To }).Single(x => !x.Equals(node));// та из вершин дуги, которая отлична от заданной
                exceptEdges.Add(edge);
            }

            return true;

        }

        //tested
        //protected override void DoRemoveEdge(Edge edge, Node node)
        //{
            //Если to/ from - одиночная вершина(имеет только одну дугу), удаляем эту дугу и вершину to/ from

            //foreach (var node in new Node[] { edge.From, edge.To })
            //{
            //    if (FindEdges(node, false, edge).Count() == 0)
            //    {
            //        _Nodes.Remove(node);
            //        res1 = true; // удалена вершина
            //    }
            //}

            //if (res1 || IsConnectionAvailable(edge))
            //{
            //    //какая то из вершин была одиночной
            //   base.DoRemoveEdge(edge);
            //    return;
            //}



            //Иначе если после удаления дуги граф перестает быть связным
            //(считаем кол-во вершин, доступных без учета направления из вершины to и сравниваем с общим кол-вом вершин) - возвращаем дугу на место и кидаем Exception

            //throw new ArgumentException(ErrorMessages.CantBeDeleted, nameof(edge));

        //}



        //Проверка связанности графа после планируемого удаления связи edge
        //not tested (simple method)
//#if DEBUG
//        //protected virtual Boolean IsConnectionAvailable(IEnumerable<Edge> edges)
//        protected virtual Boolean IsConnectionAvailable(Edge edge)
//#endif
        //#if RELEASE
        //protected Boolean IsConnectionAvailable(IEnumerable<Edge> edges)
        //#endif
        //{
        //    IEnumerable<Node> arr = new Node[] { edge.To };
        //    //Количество связанных вершин == Общее количество вершин
        //    return GetConnectedNodes(arr).Count() == _Nodes.Count();
        //}
        //protected Boolean IsConnectionAvailable(Edge edge)
        //{

            //Есть другой путь
            // return IsAlternativePathExist(edges.Select(ed=>ed.From), edges.Select(ed => ed.To), false);
        //    return DoesAlternativePathExist(edge.From, edge.To);
        //}

        
        #endregion

        #region C-tors
        internal ConnectedDirectedAcyclicGraph(Node rootNode)
        {
            _Nodes.Add(rootNode);
        }
        protected ConnectedDirectedAcyclicGraph()
        {
        }

#if DEBUG
        internal ConnectedDirectedAcyclicGraph(IEnumerable<Node> nodes, IEnumerable<Edge> edges)
        {
            _Nodes.AddRange(nodes);
            _Edges.AddRange(edges);
        }
#endif

        #endregion
    }


}
