﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.Graph
{
    public class TreeGraph : ConnectedDirectedAcyclicGraph
    {
        //not tested
        protected override bool CanAddEdge(Node from, Node to, ref Exception ex)
        {
            if (!base.CanAddEdge(from, to, ref ex))
            { return false; }

            //Здесь нужно добавить проверку, что у ноды to не появляется второй родитель
            //посчитать только родителей
            if (FindEdges(to, isDirected: false, exceptEdges: FindEdges(to, isDirected: true)).Count() > 0)
            {
                //есть дуги , для которых to- родитель
                //TODO изменить сообщение исключения
                ex = new InvalidOperationException("You cannot add Edge that has Node ");
            }

            return true;

        }

        internal TreeGraph()
        { }


#if DEBUG
        internal TreeGraph(IEnumerable<Node> nodes, IEnumerable<Edge> edges)
        {
            _Nodes.AddRange(nodes);
            _Edges.AddRange(edges);
        }
#endif
    }
}
