﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.Graph.Messages
{
    internal class ErrorMessages
    {
        internal static readonly string  NullArg = "Input argument of method must be not Null reference";
        internal static readonly string ArgRefEqual = "Reference to ToNode must be not equal to reference FromNode";
        internal static readonly string PathAlreadyExists = "Relation between nodes To-From already exists";
        internal static readonly string CantBeDeleted = "Removing isn't possible";
        internal static readonly string NodeCantBeAdded = "You cannot add a Node into ConnectedGrapph without any Edge";

    }
}
