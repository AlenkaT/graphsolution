﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Serialization.Graph.Json
{
    //
    public class DerivedClassConverter<TBaseClass, TDerivedClass> : JsonConverter where TDerivedClass: TBaseClass, new()
    {
        public override bool CanWrite => false;

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        //Reads the JSON representation of the object.
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null)
            {
                return null;
            }

            //JSON object.
            //используя переданный ридер получаем JSON объект
            JObject jObject = JObject.Load(reader); //A JObject that contains the JSON that was read from the specified JsonReader.
                                                    //JsonReader represents a reader that provides fast, non-cached, forward - only access to serialized JSON data.

            // Create target object based on JObject 
            //TBaseClass target = Create(objectType, jObject);
            var target = new TDerivedClass();

            // Populate the object properties 
            //jObject.CreateReader() - A JsonReader that can be used to read this token and its descendants.
            serializer.Populate(jObject.CreateReader(), target);


            return target;
        }


        public override bool CanConvert(Type objectType)
        {
            return typeof(TBaseClass).IsAssignableFrom(objectType); // проверка совместимости типов
        }


        protected TDerivedClass Create() //зачем сюда передаются параметры?
        {
            return new TDerivedClass();
        }
    }

}
