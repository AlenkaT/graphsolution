﻿using EVT.Applications.GraphSolution.Graph;
using EVT.Serialization.Graph.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace EVT.Serialization.Graph.Json
{
    public class JsonGraphSerializer : IStringGraphSerializer
    {
        public DirectedGraph DeserializeGraphFromString(string graphString)
        {
            var result=Newtonsoft.Json.JsonConvert.DeserializeObject<DirectedGraph>(graphString);
            return result;
        }

        public DirectedGraph DeserializeGraphFromTextFile(string fileName)
        {
            throw new NotImplementedException();
        }

        public string SerializeGraphToString(DirectedGraph graph)
        {
            throw new NotImplementedException();
        }

        public void SerializeGraphToTextFile(DirectedGraph graph, string fileName)
        {
            throw new NotImplementedException();
        }
    }
}
