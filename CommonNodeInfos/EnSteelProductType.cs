﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.SharedInfoTypes
{ 
    public enum EnSteelProductType:byte
    {
        None, //значение по умолчанию ! не должно нигде устанавливаться и возвращаться
        Rebar, // изд-е арматурное
        RebarSteelProduct, // закладное
        SteelConstruction // металлоконструкция
    }

}
