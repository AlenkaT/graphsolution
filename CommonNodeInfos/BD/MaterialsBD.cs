﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/* соответствие классов и марок для арм-ры по гост 5781-82
AI(A240) - Ст3кп(пс, сп);
AII(A300) - Ст5сп(пс) , 18Г2С;
AIII(A400) - 35ГС, 25Г2С, 
А500С, А400С - 32Г2РПС ;
AIV(A600) - 80C, 20ХГ2С;
AV(A800) - 23Х2Г2Т;
AVI(A1000) - 22Х2Г2АЮ, 22Х2Г2Р, 20Х2Г2СР.
*/

namespace EVT.Applications.GraphSolution.SharedInfoTypes
{
    public class MaterialProperties
    {
        public String Caption { get; } //наименование вида метериала по русски-не обязательно
        public String Grade { get; } //марка
        internal MaterialProperties(String caption, String grade)
        {
            Caption = caption;
            Grade = grade;
        }
    }


    public class MaterialsBD
    {
        static MaterialsBD _inst=null;
        public static MaterialsBD Data
        {
            get
            {
                if (_inst==null) { _inst = new MaterialsBD(); }
                return _inst;
            }
        }

        public static Boolean TryGetMaterial(String name, out ReadOnlyCollection<String> accessibleGosts)
        {
            List<String> list = null;
            var result = Data._Materials.TryGetValue(name, out list);
            accessibleGosts = list.AsReadOnly();
            return result;
        }
        public static Boolean TryGetMaterialProperties(String gost, String grade, out MaterialProperties properties)
        {
            List<MaterialProperties> list;
            properties = null;
            var result = Data._MaterialGrades.TryGetValue(gost, out list);
            if (!result)
            {
                return false;
            }
            properties = list.SingleOrDefault(x => x.Grade == grade);
            if (properties == null)
            {
                return false;
            }
            return true;
        }

        protected Dictionary<String, List<String>> _Materials;
        protected Dictionary<String, String> _MaterialsGradesBD; //соответствие гостов русским именам
        protected Dictionary<String, List<MaterialProperties>> _MaterialGrades;
        


        protected MaterialsBD()
        {
            _Materials = new Dictionary<string, List<string>>();
            _MaterialsGradesBD = new Dictionary<string, string>();
            _MaterialsGradesBD["gost26633"] = "ГОСТ 26633-2015";
            _MaterialsGradesBD["gost27772"] = "ГОСТ 27772-2015";
            _MaterialsGradesBD["gost5781"] = "ГОСТ 5781-82";
            _MaterialsGradesBD["gost6713"] = "ГОСТ 6713-91";

            _Materials["Concrete"] = new List<String>() { "gost26633" };
       
            _Materials["Steel"] = new List<String>() { "gost27772", "gost6713", "gost5781" };

            _MaterialGrades = new Dictionary<string, List<MaterialProperties>>();
            String name = "Бетон";
            _MaterialGrades["gost26633"] = new List<MaterialProperties>()
            {
                new MaterialProperties(name,"B15"),
                new MaterialProperties(name,"B20"),
                new MaterialProperties(name,"B22.5"),
                new MaterialProperties(name,"B25"),
                new MaterialProperties(name,"B30"),
                new MaterialProperties(name,"B35"),
                new MaterialProperties(name,"B40"),
                new MaterialProperties(name,"B45"),
                new MaterialProperties(name,"B50"),
                new MaterialProperties(name,"B55"),
                new MaterialProperties(name,"B60")
            };

            name = "Сталь";
            _MaterialGrades["gost27772"] = new List<MaterialProperties>()
            {
                new MaterialProperties(name,"С235"),
                new MaterialProperties(name,"С245"),
                new MaterialProperties(name,"С255"),
                new MaterialProperties(name,"С345"),
                new MaterialProperties(name,"С345K"),
                new MaterialProperties(name,"С355"),
                new MaterialProperties(name,"С355-1"),
                new MaterialProperties(name,"С355К"),
                new MaterialProperties(name,"С355П"),
                new MaterialProperties(name,"С390"),
                new MaterialProperties(name,"С390-1"),
                new MaterialProperties(name,"С440"),
                new MaterialProperties(name,"С550"),
                new MaterialProperties(name,"С590")
            };

            _MaterialGrades["gost6713"] = new List<MaterialProperties>()
            {
                new MaterialProperties(name,"15ХСНД"),
                new MaterialProperties(name,"10ХСНД"),
                new MaterialProperties(name,"16Д")
            };

            _MaterialGrades["gost5781"] = new List<MaterialProperties>()
            {
                new MaterialProperties(name,"А240"),
                new MaterialProperties(name,"А400"),
                new MaterialProperties(name,"А500"),
                new MaterialProperties(name,"А600"),
                new MaterialProperties(name,"Ап600"),
                new MaterialProperties(name,"А800"),
                new MaterialProperties(name,"А1000")
            };
            
        }

        
    }
}
