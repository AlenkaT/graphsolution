﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.SharedInfoTypes
{
    public class SectionProperties
    {
        public String Caption { get; }
        public Single UnitWeight { get; } // вес единицы профиля
        public String Size { get; }
        internal SectionProperties(String caption , String size, Single unitWeight)
        {
            Caption = caption;
            Size = size;
            UnitWeight = unitWeight;
        }
    }
    public class SectionsBD
    {
        static SectionsBD _inst = null;
        public static SectionsBD Data
        {
            get
            {
                if (_inst==null)
                {
                    _inst = new SectionsBD();
                }
                return _inst;
            }
        }
        public static Boolean TryGetSectionType(String sectionTypeName, out ReadOnlyCollection<String> accessibleGosts)
        {
            List<String> list ;
            var result= Data._SectionTypes.TryGetValue(sectionTypeName, out list);
            accessibleGosts = list.AsReadOnly();
            return result;
        }
        public static Boolean TryGetSectionProperties(String gost, String size, out SectionProperties properties)
        {
            List<SectionProperties> list;
            properties = null;
            var result = Data._SectionSizes.TryGetValue(gost, out list);
            if (!result)
            {
                return false;
            }
            properties = list.SingleOrDefault(x => x.Size == size);
            if (properties==null)
            {
                return false;
            }
            return true;
        }

        
        protected Dictionary<String, List<String>> _SectionTypes;
        protected Dictionary<String, String> _SectionTypesBD; //соответствие гостов русским именам
        protected Dictionary<String, List<SectionProperties>> _SectionSizes;

        protected SectionsBD()
        {
            _SectionTypes = new Dictionary<string, List<string>>();
            _SectionTypes["Rebars"] = new List<string>()
            { "gost5751"};
            _SectionTypes["Pipes"] = new List<string>()
            { "gost8732"};
            _SectionTypes["EqualLegAngles"] = new List<string>()
            { "gost8509"};

            _SectionTypesBD = new Dictionary<string, string>();
            _SectionTypesBD["gost5781"] = "ГОСТ 5781-82";
            _SectionTypesBD["gost8732"] = "ГОСТ 8732-0000";
            _SectionTypesBD["gost8509"] = "ГОСТ 34028-0000";
            _SectionSizes = new Dictionary<string, List<SectionProperties>>();

            var name = "Арматура";
            _SectionSizes["gost5781"] = new List<SectionProperties>()
            {
                new SectionProperties(name, "d6", 0.222f),
                new SectionProperties(name, "d8", 0.395f),
                new SectionProperties(name, "d10", 0.617f),
                new SectionProperties(name, "d12", 0.888f),
                new SectionProperties(name, "d14", 1.21f),
                new SectionProperties(name, "d16", 1.58f),
                new SectionProperties(name, "d18", 2.0f),
                new SectionProperties(name, "d20", 2.47f),
                new SectionProperties(name, "d22", 2.98f),
                new SectionProperties(name, "d25", 3.85f),
                new SectionProperties(name, "d28", 4.83f),
                new SectionProperties(name, "d32", 6.31f),
                new SectionProperties(name, "d36", 7.99f),
                new SectionProperties(name, "d40", 9.87f),
                new SectionProperties(name, "d45", 12.48f),
                new SectionProperties(name, "d50", 15.41f),
                new SectionProperties(name, "d55", 18.65f),
                new SectionProperties(name, "d60", 22.19f),
                new SectionProperties(name, "d70", 30.21f),
                new SectionProperties(name, "d80", 39.46f)
            };


            name = "Труба";
            _SectionSizes["gost8732"] = new List<SectionProperties>()
            {
                new SectionProperties(name, "d20_t2.5", 1.08f),
                new SectionProperties(name, "d21.3_t2.5", 1.16f),
                new SectionProperties(name, "d22_t2.5", 1.2f)
            };

            name = "Уголок равнополочный";
            _SectionSizes["gost8509"] = new List<SectionProperties>()
            {
                new SectionProperties(name, "L2_t3", 0.89f),
                new SectionProperties(name, "L2_t4", 1.15f),
                new SectionProperties(name, "L2.5_t3", 1.12f),
                new SectionProperties(name, "L2.5_t4", 1.46f)
            };
        }
    }
}
