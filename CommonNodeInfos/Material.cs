﻿using EVT.Applications.GraphSolution.GraphInfo.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.SharedInfoTypes
{
    public class Material
    {
        public static Material Create(String typeMaterialName, String materialGostName, String grade, Single unitWeight)
        {
            //Проверки
            ReadOnlyCollection<String> gosts; //госты, доступные для данного материала
            if (!MaterialsBD.TryGetMaterial(typeMaterialName, out gosts))
            {
                throw new ArgumentException("Material is missing in MaterialsBD", nameof(typeMaterialName));
            }
            if (!gosts.Contains(materialGostName))
            {
                throw new ArgumentException("This gost is missing in MaterialsBD", nameof(materialGostName));
            }

            MaterialProperties properties;
            if (!MaterialsBD.TryGetMaterialProperties(materialGostName, grade, out properties))
            {
                throw new ArgumentException("This grade is missing in MaterialsBD", nameof(grade));
            }

            return new Material(typeMaterialName, materialGostName, properties, unitWeight);
        }

        public String TypeMaterialName { get; } //бетон, сталь, дерево
        public String MaterialGostName { get; }
        public MaterialProperties Properties { get; } // наименование марки материала
        public Single UnitWeight { get; } //объемный вес
        protected Material(String name, String gost, MaterialProperties properies, Single unitWeight)
        {
            TypeMaterialName = name;
            MaterialGostName = gost;
            Properties= properies;
            UnitWeight = unitWeight;
        }
    }
}
