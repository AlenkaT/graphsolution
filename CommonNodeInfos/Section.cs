﻿using EVT.Applications.GraphSolution.GraphInfo.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.Applications.GraphSolution.SharedInfoTypes
{
    public class Section
    {
        public static Section Create(String sectionType, String gost, String size)
        {
            //Проверки
            ReadOnlyCollection<String> gosts;
            if (!SectionsBD.TryGetSectionType(sectionType, out gosts))
            {
                throw new ArgumentException("Section Type is missing in SectionsBD", nameof(sectionType));
            }
            if (!gosts.Contains(gost))
            {
                throw new ArgumentException("This gost is missing in SectionsBD", nameof(gost));
            }
            SectionProperties properties;
            if (!SectionsBD.TryGetSectionProperties(gost, size, out properties))
            {
                throw new ArgumentException("This size is missing in SectionsBD", nameof(size));
            }

            return new Section(sectionType, gost, properties);
        }
        public String SectionType { get; }
        public String GostSectionName { get; } // гост , определяющий тип профиля
        public SectionProperties Properties { get; } //размер профиля

        protected Section(String sectionType, String gost, SectionProperties properties)
        {
            SectionType = sectionType;
            GostSectionName = gost;
            Properties = properties;
        }
    }
}
